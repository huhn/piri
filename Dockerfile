FROM openjdk:8-jre-alpine

# runtime dependency for tomcat native libraries
RUN apk add --no-cache tomcat-native && \
    rm -rf /var/cache/apk/*

# vendor files
COPY target/unpacked-jar/BOOT-INF/lib /app/BOOT-INF/lib
COPY target/unpacked-jar/org /app/org

# app files
COPY target/unpacked-jar/META-INF /app/META-INF
COPY target/unpacked-jar/BOOT-INF/classes /app/BOOT-INF/classes

ENTRYPOINT ["/usr/bin/java", "-Djava.security.egd=file:/dev/./urandom", "-XX:+UnlockExperimentalVMOptions", \
            "-XX:+UseCGroupMemoryLimitForHeap", "-XX:MaxRAMFraction=1", "-cp", "/app", \
            "org.springframework.boot.loader.JarLauncher"]

EXPOSE 8080
