# Proxy for IRI

Piri is a proxy and load balancer for [Iota](https://www.iota.org) nodes, like [IRI](https://github.com/iotaledger/iri "Iota Iri Node").

It distributes calls to one or more nodes and adds a layer of functionality that IRI does not provide, like
input validation, retry, circuit breakers and metrics. 

Piri is implemented in Java and uses Spring Boot and Webflux. Piri is work in progress but already in use and stable.

## Already implemented

Currently only the following features are implemented.

1. Input validation
1. Metrics
1. Retry
1. Load balancing
1. Circuit breaker
1. Rate Limiter
1. Health check of nodes
1. Authorization
1. Node Registration API
1. Info API (basis for Web UI)

## Soon...

1. Field client compatibility

## Documentation

See [Wiki](https://gitlab.com/georg.mittendorfer/piri/wikis/home)

## Contact

You can contact mio#7632 in iotas discord if you need some help.
  
## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

