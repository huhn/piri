# Feel free to contribute.

We are going to support you as good as possible.
Better to get into contact as early as possible so that misunderstandings can be avoided.
For example open an issue and ask for a discussion.

## Tests

Complete unit test coverage is expected. Integration tests are not necessary. Test naming conventions:

* Unit tests end with 'Test'. Unit tests only test one class and don't need a spring context.
* Integration test end with 'IT'. Integration tests are all tests that are no unit tests.

## Code style

All contributions have to fit our code style and we might want to make changes of syntactical nature.
Please don't be offended by this, we know that code style is very subjective. 
Feel free to discuss the changes if you don't like them.

## License

You have to be the owner of your contributions and by contributing you guarantee and are responsible for that.
All your contributions will have the same license that the project has. But you have to agree that we 
are allowed to dual license under another open source license, if the need arises (for example, if someone needs to use the project MIT licensed).
We guarantee that all your code will stay open source forever!
