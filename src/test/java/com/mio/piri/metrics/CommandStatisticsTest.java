/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.metrics;

import io.micrometer.core.instrument.Timer;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CommandStatisticsTest {

    private final CommandStatistics commandStatistics = new CommandStatistics();

    @Test
    public void whenCalculateCurrentThenReturnCountOfAllTimers() {
        Timer timer1 = mock(Timer.class);
        Timer timer2 = mock(Timer.class);
        when(timer1.count()).thenReturn(1L);
        when(timer2.count()).thenReturn(2L);
        long counted = commandStatistics.calculateCurrent(Arrays.asList(timer1, timer2, timer2));
        assertThat(counted).isEqualTo(5);
    }

    @Test
    public void givenNoTimersWhenCalculateCurrentThenReturnZero() {
        long counted = commandStatistics.calculateCurrent(Collections.emptyList());
        assertThat(counted).isEqualTo(0);
    }

    @Test
    public void shouldCalculateDifference() {
        assertThat(commandStatistics.calculateDifference(5, 3)).isEqualTo(2);
        assertThat(commandStatistics.calculateDifference(5, 5)).isEqualTo(0);
    }

    @Test
    public void whenUpdateThenRememberNewValue() {
        assertThat(commandStatistics.updateCount(3)).isEqualTo(0);
        assertThat(commandStatistics.updateCount(42)).isEqualTo(3);
    }

    @Test
    public void shouldLogStatistics() {
        commandStatistics.logStatistics(13, 27, 6);
    }

}