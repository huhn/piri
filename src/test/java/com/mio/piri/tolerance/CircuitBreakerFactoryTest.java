/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.tolerance;

import com.mio.piri.metrics.MeterFactory;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import org.assertj.core.data.Offset;
import org.junit.Test;
import org.springframework.core.env.Environment;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CircuitBreakerFactoryTest {

    private final MeterFactory meterFactory = mock(MeterFactory.class);
    private final CircuitBreakerFactory factory = new CircuitBreakerFactory(meterFactory, null);

    @Test
    public void shouldCreateCircuitBreaker() {
        CircuitBreaker cb = factory.circuitBreaker("foo");
        assertThat(cb).isNotNull();
        assertThat(cb.getName()).isEqualTo("foo");
        verify(meterFactory).createMetricsForCircuitBreaker(cb);
    }

    @Test
    public void givenSameNameWhenCircuitBreakerThenReturnSame() {
        CircuitBreaker cb = factory.circuitBreaker("foo");
        CircuitBreaker another = factory.circuitBreaker("foo");
        assertThat(cb).isSameAs(another);
    }

    @Test
    public void shouldReadConfigurationOnCreate() {
        Environment env = mock(Environment.class);
        when(env.getProperty(eq("piri.circuit.open.duration"), anyString())).thenReturn("42s");
        when(env.getProperty(eq("piri.circuit.failure.rate"), any(), anyFloat())).thenReturn(4.2f);
        when(env.getProperty(eq("piri.circuit.buffer.closed"), any(), anyInt())).thenReturn(42);
        when(env.getProperty(eq("piri.circuit.buffer.half-open"), any(), anyInt())).thenReturn(43);
        CircuitBreakerFactory factory = new CircuitBreakerFactory(meterFactory, env);
        CircuitBreakerConfig config = factory.circuitBreaker("foo").getCircuitBreakerConfig();
        assertThat(config.getWaitDurationInOpenState()).isEqualTo(Duration.ofSeconds(42));
        assertThat(config.getFailureRateThreshold()).isCloseTo(4.2f, Offset.offset(0.1f));
        assertThat(config.getRingBufferSizeInClosedState()).isEqualTo(42);
        assertThat(config.getRingBufferSizeInHalfOpenState()).isEqualTo(43);
    }

}