/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.tolerance;

import com.mio.piri.metrics.MeterFactory;
import io.github.resilience4j.ratelimiter.RateLimiter;
import org.junit.Test;
import org.springframework.core.env.Environment;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class RateLimiterRegistryTest {

    private final Environment env = mock(Environment.class);
    private final MeterFactory meterFactory = mock(MeterFactory.class);

    private final RateLimiterRegistry registry = new RateLimiterRegistry(env, meterFactory);

    @Test
    public void whenGlobalRateLimiterThenReturnSame() {
        assertThat(registry.globalRateLimiter()).isSameAs(registry.globalRateLimiter());
    }

    @Test
    public void whenRateLimiterThenReturnDifferent() {
        assertThat(registry.rateLimiter("foo", null)).isNotEqualTo(registry.rateLimiter("bar", null));
    }

    @Test
    public void whenRateLimiterThenCleanUpCache() throws InterruptedException {
        when(env.getProperty("piri.rate.cleanup.max.count", Integer.class)).thenReturn(1);
        when(env.getProperty("piri.rate.cleanup.idle")).thenReturn("50ms");
        RateLimiterRegistry registry = new RateLimiterRegistry(env, meterFactory);

        RateLimiter limiter = registry.rateLimiter("foo", null);
        TimeUnit.MILLISECONDS.sleep(100);
        registry.rateLimiter("bar", null); // should replace foo
        assertThat(limiter).isNotSameAs(registry.rateLimiter("foo", null));
    }

    @Test
    public void givenRateLimitedCommandWhenRateLimiterThenReturnSpecificLimiter() {
        when(env.getProperty("piri.rate.ip.limited.commands")).thenReturn("foo-command");
        RateLimiterRegistry registry = new RateLimiterRegistry(env, meterFactory);

        RateLimiter limiter = registry.rateLimiter("foo", "foo-command");
        RateLimiter another = registry.rateLimiter("foo", "bar-command");
        assertThat(limiter).isNotEqualTo(another);
        assertThat(limiter.getRateLimiterConfig()).isNotSameAs(another.getRateLimiterConfig());
    }

    @Test
    public void givenUnlimitedCommandWhenRateLimiterThenReturnSameLimiter() {
        when(env.getProperty("piri.rate.ip.limited.commands")).thenReturn("some-command");
        RateLimiterRegistry registry = new RateLimiterRegistry(env, meterFactory);

        RateLimiter limiter = registry.rateLimiter("foo", "foo-command");
        assertThat(limiter).isSameAs(registry.rateLimiter("foo", "bar-command"));
    }

    @Test
    public void whenConstructThenRegisterGauge() {
        verify(meterFactory).createRateLimiterCountGauge(anyMap());
    }

    @Test
    public void givenNoConfigWhenIsRateLimitedIpThenReturnTrue() {
        RateLimiterRegistry registry = new RateLimiterRegistry(env, meterFactory);
        assertThat(registry.isIpRateLimited("foo")).isTrue();
        assertThat(registry.isIpRateLimited(null)).isTrue();
    }

    @Test
    public void givenNoMatchWhenIsRateLimitedIpThenReturnTrue() {
        when(env.getProperty("piri.rate.ip.unlimited.pattern")).thenReturn("foo");
        RateLimiterRegistry registry = new RateLimiterRegistry(env, meterFactory);
        assertThat(registry.isIpRateLimited("bar")).isTrue();
        assertThat(registry.isIpRateLimited(null)).isTrue();
    }

    @Test
    public void givenMatchWhenIsRateLimitedIpThenReturnFalse() {
        when(env.getProperty("piri.rate.ip.unlimited.pattern")).thenReturn("foo");
        RateLimiterRegistry registry = new RateLimiterRegistry(env, meterFactory);
        assertThat(registry.isIpRateLimited("foo")).isFalse();
    }

}