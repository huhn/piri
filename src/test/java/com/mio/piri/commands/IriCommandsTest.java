/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands;

import org.junit.Test;

import static com.mio.piri.commands.IriCommands.*;
import static org.assertj.core.api.Assertions.assertThat;

public class IriCommandsTest {

    @Test
    public void whenGetByNameThenReturnEnum() {
        assertThat(byKey("addNeighbors")).isSameAs(ADD_NEIGHBORS);
        assertThat(byKey("attachToTangle")).isSameAs(ATTACH_TO_TANGLE);
        assertThat(byKey("broadcastTransactions")).isSameAs(BROADCAST_TRANSACTIONS);
        assertThat(byKey("checkConsistency")).isSameAs(CHECK_CONSISTENCY);
        assertThat(byKey("findTransactions")).isSameAs(FIND_TRANSACTIONS);
        assertThat(byKey("getBalances")).isSameAs(GET_BALANCES);
        assertThat(byKey("getInclusionStates")).isSameAs(GET_INCLUSION_STATES);
        assertThat(byKey("getMissingTransactions")).isSameAs(GET_MISSING_TRANSACTIONS);
        assertThat(byKey("getNeighbors")).isSameAs(GET_NEIGHBORS);
        assertThat(byKey("getNodeInfo")).isSameAs(GET_NODE_INFO);
        assertThat(byKey("getTips")).isSameAs(GET_TIPS);
        assertThat(byKey("getTransactionsToApprove")).isSameAs(GET_TRANSACTIONS_TO_APPROVE);
        assertThat(byKey("getTrytes")).isSameAs(GET_TRYTES);
        assertThat(byKey("interruptAttachingToTangle")).isSameAs(INTERRUPT_ATTACHING_TO_TANGLE);
        assertThat(byKey("removeNeighbors")).isSameAs(REMOVE_NEIGHBORS);
        assertThat(byKey("storeTransactions")).isSameAs(STORE_TRANSACTIONS);
        assertThat(byKey("wereAddressesSpentFrom")).isSameAs(WERE_ADDRESSES_SPENT_FROM);
    }

    @SuppressWarnings("SpellCheckingInspection")
    @Test
    public void givenWrongCaseWhenGetByNameThenReturnEnum() {
        assertThat(byKey("addneighbors")).isSameAs(ADD_NEIGHBORS);
        assertThat(byKey("attachtotangle")).isSameAs(ATTACH_TO_TANGLE);
        assertThat(byKey("broadcasttransactions")).isSameAs(BROADCAST_TRANSACTIONS);
        assertThat(byKey("checkconsistency")).isSameAs(CHECK_CONSISTENCY);
        assertThat(byKey("findtransactions")).isSameAs(FIND_TRANSACTIONS);
        assertThat(byKey("getbalances")).isSameAs(GET_BALANCES);
        assertThat(byKey("getinclusionstates")).isSameAs(GET_INCLUSION_STATES);
        assertThat(byKey("GETMISSINGTRANSACTIONS")).isSameAs(GET_MISSING_TRANSACTIONS);
        assertThat(byKey("GETNEIGHBORS")).isSameAs(GET_NEIGHBORS);
        assertThat(byKey("GETNODEINFO")).isSameAs(GET_NODE_INFO);
        assertThat(byKey("GETTIPS")).isSameAs(GET_TIPS);
        assertThat(byKey("GETTRANSACTIONSTOAPPROVE")).isSameAs(GET_TRANSACTIONS_TO_APPROVE);
        assertThat(byKey("getTRYTES")).isSameAs(GET_TRYTES);
        assertThat(byKey("INTERRUPTAttachingToTangle")).isSameAs(INTERRUPT_ATTACHING_TO_TANGLE);
        assertThat(byKey("removeNEIGHBORS")).isSameAs(REMOVE_NEIGHBORS);
        assertThat(byKey("sTORETransactions")).isSameAs(STORE_TRANSACTIONS);
        assertThat(byKey("wereADDRESSESSpentfrom")).isSameAs(WERE_ADDRESSES_SPENT_FROM);
    }

    @Test
    public void givenUnknownNameWhenGetByNameThenReturnNull() {
        assertThat(byKey("foo")).isNull();
        assertThat(byKey(null)).isNull();
    }

}