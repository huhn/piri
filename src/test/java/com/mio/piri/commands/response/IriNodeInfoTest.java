/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands.response;

import com.mio.piri.util.JsonTestUtil;
import com.mio.piri.util.JsonUtil;
import org.junit.Test;

import static com.mio.piri.commands.response.ResponseDtoBuilder.nodeInfoBuilder;
import static org.assertj.core.api.Assertions.assertThat;

public class IriNodeInfoTest {

    private final NodeInfo sameMileStone = nodeInfoBuilder().latestMilestoneIndex(42).latestSolidSubtangleMilestoneIndex(42).build();
    private final NodeInfo oneOff = nodeInfoBuilder().latestMilestoneIndex(42).latestSolidSubtangleMilestoneIndex(41).build();
    private final NodeInfo notSynced = nodeInfoBuilder().latestMilestoneIndex(42).latestSolidSubtangleMilestoneIndex(40).build();

    private static final String nodeInfoExample = "{" +
            " \"appName\": \"IRI\"," +
            " \"appVersion\": \"1.5.3\"," +
            " \"jreAvailableProcessors\": 5," +
            " \"jreFreeMemory\": 111826432," +
            " \"jreVersion\": \"1.8.0_172\"," +
            " \"jreMaxMemory\": 6681526272," +
            " \"jreTotalMemory\": 969408512," +
            " \"latestMilestone\": \"FOO\"," +
            " \"latestMilestoneIndex\": 694476," +
            " \"latestSolidSubtangleMilestone\": \"BAR\"," +
            " \"latestSolidSubtangleMilestoneIndex\": 694475," +
            " \"milestoneStartIndex\": 590000," +
            " \"neighbors\": 5," +
            " \"packetsQueueSize\": 0," +
            " \"time\": 1534703576506," +
            " \"tips\": 5056," +
            " \"transactionsToRequest\": 1," +
            " \"duration\": 0" +
            "}";

    @Test
    public void whenIsSyncedThenReturnSyncStatus() {
        assertThat(sameMileStone.isSynced()).isTrue();
        assertThat(oneOff.isSynced()).isTrue();
        assertThat(notSynced.isSynced()).isFalse();
    }

    @Test
    public void whenCreateWithoutArgumentsThenIsNotSynced() {
        NodeInfo empty = nodeInfoBuilder().build();
        assertThat(empty.isSynced()).isFalse();
    }

    @Test
    public void testDeserialization() {
        assertThat(JsonTestUtil.toJson(nodeInfoBuilder().build())).doesNotContain("appName");
        assertThat(JsonTestUtil.toJson(nodeInfoBuilder().build())).doesNotContain("appVersion");
        assertThat(JsonTestUtil.toJson(nodeInfoBuilder().build()).toLowerCase()).doesNotContain("synced");
        assertThat(JsonTestUtil.toJson(nodeInfoBuilder().build()).toLowerCase()).doesNotContain("valid");
        assertThat(JsonTestUtil.toJson(nodeInfoBuilder().appName("foo").build()))
                .contains("\"appName\":\"foo\"");
        assertThat(JsonTestUtil.toJson(nodeInfoBuilder().appVersion("foo").build()))
                .contains("\"appVersion\":\"foo\"");
        assertThat(JsonTestUtil.toJson(nodeInfoBuilder().latestSolidSubtangleMilestoneIndex(42).build()))
                .contains("\"latestSolidSubtangleMilestoneIndex\":42");
        assertThat(JsonTestUtil.toJson(nodeInfoBuilder().latestMilestoneIndex(42).build()))
                .contains("\"latestMilestoneIndex\":42");
    }

    @Test
    public void testSerialization() {
        NodeInfo nodeInfo = JsonUtil.fromJson(nodeInfoExample, NodeInfo.class);
        assertThat(nodeInfo.getLatestMilestoneIndex()).isEqualTo(694476);
        assertThat(nodeInfo.getLatestSolidSubtangleMilestoneIndex()).isEqualTo(694475);
        assertThat(nodeInfo.getAppName()).isEqualTo("IRI");
        assertThat(nodeInfo.getAppVersion()).isEqualTo("1.5.3");
    }


}