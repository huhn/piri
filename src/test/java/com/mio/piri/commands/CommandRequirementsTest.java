/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class CommandRequirementsTest {


    @Parameterized.Parameters(name = "{index}: {0}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { new IriCommand() { }, true, true, true }, // default
                { new AttachToTangle(), false, false, false },
                { new BroadcastTransactions(), false, false, false },
                { new CheckConsistency(), true, true, true },
                { new FindTransactions(), true, true, true }, // client might need tx that is not available on all nodes (snapshot)
                { new GetBalances(), true, true, true },
                { new GetInclusionStates(), true, true, true },  // needs a very current node
                { new GetMissingTransactions(), false, false, false },
                // we want get node info from a up to date node. If not up to date switch client to another one.
                { new GetNodeInfo(), true, true, true },
                // we assume that someone asking for tips want's new ones. not clear what this is for.
                { new GetTips(), false, true, true },
                { new GetTransactionsToApprove(), true, true, true }, // needs a very current node
                { new GetTrytes(), true, true, true },
                // we assume that it makes sense to store transactions at that node the client is currently using.
                // should we switched if not up to date?
                { new StoreTransactions(), true, false, true }, // we don't want to store transactions on a node that is far behind
                { new WereAddressesSpentFrom(), true, true, true },
        });
    }

    @Parameterized.Parameter // first data value (0) is default
    public IriCommand command;

    @Parameterized.Parameter(1)
    public boolean clientBound;

    @Parameterized.Parameter(2)
    public boolean requiresUpToDate;

    @Parameterized.Parameter(3)
    public boolean caresAboutDelay;

    @Test
    public void testWantsSameClient() {
        assertThat(command.wantsSameNodePerClient()).isEqualTo(clientBound);
    }

    @Test
    public void testNeedsUpToDate() {
        assertThat(command.needsUpToDateNode()).isEqualTo(requiresUpToDate);
    }

    @Test
    public void testCaresAboutDelay() {
        assertThat(command.wantsUpToDateNode()).isEqualTo(caresAboutDelay);
    }


}
