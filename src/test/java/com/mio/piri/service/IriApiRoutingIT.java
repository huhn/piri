/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.springSecurity;
import static org.springframework.web.reactive.function.client.ExchangeFilterFunctions.basicAuthentication;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = { "iri.direct.forward.enabled=true"},  webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IriApiRoutingIT {

    @Autowired
    private ApplicationContext context;

    private WebTestClient restClient;

    @Before
    public void setUpSecurity() {
        restClient = WebTestClient.bindToApplicationContext(context).apply(springSecurity()).configureClient().filter(basicAuthentication()).build();
    }

    @Test
    public void whenPingThenForwardToHealthCheck() {
        restClient.get().uri("/").exchange()
                .expectStatus().is3xxRedirection()
                .expectHeader().valueMatches(HttpHeaders.LOCATION, "/actuator/health");
    }

    @WithMockUser
    @Test
    public void givenLoggedInWhenWrongUriThen404NotFound() {
        restClient.get().uri("/foo/bar")
                .accept(MediaType.TEXT_PLAIN)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    public void givenNotLoggedInWhenWrongUriThen401Unauthorized() {
        restClient.get().uri("/foo/bar")
                .accept(MediaType.TEXT_PLAIN)
                .exchange()
                .expectStatus()
                .isUnauthorized();
    }


}