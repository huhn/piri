/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service;

import com.mio.piri.commands.IriCommand;
import com.mio.piri.exceptions.NoNodeAvailable;
import com.mio.piri.metrics.MeterFactory;
import com.mio.piri.nodes.IriNode;
import com.mio.piri.nodes.selection.NodeSelector;
import com.mio.piri.tolerance.RateLimiterRegistry;
import com.mio.piri.util.ClientSessionExtractor;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.ratelimiter.RateLimiterConfig;
import io.github.resilience4j.ratelimiter.RequestNotPermitted;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.collection.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class IriApiHandlerTest {

    @Mock
    private CommandChecker commandChecker;

    @Mock
    private MeterFactory metrics;

    @Mock
    private NodeSelector nodeSelector;

    @Mock
    private ClientSessionExtractor sessionReader;

    @Mock
    private RateLimiterRegistry rateLimiterRegistry;

    @InjectMocks
    private final IriApiHandler iriApiHandler = new IriApiHandler();

    @Mock
    private ServerHttpRequest httpRequest;

    @Mock
    private IriCommand command;

    @Mock
    private IriNode node;

    @Mock
    private ResponseEntity<String> response;

    private final RateLimiter dummyLimiter = io.github.resilience4j.ratelimiter.RateLimiterRegistry.ofDefaults().rateLimiter("test-rate-limiter");
    private final RateLimiter dummyGlobalLimiter = io.github.resilience4j.ratelimiter.RateLimiterRegistry.ofDefaults().rateLimiter("test-global-limiter");
    private final MeterRegistry simpleMeterRegistry = new SimpleMeterRegistry();
    private final Counter rejectedCounter = simpleMeterRegistry.counter("test-rejected-counter");
    private final Counter failedCounter = simpleMeterRegistry.counter("test-error-counter");

    @Before
    public void setUpMocks() {
        when(command.getCommand()).thenReturn("test-command");
        when(node.getName()).thenReturn("test-node");
        when(commandChecker.isEnabled(any())).thenReturn(true);
        when(metrics.createRejectedCommandCounter(anyString())).thenReturn(rejectedCounter);
        when(metrics.createFailedCommandCounter(anyString(), anyString())).thenReturn(failedCounter);
        when(sessionReader.getClientIp(httpRequest)).thenReturn("test-ip");
        when(rateLimiterRegistry.rateLimiter(anyString(), anyString())).thenReturn(dummyLimiter);
        when(rateLimiterRegistry.globalRateLimiter()).thenReturn(dummyGlobalLimiter);
        when(rateLimiterRegistry.isIpRateLimited(anyString())).thenReturn(true);
        when(nodeSelector.selectNodes(any(IriCommand.class))).thenReturn(List.of(node));
        when(response.getStatusCode()).thenReturn(HttpStatus.OK);
    }

    @Test
    public void whenExchangeThenCallIri() {
        when(node.call(any(IriCommand.class))).thenReturn(Mono.just(response));
        Mono<ResponseEntity<String>> responseMono = iriApiHandler.exchangeWithIri(command, httpRequest);
        StepVerifier.create(responseMono)
                .expectNext(response)
                .verifyComplete();
    }

    @Test
    public void whenExchangeThenExtractSessionInformation() {
        when(node.call(any(IriCommand.class))).thenReturn(Mono.just(response));
        Mono<ResponseEntity<String>> responseMono = iriApiHandler.exchangeWithIri(command, httpRequest);
        StepVerifier.create(responseMono)
                .expectNext(response)
                .verifyComplete();

        verify(sessionReader).getClientIp(httpRequest);
        verify(sessionReader).getClientSslId(httpRequest);
        verify(command).setSessionId(anyString());
    }

    @Test
    public void whenExchangeThenCheckIfCommandIsEnabled() {

        when(node.call(any(IriCommand.class))).thenReturn(Mono.just(response));
        Mono<ResponseEntity<String>> responseMono = iriApiHandler.exchangeWithIri(command, httpRequest);
        StepVerifier.create(responseMono)
                .expectNext(response)
                .verifyComplete();
        verify(commandChecker).isEnabled(command);

    }

    @Test
    public void givenUnauthorizedCommandWhenExchangeThenReturnUnauthorized() {

        when(commandChecker.isEnabled(any())).thenReturn(false);
        Mono<ResponseEntity<String>> responseMono = iriApiHandler.exchangeWithIri(command, httpRequest);
        StepVerifier.create(responseMono)
                .expectNext(ResponseEntity
                        .status(HttpStatus.UNAUTHORIZED)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body("{\"error\":\"COMMAND test-command is not available on this node\",\"duration\":0}"))
                .verifyComplete();

        assertThat(rejectedCounter.count()).isEqualTo(1);
    }

    @Test
    public void givenRateLimitTriggeredWhenExchangeThenReturnRateLimitReached() {

        RateLimiter triggered = io.github.resilience4j.ratelimiter.RateLimiterRegistry.of(RateLimiterConfig.custom()
                .limitForPeriod(2)
                .limitRefreshPeriod(Duration.ofHours(1))
                .timeoutDuration(Duration.ofMillis(100)) // duration for requests to wait until RequestNotPermitted
                .build()).rateLimiter("trigger-me");
        triggered.getEventPublisher()
                .onSuccess(event -> System.out.println("success: " + event)).onFailure(event -> System.out.println("failure: " + event));
        when(sessionReader.getClientIp(httpRequest)).thenReturn("trigger-me");
        when(rateLimiterRegistry.rateLimiter(anyString(), anyString())).thenReturn(triggered);

        Mono<ResponseEntity<String>> error = Mono.error(new RuntimeException("test"));
        when(node.call(any(IriCommand.class)))
                .thenReturn(error) // first. should not count for rate limit.
                .thenReturn(error) // second. should not count for rate limit.
                .thenReturn(Mono.just(response)); // all others

        // rate limit not reached yet
        StepVerifier.create(iriApiHandler.exchangeWithIri(command, httpRequest))
                .expectNext(response)
                .verifyComplete();

        // rate limit reached
        StepVerifier.create(iriApiHandler.exchangeWithIri(command, httpRequest))
                .expectNext(response)
                .verifyComplete();

        // cannot acquire withing timeout
        StepVerifier.create(iriApiHandler.exchangeWithIri(command, httpRequest))
                .expectError(RequestNotPermitted.class)
                .verify();

        // retries don't count to rate limit
        assertThat(failedCounter.count()).isEqualTo(2);

    }

    @Test
    public void givenLongTimeOutWhenRateLimitThenDoNotThrow() {

        RateLimiter triggered = io.github.resilience4j.ratelimiter.RateLimiterRegistry.of(RateLimiterConfig.custom()
                .limitForPeriod(1)
                .limitRefreshPeriod(Duration.ofMillis(500))
                .timeoutDuration(Duration.ofMillis(500))
                .build()).rateLimiter("trigger-me");
        triggered.getEventPublisher()
                .onSuccess(event -> System.out.println("success: " + event)).onFailure(event -> System.out.println("failure: " + event));
        when(sessionReader.getClientIp(httpRequest)).thenReturn("trigger-me");
        when(rateLimiterRegistry.rateLimiter(anyString(), anyString())).thenReturn(triggered);

        when(node.call(any(IriCommand.class)))
                .thenReturn(Mono.just(response)); // all others

        for (int i = 0; i < 3; i++) {
            StepVerifier.create(iriApiHandler.exchangeWithIri(command, httpRequest))
                    .expectNext(response)
                    .verifyComplete();
        }

    }

    @Test
    public void givenRateLimitExceptionWhenExchangeThenIgnoreRateLimit() {
        when(sessionReader.getClientIp(httpRequest)).thenReturn("some-ip");
        when(rateLimiterRegistry.isIpRateLimited("some-ip")).thenReturn(false);
        when(node.call(any(IriCommand.class))).thenReturn(Mono.just(response));

        StepVerifier.create(iriApiHandler.exchangeWithIri(command, httpRequest))
                .expectNext(response)
                .verifyComplete();

        verify(rateLimiterRegistry, never()).globalRateLimiter();
        verify(rateLimiterRegistry, never()).rateLimiter(anyString(), anyString());
    }

    @Test
    public void givenNoActiveNodesWhenExchangeThenReturnRateLimitReached() {
        when(nodeSelector.selectNodes(command)).thenReturn(List.empty());
        Mono<ResponseEntity<String>> responseMono = iriApiHandler.exchangeWithIri(command, httpRequest);
        StepVerifier.create(responseMono)
                .expectError(NoNodeAvailable.class)
                .verify();
    }

    @Test
    public void givenTwoErrorsWhenExchangeThenReturnThirdResult() {
        // first two calls return error. third returns result.
        Mono<ResponseEntity<String>> error = Mono.error(new RuntimeException("test"));
        when(node.call(any(IriCommand.class))).thenReturn(error).thenReturn(error).thenReturn(Mono.just(response));
        // test
        Mono<ResponseEntity<String>> responseMono = iriApiHandler.exchangeWithIri(command, httpRequest);
        StepVerifier.create(responseMono)
                .expectNext(response)
                .verifyComplete();

        assertThat(failedCounter.count()).isEqualTo(2);
    }

    @Test
    public void givenErrorsWhenExchangeThenRetryWithDifferentNode() {
        // first node returns error. second node returns result.
        Mono<ResponseEntity<String>> error = Mono.error(new RuntimeException("test"));
        IriNode another = mock(IriNode.class);
        when(another.getName()).thenReturn("bar");
        when(nodeSelector.selectNodes(command)).thenReturn(List.of(node)).thenReturn(List.of(another));
        when(node.call(any(IriCommand.class))).thenReturn(error);
        when(another.call(any(IriCommand.class))).thenReturn(Mono.just(response));
        // test
        Mono<ResponseEntity<String>> responseMono = iriApiHandler.exchangeWithIri(command, httpRequest);
        StepVerifier.create(responseMono)
                .expectNext(response)
                .verifyComplete();
    }

}