/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service;

import com.mio.piri.commands.GetNodeInfo;
import com.mio.piri.util.TestCommandFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"iri.allow.attachToTangle=true", "iri.nodes="}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IriApiHandlerAttachToTangleIT extends AbstractIriApiHandlerTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void givenNoNodeForPowWhenExchangeThenTooManyRequests() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        util.postCommand(new GetNodeInfo()).expectStatus().isOk(); // get node info is supported by node

        // for pow we do not find a node that supports this command
        String responseBody = util.postCommand(TestCommandFactory.attachToTangleCommand("a", "b", "c"))
                .expectStatus().isEqualTo(HttpStatus.TOO_MANY_REQUESTS)
                .expectBody(String.class).returnResult().getResponseBody();

        logger.debug(responseBody);
    }

    protected boolean powEnabled() {
        return false;
    }

}