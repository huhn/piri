/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service;

import com.mio.piri.commands.response.ResponseDtoBuilder;
import com.mio.piri.nodes.IriNode;
import com.mio.piri.nodes.NodeRegistry;
import com.mio.piri.util.IriClientTestUtility;
import com.mio.piri.util.JsonTestUtil;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public abstract class AbstractIriApiHandlerTest {

    private MockWebServer server;

    @Autowired
    protected WebTestClient webTestClient;

    @Autowired
    protected NodeRegistry nodeRegistry;

    protected IriClientTestUtility util;

    protected final String syncedNodeInfo = JsonTestUtil.toJson(ResponseDtoBuilder.nodeInfoBuilder().latestMilestoneIndex(42).latestSolidSubtangleMilestoneIndex(42).build());

    @Before
    public void startServer() throws IOException, InterruptedException {
        util = new IriClientTestUtility(webTestClient);
        server = new MockWebServer();
        server.start(1234);
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));
        IriNode test = nodeRegistry.registerIriNode("test", "http://localhost:1234", "testKey", powEnabled());
        test.queryNodeHealth().subscribe();
        TimeUnit.MILLISECONDS.sleep(250);
    }

    @After
    public void stopServer() throws IOException {
        nodeRegistry.unregisterNode("test");
        server.shutdown();
    }

    protected void prepareResponse(Consumer<MockResponse> consumer) {
        MockResponse response = new MockResponse();
        consumer.accept(response);
        this.server.enqueue(response);
    }

    protected boolean powEnabled() {
        return true;
    }

}
