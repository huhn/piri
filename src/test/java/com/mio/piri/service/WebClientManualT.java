/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service;

import com.mio.piri.commands.GetNodeInfo;
import com.mio.piri.commands.response.ResponseDtoBuilder;
import com.mio.piri.util.JsonTestUtil;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

@Ignore("For manual testing only.")
@RunWith(SpringRunner.class)
public class WebClientManualT {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final int port = 1234;
    private final int testRuns = 100;
    private final int simultaneousRequests = 30;

    private final List<MockWebServer> servers = new ArrayList<>();
    private final MockWebServer server = new MockWebServer();

    private WebClient webClient;

    private final String someResponse = JsonTestUtil.toJson(ResponseDtoBuilder.nodeInfoBuilder().latestMilestoneIndex(42).latestSolidSubtangleMilestoneIndex(42).build());


    @Before
    public void startServer() throws IOException {
        logger.info("Start server.");
        server.start(port-1);

    }

    @Before
    public void startServers() throws IOException {
        logger.info("Start server.");
        for (int i = 0; i < simultaneousRequests; i++) {
            MockWebServer server = new MockWebServer();
            servers.add(server);
            server.start(port + i);
        }

    }

    @Before
    public void createWebClient() {
        ReactorClientHttpConnector connector = new ReactorClientHttpConnector();
        webClient = WebClient.builder().clientConnector(connector)
                .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
    }

    @After
    public void stopServer() throws IOException {
        logger.info("Stopping server.");
        server.shutdown();
    }

    @After
    public void stopServers() throws IOException {
        logger.info("Stopping servers.");
        for (MockWebServer server : servers) {
            server.shutdown();
        }
    }

    @Test
    public void sendRequests() throws Exception {

        java.util.logging.Logger.getLogger(MockWebServer.class.getName()).setLevel(java.util.logging.Level.WARNING);

        logger.info("Starting test.");

        for (int j = 0; j < testRuns; j++) {

            List<Mono<String>> responseMonos = new ArrayList<>();

            for (int i = 0; i < simultaneousRequests; i++) {
                prepareResponse(i, response -> response
                        .setHeader("Content-Type", "application/json")
                        .setBody(someResponse));

                Mono<String> responseMono = webClient.post()
                        .uri("http://localhost:" + (port + i)) // + (port))
                        .syncBody(new GetNodeInfo())
                        .retrieve()
                        .bodyToMono(String.class)
                        .onErrorResume(t -> {
                            logger.error(t.toString(), t);
                            return Mono.just(someResponse);
                        })
                        ;
                responseMonos.add(responseMono);
            }


            Mono.zip(responseMonos, result -> Arrays.stream(result).count())
                    .subscribe(
                            r -> logger.debug("Processed [{}] requests.", r),
                            t -> logger.error("Processing failed failed: [{}]", t.toString(), t)
                    );

            logger.info("Prepared run [{}]", j);
            TimeUnit.MILLISECONDS.sleep(1000);

        }

        TimeUnit.SECONDS.sleep(testRuns);

    }


    private void prepareResponse(int index, Consumer<MockResponse> consumer) {
        MockResponse response = new MockResponse();
        response.setBodyDelay(ThreadLocalRandom.current().nextInt(0, 1001), TimeUnit.MILLISECONDS);
        consumer.accept(response);
        servers.get(index).enqueue(response);
    }



}