/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service;

import com.mio.piri.service.CommandChecker;
import com.mio.piri.util.TestCommandFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class CommandCheckerTest {

    @Mock
    private Environment env;

    @Before
    public void init() {
        when(env.getProperty(anyString(), any(), anyBoolean())).thenReturn(false);
    }

    @Test
    public void givenDisabledCommandThenReturnFalse() {
        CommandChecker checker = new CommandChecker(env);
        assertThat(checker.isEnabled(TestCommandFactory.getNeighborsCommand())).isFalse();
        assertThat(checker.isEnabled(TestCommandFactory.getTrytes("foo"))).isFalse();
    }

    @Test
    public void givenEnabledCommandThenReturnTrue() {
        when(env.getProperty("iri.allow.getNeighbors", Boolean.class, false)).thenReturn(true);
        CommandChecker checker = new CommandChecker(env);
        assertThat(checker.isEnabled(TestCommandFactory.getNeighborsCommand())).isTrue();
    }

    @Test
    public void givenNullWhenIsEnabledThenReturnFalse() {
        CommandChecker checker = new CommandChecker(env);
        assertThat(checker.isEnabled(null)).isFalse();
    }

}