/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mio.piri.commands.*;
import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.metrics.MeterFactory;
import com.mio.piri.util.IriClientTestUtility;
import com.mio.piri.util.ResponseHashes;
import com.mio.piri.util.responses.GetTransactionsToApproveResponse;
import io.micrometer.core.instrument.Timer;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.concurrent.TimeUnit;

import static com.mio.piri.commands.IriCommands.GET_NODE_INFO;
import static com.mio.piri.util.TestCommandFactory.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@SuppressWarnings("SpellCheckingInspection")
@RunWith(SpringRunner.class)
@SpringBootTest(properties = {
        "iri.nodes=test",
        "iri.test.url=http://localhost:14265",
        "iri.test.pow=true",
        "pow.nodes=powsrv",
        "pow.powsrv.url=https://api.powsrv.io",
        "iri.allow.loopback.bypass=true",
        "iri.allow.attachToTangle=true", // false by default
        "iri.allow.interruptAttachingToTangle=true", // false by default
        "iri.allow.getNeighbors=true", // false by default
        "iri.allow.removeNeighbors=true", // false by default
        "iri.allow.addNeighbors=true", // false by default
        "iri.allow.getMissingTransactions=true" // false by default
}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IriApiHandlerCommandsSysIT {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private MeterFactory meterFactory;

    private IriClientTestUtility util;

    @Before
    public void initTestUtils() {
        util = new IriClientTestUtility(webTestClient);
    }


    // FIXME as nodes are checked for sync base on commands and no health check did run yet we don't find any nodes.
    // Solution: add health check on node registration and/or make defaults so that node seems to be 'synced' and not delayed.

    @Test
    public void whenPostThenRecordTime() {
        Timer timer = meterFactory.createCommandTimer(GET_NODE_INFO.getKey(), "test", "test");
        long count = timer.count();
        double start = timer.totalTime(TimeUnit.MILLISECONDS);
        int minimumDuration = 2; // we assume that the call takes at leas this long

        postCommandExpectOk(command(GET_NODE_INFO.getKey()));

        logger.info("Duration: {}", timer.totalTime(TimeUnit.MILLISECONDS) - start);
        Assertions.assertThat(timer.count()).isEqualTo(count + 1);
        Assertions.assertThat(timer.totalTime(TimeUnit.MILLISECONDS)).isGreaterThan(start + minimumDuration);
    }

    @Test
    public void givenGetNodeInfoWhenPostThenReturnIriNodeInfo() {
        String result = postCommandExpectOk(command(GET_NODE_INFO.getKey()));
        assertTrue(result, result.startsWith("{\"appName\":\"IRI\""));
    }

    @Test
    public void givenGetTipsWhenPostThenReturnTips() {
        String result = postCommandExpectOk(command("getTips"));
        assertTrue(result, result.startsWith("{\"hashes\":[\""));
    }

    @Test
    public void givenFindTransactionsWhenPostThenReturnHashes() {
        NodeInfo nodeInfo = postCommandExpectOk(command(GET_NODE_INFO.getKey()), NodeInfo.class);
        // might fail after snapshot
        String result = postCommandExpectOk(findTransactions(nodeInfo.getLatestSolidSubtangleMilestone()));
        assertThat(result).startsWith("{\"hashes\":[\"");
    }

    @Test
    public void givenGetTrytesWhenPostThenReturnHashes() {
        String result = postCommandExpectOk(getTrytes("BDHIGZXHYDNTVQVNIRYYLNPMZITPOUXBJPHCVXYTTURNR9XGAVKMAAJARQPFYPLWEXKEHXYAVLOE99999"));
        assertTrue(result, result.startsWith("{\"trytes\":[\""));
    }

    @Test
    public void givenGetInclusionStatesWhenPostThenReturnStates() {
        ResponseHashes tips = util.postCommand(command("getTips")).expectStatus().isOk().expectBody(ResponseHashes.class).returnResult().getResponseBody();
        assert tips != null && tips.getHashes() != null;
        String tx = tips.getHashes().get(0);
        String result = postCommandExpectOk(getInclusionStates(tx, tx));
        assertTrue(result, result.startsWith("{\"states\":[true]")); // same tx naturally includes itself
    }

    @Test
    public void givenGetBalancesWhenPostThenReturnBalances() {
        String address = "NAFAEIOFEAIJKKDMDVDEXKCDBIMSHIAFSMURU9WDFRVPKEZX9AKGYYOJSXKOAXRPNPJTDDRWXFRADKEDC";
        String result = postCommandExpectOk(getBalances(address));
        assertTrue(result, result.startsWith("{\"balances\":["));
    }

    @Test
    public void givenGetTransactionsToApproveWhenPostThenReturnTrunkAndBranchTx() {
        GetTransactionsToApprove gtta = getTransactionsToApprove();
        GetTransactionsToApproveResponse result = postCommandExpectOk(gtta, GetTransactionsToApproveResponse.class);
        Assertions.assertThat(result.getTrunkTransaction()).isNotBlank();
        Assertions.assertThat(result.getBranchTransaction()).isNotBlank();
    }

    @Test
    public void givenAttachToTangleWhenPostThenReturnTrytes() {
        GetTransactionsToApproveResponse gtta = postCommandExpectOk(getTransactionsToApprove(), GetTransactionsToApproveResponse.class);

        AttachToTangle att = attachToTangleCommand(gtta.getTrunkTransaction(), gtta.getBranchTransaction(),
                "CCWCXCGDEAGDDDPCADEAKDPCGDEARCFDTCPCHDTCSCEAQCMDEACCMDDDDDXCHAXACBUA9BSAEASBUCEAMDCDIDEAKDCDID9DSCEA9DXCZCTCEAHDCDEAVCTCHDEAXCBDEARCCDBDHDPCRCHDEAKDXCHDWCEAADTCEADD9DTCPCGDTCEAPCSCSCEAADTCEACDBDEANBXCGDRCCDFDSCQAEACDFDEATCADPCXC9DEAADTCEAPCHDEACCMDDDDDXCJBXCCDHDPCWCCDGDHDXCBDVCSACDFDVCSAEACCWCXCGDEAGDDDPCADEAKDPCGDEARCFDTCPCHDTCSCEAIDGDXCBDVCEAHDWCTCEAXCCDHDPCGDDDPCADSARCCDADEAGDDDPCADADTCFDEAXCBDEACDUCUC9DXCBDTCEAADCDSCTCQAEAHDWCTCFDTCUCCDFDTCEASBEAPCADEABDCDHDEAFDTCRCXCTCJDXCBDVCEAFDTCKDPCFDSCGDEAUCCDFDEAHDWCXCGDEAGDDDPCADSAEASBCDHDPCGDDDPCADSARCCDADEAKDXC9D9DEAFDTCGDIDADTCEAKDWCTCBDEAHDWCTCEAHDPCBDVC9DTCEARCPCBDEAWCPCBDSC9DTCEAHDWCTCEA9DCDPCSCSAJ9J9LAWBYBBCRBSBEAWBYBBCRBSBQAEACCOBACDCWBSBEANBOBBCDCLAJ9J9WBCDGDWCXCEAWBCDGDWCXCQAEACCTCFDIDADXCEANBTCGDIDEAMAEAEAEAEAEAEAEAEAEAEAEAQAEA9DXCHDSAEAGARBTC9D9DCDQAEACCWCXCGDEAXCGDEACCTCFDIDADXCGANAEAXCGDEAPCEATBPCDDPCBDTCGDTCEAADPCBDVCPCEAGDTCFDXCTCGDEAQCMDEAOBHDGDIDZCCDEAWBXCNDIDGDPCKDPCSAEASBHDEAWCPCGDEAQCTCTCBDEAGDTCFDXCPC9DXCNDTCSCEAXCBDEABCWCCDVCPCZCIDZCPCBDLAGDEAGDTCXCBDTCBDEAADPCBDVCPCEAADPCVCPCNDXCBDTCEALBXCVCEAMBCDADXCRCEABCDDXCFDXCHDGDEAQCTCHDKDTCTCBDEAWBPCFDRCWCEAWAUAVA9BEAPCBDSCEAPBTCQCFDIDPCFDMDEAWAUAVABBEAPCBDSCEAWCPCGDEAQCTCTCBDEARCCD9D9DTCRCHDTCSCEAXCBDEAPCEAGDXCBDVC9DTCEAHDPCBDZCEAQCCDBDEAJDCD9DIDADTCSAEAKBBDEAPCBDXCADTCEAPCSCPCDDHDPCHDXCCDBDEAWCPCGDEAQCTCTCBDEAPCBDBDCDIDBDRCTCSCSAJ9J9J9GBGBEAACTCUCTCFDTCBDRCTCGDSASASAEAGBGB9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999OFFLINE9SPAM9ADDRESS99999999999999999999999999999999999999999999999999999999TYPPI999999999999999999999999999AGUASPAM9DOT9COM9999TYPPI99DIYKIYD99999999999999999999FCJKIUHWJEGWDJBWEGOQNDCXNIHWVACQICUKG9KWJLHDQ9DXILKBBQJBOKLDUNXPNOHVKHPCCJGZCEAZCMINVQDIQFS9VMBNCOEWFTNMYV9VWNSIOIOHQBETNDFQGQD9MNSLX9PVFGCNUYCINCAZYF9OPSOBY99999QEPNICKIHZYYTPYDOGPA9QCHS9FHOLEBPSXZSRYLYJTTGYFMBDOBYHXAGWOSDTFNPPID9LFDZ9B999999IOTASPAM9DOT9COM9999TYPPI99YPNDIVXKE999999999MMMMMMMMMCAF9999999RBA99999999999999");

//        for (int i = 0; i < 30; i++) {
//            String result = util.postCommand("/", att, 5)
//                    .expectStatus().isOk()
//                    .expectBody(String.class).returnResult().getResponseBody();
//            logger.debug(result);
//            assertNotNull(result);
//            assertTrue(result, result.startsWith("{\"trytes\":"));
//        }

        String result = util.postCommand("/", att, 45)
                .expectStatus().isOk()
                .expectBody(String.class).returnResult().getResponseBody();
        logger.debug(result);
        assertNotNull(result);
        assertTrue(result, result.startsWith("{\"trytes\":"));
    }

    @Test
    public void givenInterruptAttachingToTangleWhenPostThenOk() {
        String result = postCommandExpectOk(command("interruptAttachingToTangle"));
        assertTrue(result, result.contains("duration"));
    }

    @Test
    public void givenBroadcastTransactionsWhenPostThenOk() {
        // test might fail after snapshot. fix generating trytes (do ATT for example).
        String trytes = "CCWCXCGDEAGDDDPCADEAKDPCGDEARCFDTCPCHDTCSCEAQCMDEACCMDDDDDXCHAXACBUA9BSAEASBUCEAMDCDIDEAKDCDID9DSCEA9DXCZCTCEAHDCDEAVCTCHDEAXCBDEARCCDBDHDPCRCHDEAKDXCHDWCEAADTCEADD9DTCPCGDTCEAPCSCSCEAADTCEACDBDEANBXCGDRCCDFDSCQAEACDFDEATCADPCXC9DEAADTCEAPCHDEACCMDDDDDXCJBXCCDHDPCWCCDGDHDXCBDVCSACDFDVCSAEACCWCXCGDEAGDDDPCADEAKDPCGDEARCFDTCPCHDTCSCEAIDGDXCBDVCEAHDWCTCEAXCCDHDPCGDDDPCADSARCCDADEAGDDDPCADADTCFDEAXCBDEACDUCUC9DXCBDTCEAADCDSCTCQAEAHDWCTCFDTCUCCDFDTCEASBEAPCADEABDCDHDEAFDTCRCXCTCJDXCBDVCEAFDTCKDPCFDSCGDEAUCCDFDEAHDWCXCGDEAGDDDPCADSAEASBCDHDPCGDDDPCADSARCCDADEAKDXC9D9DEAFDTCGDIDADTCEAKDWCTCBDEAHDWCTCEAHDPCBDVC9DTCEARCPCBDEAWCPCBDSC9DTCEAHDWCTCEA9DCDPCSCSAJ9J9LAWBYBBCRBSBEAWBYBBCRBSBQAEACCOBACDCWBSBEANBOBBCDCLAJ9J9WBCDGDWCXCEAWBCDGDWCXCQAEACCTCFDIDADXCEANBTCGDIDEAMAEAEAEAEAEAEAEAEAEAEAEAQAEA9DXCHDSAEAGARBTC9D9DCDQAEACCWCXCGDEAXCGDEACCTCFDIDADXCGANAEAXCGDEAPCEATBPCDDPCBDTCGDTCEAADPCBDVCPCEAGDTCFDXCTCGDEAQCMDEAOBHDGDIDZCCDEAWBXCNDIDGDPCKDPCSAEASBHDEAWCPCGDEAQCTCTCBDEAGDTCFDXCPC9DXCNDTCSCEAXCBDEABCWCCDVCPCZCIDZCPCBDLAGDEAGDTCXCBDTCBDEAADPCBDVCPCEAADPCVCPCNDXCBDTCEALBXCVCEAMBCDADXCRCEABCDDXCFDXCHDGDEAQCTCHDKDTCTCBDEAWBPCFDRCWCEAWAUAVA9BEAPCBDSCEAPBTCQCFDIDPCFDMDEAWAUAVABBEAPCBDSCEAWCPCGDEAQCTCTCBDEARCCD9D9DTCRCHDTCSCEAXCBDEAPCEAGDXCBDVC9DTCEAHDPCBDZCEAQCCDBDEAJDCD9DIDADTCSAEAKBBDEAPCBDXCADTCEAPCSCPCDDHDPCHDXCCDBDEAWCPCGDEAQCTCTCBDEAPCBDBDCDIDBDRCTCSCSAJ9J9J9GBGBEAACTCUCTCFDTCBDRCTCGDSASASAEAGBGB9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999OFFLINE9SPAM9ADDRESS99999999999999999999999999999999999999999999999999999999TYPPI999999999999999999999999999AGUASPAM9DOT9COM9999TYPPI99DIYKIYD99999999999999999999FCJKIUHWJEGWDJBWEGOQNDCXNIHWVACQICUKG9KWJLHDQ9DXILKBBQJBOKLDUNXPNOHVKHPCCJGZCEAZCIILOREWNQ9XBRGFVCSCWPFPUEUCRHVHKDXUDGX9RAJFF9IMHZKOWTFQWCPZVGAHXNZSPLLGZXLFPZ9999L9R9E9WXKHLWLGS9VYY99KDLPPV9XDZB9YMQILBUE9VVHAI9GOWHRNEPZGXNNYWMTQUONDGAJYIWZ9999IOTASPAM9DOT9COM9999TYPPI999HB9JWUME999999999MMMMMMMMMPOWSRVIO9KW99999LGVMEVKMMMM";
        String result = postCommandExpectOk(broadcastTransactions(trytes));
        assertTrue(result, result.contains("duration"));
    }

    @Test
    public void givenStoreTransactionsWhenPostThenOk() {
        // test might fail after snapshot. fix generating trytes (do ATT for example).
        String trytes = "CCWCXCGDEAGDDDPCADEAKDPCGDEARCFDTCPCHDTCSCEAQCMDEACCMDDDDDXCHAXACBUA9BSAEASBUCEAMDCDIDEAKDCDID9DSCEA9DXCZCTCEAHDCDEAVCTCHDEAXCBDEARCCDBDHDPCRCHDEAKDXCHDWCEAADTCEADD9DTCPCGDTCEAPCSCSCEAADTCEACDBDEANBXCGDRCCDFDSCQAEACDFDEATCADPCXC9DEAADTCEAPCHDEACCMDDDDDXCJBXCCDHDPCWCCDGDHDXCBDVCSACDFDVCSAEACCWCXCGDEAGDDDPCADEAKDPCGDEARCFDTCPCHDTCSCEAIDGDXCBDVCEAHDWCTCEAXCCDHDPCGDDDPCADSARCCDADEAGDDDPCADADTCFDEAXCBDEACDUCUC9DXCBDTCEAADCDSCTCQAEAHDWCTCFDTCUCCDFDTCEASBEAPCADEABDCDHDEAFDTCRCXCTCJDXCBDVCEAFDTCKDPCFDSCGDEAUCCDFDEAHDWCXCGDEAGDDDPCADSAEASBCDHDPCGDDDPCADSARCCDADEAKDXC9D9DEAFDTCGDIDADTCEAKDWCTCBDEAHDWCTCEAHDPCBDVC9DTCEARCPCBDEAWCPCBDSC9DTCEAHDWCTCEA9DCDPCSCSAJ9J9LAWBYBBCRBSBEAWBYBBCRBSBQAEACCOBACDCWBSBEANBOBBCDCLAJ9J9WBCDGDWCXCEAWBCDGDWCXCQAEACCTCFDIDADXCEANBTCGDIDEAMAEAEAEAEAEAEAEAEAEAEAEAQAEA9DXCHDSAEAGARBTC9D9DCDQAEACCWCXCGDEAXCGDEACCTCFDIDADXCGANAEAXCGDEAPCEATBPCDDPCBDTCGDTCEAADPCBDVCPCEAGDTCFDXCTCGDEAQCMDEAOBHDGDIDZCCDEAWBXCNDIDGDPCKDPCSAEASBHDEAWCPCGDEAQCTCTCBDEAGDTCFDXCPC9DXCNDTCSCEAXCBDEABCWCCDVCPCZCIDZCPCBDLAGDEAGDTCXCBDTCBDEAADPCBDVCPCEAADPCVCPCNDXCBDTCEALBXCVCEAMBCDADXCRCEABCDDXCFDXCHDGDEAQCTCHDKDTCTCBDEAWBPCFDRCWCEAWAUAVA9BEAPCBDSCEAPBTCQCFDIDPCFDMDEAWAUAVABBEAPCBDSCEAWCPCGDEAQCTCTCBDEARCCD9D9DTCRCHDTCSCEAXCBDEAPCEAGDXCBDVC9DTCEAHDPCBDZCEAQCCDBDEAJDCD9DIDADTCSAEAKBBDEAPCBDXCADTCEAPCSCPCDDHDPCHDXCCDBDEAWCPCGDEAQCTCTCBDEAPCBDBDCDIDBDRCTCSCSAJ9J9J9GBGBEAACTCUCTCFDTCBDRCTCGDSASASAEAGBGB9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999OFFLINE9SPAM9ADDRESS99999999999999999999999999999999999999999999999999999999TYPPI999999999999999999999999999AGUASPAM9DOT9COM9999TYPPI99DIYKIYD99999999999999999999FCJKIUHWJEGWDJBWEGOQNDCXNIHWVACQICUKG9KWJLHDQ9DXILKBBQJBOKLDUNXPNOHVKHPCCJGZCEAZCIILOREWNQ9XBRGFVCSCWPFPUEUCRHVHKDXUDGX9RAJFF9IMHZKOWTFQWCPZVGAHXNZSPLLGZXLFPZ9999L9R9E9WXKHLWLGS9VYY99KDLPPV9XDZB9YMQILBUE9VVHAI9GOWHRNEPZGXNNYWMTQUONDGAJYIWZ9999IOTASPAM9DOT9COM9999TYPPI999HB9JWUME999999999MMMMMMMMMPOWSRVIO9KW99999LGVMEVKMMMM";
        String result = postCommandExpectOk(storeTransactions(trytes));
        assertTrue(result, result.contains("duration"));
    }

    @Test
    public void givenWereAddressesSpentFromWhenPostThenReturnStates() {
        String address = "UIQJD9ADKNZYSL9CCJULJQNWTZ9FWGVQMUFSXDK9SJVINWQOVGKXLMSNWJGCCMAQRUTSETKUSCFXGMTHA";
        String result = postCommandExpectOk(wereAddressesSpentFrom(address));
        assertTrue(result, result.startsWith("{\"states\":"));
    }

    @Test
    public void givenCheckConsistencyWhenPostThenOk() {
        ResponseHashes tips = util.postCommand(command("getTips"))
                .expectStatus().isOk()
                .expectBody(ResponseHashes.class).returnResult().getResponseBody();
        assert tips != null && tips.getHashes() != null;
        String tail = tips.getHashes().get(0);
//        String tail = "MFGXY9RYV9NSIZTBMCLTUFCDGKJ9GUXJEZPDLAXKPKVBXXKAURSMXTKTECSXYLOWWTMBQAZZKZUXZ9999";
        String result = postCommandExpectOk(checkConsistency(tail));
        assertTrue(result, result.startsWith("{\"state\":")); // could be false, too. in case of max depth, ...
    }

    @Test
    public void givenGetMissingTransactionsWhenPostThenReturnTips() {
        String result = postCommandExpectOk(command("getMissingTransactions"));
        assertTrue(result, result.startsWith("{\"hashes\":["));
    }

    @Test
    public void givenGetNeighborsWhenPostThenReturnNeighbors() {
        String result = postCommandExpectOk(getNeighborsCommand());
        assertTrue(result, result.startsWith("{\"neighbors\":["));
    }

    @Test
    public void givenAddNeighborsWhenPostThenReturnNumberOfAdded() {
        String result = postCommandExpectOk(addNeighborsCommand("udp://8.8.8.8:14600"));
        assertTrue(result, result.startsWith("{\"addedNeighbors\":"));
        util.postCommand(removeNeighborsCommand("udp://8.8.8.8:14600")).expectStatus().isOk(); // clean up
    }

    @Test
    public void givenRemoveNeighborsWhenPostThenReturnNumberOfRemoved() {
        String result = postCommandExpectOk(removeNeighborsCommand("udp://8.8.8.8:14600"));
        assertTrue(result, result.startsWith("{\"removedNeighbors\":"));
    }

    // Attach to tangle validation errors:

    @Test
    public void givenEmptyAttachToTangleThenSimulateIriBehaviour() {
        AttachToTangle att = attachToTangleCommand(null, null, null);
        att.setMinWeightMagnitude(1);
        String result = util.postCommand(att)
                .expectStatus()
                .isBadRequest()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBody(String.class).returnResult().getResponseBody();
        logger.info(result);
        assertThat(result).contains("{\"error\":\"Invalid parameters\",\"duration\":");
    }

    @Test
    public void givenInvalidWeightWhenAttachToTangleThenSimulateIriBehaviour() {
        AttachToTangle att = attachToTangleCommand("foo", "bar", "foobar");
        att.setMinWeightMagnitude(1);
        String result = util.postCommand(att)
                .expectStatus()
                .isBadRequest()
                .expectBody(String.class).returnResult().getResponseBody();
        logger.info(result);
        assertThat(result).contains("{\"error\":\"Invalid parameters\",\"duration\":");
    }

    private <T> T postCommandExpectOk(Object command, Class<T> responseClass) {
        if (command instanceof IriCommand) {
            logger.debug("request: {}", toJson((IriCommand) command));
        } else {
            logger.debug("request: {}", command);
        }
        T result = util.postCommand(command)
                .expectStatus().isOk()
                .expectBody(responseClass).returnResult().getResponseBody();
        assertNotNull(result);
        logger.debug("result: {}", result.toString());
        return result;
    }

    private String postCommandExpectOk(Object command) {
        return postCommandExpectOk(command, String.class);
    }


    private String toJson(IriCommand command) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(command);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}