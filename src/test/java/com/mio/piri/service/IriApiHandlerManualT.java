/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service;

import com.mio.piri.nodes.api.dtos.RegisterNodeRequest;
import com.mio.piri.nodes.api.dtos.RegisterNodeResponse;
import com.mio.piri.nodes.api.dtos.UnregisterNodeResponse;
import com.mio.piri.commands.GetNodeInfo;
import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.commands.response.ResponseDtoBuilder;
import com.mio.piri.util.IriClientTestUtility;
import com.mio.piri.util.JsonTestUtil;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.logging.Level;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("SpellCheckingInspection")
@Ignore("For manual testing only.")
@RunWith(SpringRunner.class)
public class IriApiHandlerManualT {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private MockWebServer server;

    private WebClient webClient;

    private WebTestClient webTestClient;

    private IriClientTestUtility util;

    private final String syncedNodeInfo = JsonTestUtil.toJson(ResponseDtoBuilder.nodeInfoBuilder().latestMilestoneIndex(42).latestSolidSubtangleMilestoneIndex(42).build());

    @Before
    public void startServer() throws IOException {
        webClient = WebClient.create("http://localhost:8080");
        webTestClient = WebTestClient.bindToServer().baseUrl("http://localhost:8080").build();
        util = new IriClientTestUtility(webTestClient);
        server = new MockWebServer();
        server.start(1234);
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));
    }

    @After
    public void stopServer() throws IOException {
        server.shutdown();
    }

    @Test
    public void registerNode() {
        RegisterNodeResponse responseBody = util.
                postCommand("/nodes", new RegisterNodeRequest("test", "http://localhost:1234", true), 10)
                .expectStatus().isCreated()
                .expectBody(RegisterNodeResponse.class)
                .returnResult().getResponseBody();
        assertThat(responseBody).isNotNull();
        logger.info(responseBody.getPassword());
    }

    @Test
    public void unregisterNode() {
        String key = "VZKQWOHOUJJ9BMARNTICTFWCXR9BZCFSNPSVOCOYOEWUOBLTDPYUKKGVWQNLBCQCPNKHO9HMYILMAGYDV";
        UnregisterNodeResponse response = webTestClient.delete().uri("/nodes/" + key).exchange()
                .expectStatus().isOk()
                .expectBody(UnregisterNodeResponse.class).returnResult().getResponseBody();
        assertThat(response).isNotNull();
        logger.info(response.toString());
    }

    @Test
    public void runTestWithWebTestClient() {
        java.util.logging.Logger.getLogger(MockWebServer.class.getName()).setLevel(Level.WARNING);
        for (int i = 0; i < 100000; i++) {
            if (i%100 == 0) {
                logger.info("Sent [{}] requests.", i);
            }
            prepareResponse(response -> response
                    .setHeader("Content-Type", "application/json")
                    .setBody(syncedNodeInfo));
            util.postCommand(new GetNodeInfo()).expectStatus().isOk();
        }

    }

    @Test
    public void runTestWithWebClient() throws InterruptedException {
        java.util.logging.Logger.getLogger(MockWebServer.class.getName()).setLevel(Level.WARNING);
        Flux.interval(Duration.ofMillis(1)).subscribe(
                count -> {
                    if (count % 1000 == 0) {
                        System.out.print("\n" + count + ".");
                    }
                    prepareResponse(response -> response
                            .setHeader("Content-Type", "application/json")
                            .setBody(syncedNodeInfo));
                    webClient.post()
                            .contentType(MediaType.APPLICATION_JSON)
                            .accept(MediaType.APPLICATION_JSON)
                            .syncBody(new GetNodeInfo())
                            .retrieve()
                            .bodyToMono(NodeInfo.class)
                            .subscribe(
                                    nodeInfo -> { },
                                    t -> logger.error(t.toString(), t)
                            );
                }
        );
        TimeUnit.MINUTES.sleep(10);
    }

    private void prepareResponse(Consumer<MockResponse> consumer) {
        MockResponse response = new MockResponse();
        consumer.accept(response);
        this.server.enqueue(response);
    }



}