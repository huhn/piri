/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service.validation;

import com.mio.piri.commands.GetTransactionsToApprove;
import com.mio.piri.commands.IriCommand;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.env.Environment;
import org.springframework.validation.Errors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class GetTransactionsToApproveValidatorTest {

    private GetTransactionsToApproveValidator validator;

    private final MinMaxValidator minMaxValidator = mock(MinMaxValidator.class);
    private final GetTransactionsToApprove command = mock(GetTransactionsToApprove.class);
    private final Errors errors = mock(Errors.class);

    @Before
    public void initEnvironment() {
        Environment env = mock(Environment.class);
        when(env.getProperty("iri.getTransactionsToApprove.depth.max", Integer.class, GetTransactionsToApprove.DEFAULT_DEPTH_MAX))
                .thenReturn(3);
        when(env.getProperty("iri.getTransactionsToApprove.depth.min", Integer.class, GetTransactionsToApprove.DEFAULT_DEPTH_MIN))
                .thenReturn(2);
        validator = new GetTransactionsToApproveValidator(env, minMaxValidator);
    }

    @Test
    public void givenEmptyWhenValidateThenDoNotThrow() {
        validator.validate(new GetTransactionsToApprove(), errors);
    }

    @Test
    public void givenAttachToTangleWhenSupportsThenTrue() {
        assertThat(validator.supports(GetTransactionsToApprove.class)).isTrue();
    }

    @Test
    public void givenOtherClassWhenSupportsThenFalse() {
        assertThat(validator.supports(Object.class)).isFalse();
        assertThat(validator.supports(IriCommand.class)).isFalse();
    }

    @Test
    public void whenValidateThenCallMinMaxValidator() {
        when(command.getDepth()).thenReturn(1);
        validator.validate(command, errors);
        verify(minMaxValidator).validate(1, 2, 3, "depth", errors);
    }

}