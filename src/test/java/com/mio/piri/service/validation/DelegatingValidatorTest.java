/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service.validation;

import com.mio.piri.commands.GetNodeInfo;
import com.mio.piri.commands.IriCommand;
import org.junit.Test;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class DelegatingValidatorTest {

    private final Validator validator1 = mock(Validator.class);
    private final Validator validator2 = mock(Validator.class);

    private final DelegatingValidator validator = new DelegatingValidator(IriCommand.class, validator1, validator2);

    @Test
    public void givenValidClassWhenSupportsThenReturnTrue() {
        assertThat(validator.supports(GetNodeInfo.class)).isTrue();
    }

    @Test
    public void givenUnsupportedClassWhenSupportsThenReturnFalse() {
        assertThat(validator.supports(Object.class)).isFalse();
    }

    @Test
    public void whenValidateThenCallMatchingValidator() {
        when(validator1.supports(any())).thenReturn(true);
        validator.validate(mock(Object.class), mock(Errors.class));
        verify(validator1).validate(any(), any(Errors.class));
        verify(validator2).supports(any());
        verifyNoMoreInteractions(validator2);
    }

    @Test
    public void whenValidateThenCallAllMatchingValidators() {
        when(validator1.supports(any())).thenReturn(true);
        when(validator2.supports(any())).thenReturn(true);
        validator.validate(mock(Object.class), mock(Errors.class));
        verify(validator1).validate(any(), any(Errors.class));
        verify(validator2).validate(any(), any(Errors.class));
    }


}