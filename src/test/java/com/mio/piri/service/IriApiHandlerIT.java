/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service;

import com.mio.piri.commands.GetNodeInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"iri.nodes="}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IriApiHandlerIT extends AbstractIriApiHandlerTest {

    @Test
    public void givenGetNodeInfoWhenPostThenReturnIriNodeInfo() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        String result = util.postCommand(new GetNodeInfo())
                .expectStatus().isOk()
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("latestMilestoneIndex");
    }

}