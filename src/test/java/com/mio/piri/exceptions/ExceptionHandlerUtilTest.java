/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.exceptions;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.support.WebExchangeBindException;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ExceptionHandlerUtilTest {

    @Test
    public void shouldExtractValidationErrorMessage() {
        WebExchangeBindException ex = mock(WebExchangeBindException.class);
        when(ex.getReason()).thenReturn("reason");
        FieldError fe1 = mock(FieldError.class);
        when(fe1.getField()).thenReturn("field1");
        when(fe1.getDefaultMessage()).thenReturn("msg1");
        FieldError fe2 = mock(FieldError.class);
        when(fe2.getField()).thenReturn("field2");
        when(fe2.getDefaultMessage()).thenReturn("msg2");
        when(ex.getFieldErrors()).thenReturn(Arrays.asList(fe1, fe2));
        assertThat(ExceptionHandlerUtil.extractValidationErrorMessage(ex)).contains("reason: [field1 msg1, field2 msg2]");
    }

    @Test
    public void whenConvertToResponseExceptionThenNestOriginalException() {
        WebExchangeBindException ex = mock(WebExchangeBindException.class);
        when(ex.getStatus()).thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(ExceptionHandlerUtil.convertToResponseException(ex, "boom")).hasMessageContaining("boom");
        assertThat(ExceptionHandlerUtil.convertToResponseException(ex, "foo")).hasNoCause();
        assertThat(ExceptionHandlerUtil.convertToResponseException(ex, "foo").getStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(ExceptionHandlerUtil.convertToResponseException(ex, "foo")).hasMessageContaining("500");
    }

}