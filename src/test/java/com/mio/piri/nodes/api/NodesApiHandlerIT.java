/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.api;

import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.nodes.api.dtos.RegisterNodeRequest;
import com.mio.piri.nodes.api.dtos.RegisterNodeResponse;
import com.mio.piri.nodes.api.dtos.UnregisterNodeRequest;
import com.mio.piri.nodes.api.dtos.UnregisterNodeResponse;
import com.mio.piri.util.JsonTestUtil;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.io.IOException;
import java.util.UUID;
import java.util.function.Consumer;

import static com.mio.piri.WebSecurityConfiguration.ROLE_NODE_ADMIN;
import static com.mio.piri.commands.response.ResponseDtoBuilder.nodeInfoBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.springSecurity;
import static org.springframework.web.reactive.function.client.ExchangeFilterFunctions.basicAuthentication;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"piri.nodes.registration.secure=false"}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NodesApiHandlerIT {

    private static final String NODE_URL = "http://localhost:1234";
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ApplicationContext context;

    private MockWebServer server; // for health check
    private WebTestClient webTestClient;

    private final String syncedNodeInfo = JsonTestUtil.toJson(nodeInfoBuilder()
            .latestMilestoneIndex(42)
            .latestSolidSubtangleMilestoneIndex(41)
            .neighbors(3)
            .build());

    private final String unsyncedNodeInfo = JsonTestUtil.toJson(nodeInfoBuilder()
            .latestMilestoneIndex(42)
            .latestSolidSubtangleMilestoneIndex(39)
            .build());

    private String key = null; // registering node dirties context. We need this to clean up even if test fails.

    // needed because endpoint is secured
    @Before
    public void setUpSecurity() {
        webTestClient = WebTestClient
                .bindToApplicationContext(context)
                .apply(springSecurity())
                .configureClient()
                .filter(basicAuthentication())
                .build();
    }

    @Before
    public void setUpTestNode() throws IOException {
        server = new MockWebServer();
        server.start(1234);
    }

    @After
    public void shutdown() throws Exception {
        if (key != null) {
            unregisterNodeOk(key);
        }
        this.server.shutdown();
    }

    @Test
    public void whenRegisterThenReturn201() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        key = registerNodeCreated(UUID.randomUUID().toString());
        assertThat(key).isNotBlank();
        assertThat(key).hasSize(81);
    }

    @Test
    public void whenUnregisterThenReturnNodeName() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        String key = registerNodeCreated("foo");
        UnregisterNodeResponse response = unregisterNodeOk(key);
        assertThat(response.getName()).isEqualTo("foo");
    }

    @Test
    public void givenSameNameWhenRegisterThenReturn409() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        key = registerNodeCreated("foo");

        String result = registerNode(new RegisterNodeRequest("foo", "http://localhost:666", true))
                .expectStatus().isEqualTo(HttpStatus.CONFLICT)
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("already registered");
        logger.debug(result);

    }

    @Test
    public void givenSamePasswordWhenRegisterThenReturn409() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        key = registerNodeCreated("foo");

        String result = registerNode(new RegisterNodeRequest("bar", "http://localhost:666", true, key))
                .expectStatus().isEqualTo(HttpStatus.CONFLICT)
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("Password already taken.");
        logger.debug(result);

    }

    @Test
    public void givenSameUrlWhenRegisterThenReturn409() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        key = registerNodeCreated("foo");

        String result = registerNode(new RegisterNodeRequest("bar", "http://localhost:1234", true))
                .expectStatus().isEqualTo(HttpStatus.CONFLICT)
                .expectBody(String.class).returnResult().getResponseBody();
        assertThat(result).contains("already registered");
        logger.debug("Result: {}", result);

    }

    @Test
    public void givenInvalidCharacterWhenRegisterThen400() {
        EntityExchangeResult<String> result = registerNode(new RegisterNodeRequest("hel<lo.world", "http://localhost:1234", true))
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult();
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
        assertThat(result.getResponseBody()).contains("name must match");
    }

    @Test
    public void givenInvalidLengthWhenRegisterThen400() {
        EntityExchangeResult<String> result = registerNode(new RegisterNodeRequest("1234567890123456789012345678901234567890", "http://localhost:1234", true))
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult();
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
        assertThat(result.getResponseBody()).contains("size must be");
    }

    @Test
    public void givenBlankNameWhenRegisterThen400() {
        EntityExchangeResult<String> result = registerNode(new RegisterNodeRequest(" ", "http://localhost:1234", true))
                .expectStatus().isBadRequest()
                .expectBody(String.class).returnResult();
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
        assertThat(result.getResponseBody()).contains("name must not be blank");
    }

    @Test
    public void givenInvalidUrlWhenRegisterThen400() {
        EntityExchangeResult<Object> result = registerNode(new RegisterNodeRequest("foo", "no.valid.url", true))
                .expectStatus().isBadRequest()
                .expectBody(Object.class).returnResult();
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
    }

    @Test
    public void givenShortPasswordWhenRegisterThen400() {
        EntityExchangeResult<Object> result = registerNode(new RegisterNodeRequest("foo", "no.valid.url", true, "blah"))
                .expectStatus().isBadRequest()
                .expectBody(Object.class).returnResult();
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
    }

    @Test
    public void givenInvalidPasswordWhenRegisterThen400() {
        EntityExchangeResult<Object> result = registerNode(new RegisterNodeRequest("foo", "no.valid.url", true, "blah blah blah!"))
                .expectStatus().isBadRequest()
                .expectBody(Object.class).returnResult();
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
    }

    @Test
    public void givenUnreachableNodeWhenRegisterThen422() {
        EntityExchangeResult<Object> result = registerNode(new RegisterNodeRequest("foo", "http://localhost:666", true))
                .expectStatus().isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY)
                .expectBody(Object.class).returnResult();
        assertThat(result.getResponseBody()).isNotNull();
        assertThat(result.getResponseBody().toString()).contains("not available");
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
    }

    @Test
    public void givenUnsyncedNodeWhenRegisterThen422() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(unsyncedNodeInfo));

        EntityExchangeResult<Object> result = registerNode(new RegisterNodeRequest("foo", "http://localhost:1234", true))
                .expectStatus().isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY)
                .expectBody(Object.class).returnResult();
        assertThat(result.getResponseBody()).isNotNull();
        assertThat(result.getResponseBody().toString()).contains("not synced");
        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
    }

// No idea how to test this...
//    @Test
//    public void givenUnexpectedErrorWhenRegisterThen500() {
//        prepareResponse(response -> response
//                .setHeader("Content-Type", "application/json")
//                .setBody(unsyncedNodeInfo));
//        EntityExchangeResult<Object> result = register(new RegisterNodeRequest("foo", "http://localhost:1234", true))
//                .expectStatus().isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
//                .expectBody(Object.class).returnResult();
//        assertThat(result.getResponseBody()).isNotNull();
//        logger.debug("Result: {} => {}", result.getStatus(), result.getResponseBody());
//    }

    @Test
    public void whenGetNodesThenReturnNodeInfos() {
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        key = registerNodeCreated("foo");
        WebTestClient.ResponseSpec response = getNodes();

        String body = response.expectBody(String.class).returnResult().getResponseBody();
        assertThat(body).contains("name\":\"foo");
        assertThat(body).contains("appName");
        assertThat(body).contains("appVersion");
        assertThat(body).contains("key\":\"");
        assertThat(body).contains("neighbors\":3");
        assertThat(body).contains("milestone\":42");
        assertThat(body).contains("solidMilestone\":41");
        assertThat(body).contains("delay\":1");
        assertThat(body).contains("pow\":");
        assertThat(body).doesNotContain("synced");
        assertThat(body).contains("available\":true");
        assertThat(body).contains("commands");
        assertThat(body).contains("millis");
        assertThat(body).contains("count");

        logger.info("Response: {}", body);
    }

    @Test
    public void whenGetNodeByNameThenReturnNodeInfo() {

        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        key = registerNodeCreated("foo");
        WebTestClient.ResponseSpec response = getNode("foo");
        NodeInfo ni = response.expectStatus().isOk().expectBody(NodeInfo.class).returnResult().getResponseBody();
        assertThat(ni).isNotNull();
        logger.info("Response: {}", ni);
    }

    @Test
    public void givenUnknownNameWhenGetNodeByNameThenReturnNotFound() {
        getNode("unknown").expectStatus().isNotFound();
    }

    @WithMockUser(roles = ROLE_NODE_ADMIN)
    @Test
    public void whenUnregisterByPostThenReturnOk() {

        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo));

        registerNodeCreated("foo");
        WebTestClient.ResponseSpec responseSpec = unregisterNode(new UnregisterNodeRequest("foo"));
        UnregisterNodeResponse response = responseSpec
                .expectStatus().isOk()
                .expectBody(UnregisterNodeResponse.class).returnResult().getResponseBody();
        assertThat(response).isNotNull();
        assertThat(response.getName()).isEqualTo("foo");
        assertThat(response.getUrl()).isEqualTo(NODE_URL);
    }

    @WithMockUser(roles = ROLE_NODE_ADMIN)
    @Test
    public void givenNoNameWhenUnregisterByPostThenError() {
        unregisterNode(new UnregisterNodeRequest(null)).expectStatus().isBadRequest();
    }

    @WithMockUser(roles = ROLE_NODE_ADMIN)
    @Test
    public void givenUnknownNameWhenUnregisterByPostThenNotFound() {
        unregisterNode(new UnregisterNodeRequest("blah")).expectStatus().isNotFound();
    }

    private String registerNodeCreated(String name) {
        RegisterNodeResponse responseBody = registerNode(new RegisterNodeRequest(name, NODE_URL, true))
                .expectStatus().isCreated()
                .expectBody(RegisterNodeResponse.class)
                .returnResult().getResponseBody();
        assertThat(responseBody).isNotNull();
        assertThat(responseBody.getName()).isEqualTo(name);
        assertThat(responseBody.getUrl()).isEqualTo("http://localhost:1234");
        logger.debug("Response: {}", JsonTestUtil.toJson(responseBody));
        return responseBody.getPassword();

    }

    private WebTestClient.ResponseSpec registerNode(Object request) {
        return webTestClient
                .post()
                .uri("/nodes")
                .syncBody(request)
                .exchange();
    }

    private WebTestClient.ResponseSpec getNodes() {
        return webTestClient
                .get()
                .uri("/nodes")
                .exchange();
    }

    private WebTestClient.ResponseSpec getNode(String name) {
        return webTestClient
                .get()
                .uri("/nodes/{name}", name)
                .accept(MediaType.APPLICATION_JSON)
                .exchange();
    }

    private WebTestClient.ResponseSpec unregisterNode(UnregisterNodeRequest request) {
        return webTestClient
                .post()
                .uri("/nodes/unregister")
                .syncBody(request)
                .exchange();
    }

    private UnregisterNodeResponse unregisterNodeOk(String key) {
        UnregisterNodeResponse response = webTestClient.delete().uri("/nodes/" + key).exchange()
                .expectStatus().isOk()
                .expectBody(UnregisterNodeResponse.class).returnResult().getResponseBody();
        logger.debug("Response: {}", JsonTestUtil.toJson(response));
        return response;
    }

    private void prepareResponse(Consumer<MockResponse> consumer) {
        MockResponse response = new MockResponse();
        consumer.accept(response);
        this.server.enqueue(response);
    }

}
