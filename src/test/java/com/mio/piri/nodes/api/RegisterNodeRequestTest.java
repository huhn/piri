/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.api;

import com.mio.piri.nodes.api.dtos.RegisterNodeRequest;
import org.junit.Test;

import java.util.UUID;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;


public class RegisterNodeRequestTest {

    @Test
    public void whenValidNameThenMatchRegexp() {
        Pattern nodeName = Pattern.compile(RegisterNodeRequest.NAME_REGEXP);
        assertThat(nodeName.matcher("Test").matches()).isTrue();
        assertThat(nodeName.matcher("some-test-node").matches()).isTrue();
        assertThat(nodeName.matcher("Hello_World").matches()).isTrue();
        assertThat(nodeName.matcher("Hello.World").matches()).isTrue();
        assertThat(nodeName.matcher("foo.bar.com").matches()).isTrue();
        assertThat(nodeName.matcher(UUID.randomUUID().toString()).matches()).isTrue();
        assertThat(nodeName.matcher("Hello World").matches()).isTrue();
    }

    @Test
    public void whenValidNameThenDoNotMatchRegexp() {
        Pattern nodeName = Pattern.compile(RegisterNodeRequest.NAME_REGEXP);
        assertThat(nodeName.matcher("Test ").matches()).isFalse();
        assertThat(nodeName.matcher(" Test").matches()).isFalse();
        assertThat(nodeName.matcher(" ").matches()).isFalse();
        assertThat(nodeName.matcher("HelloWorld!").matches()).isFalse();
        assertThat(nodeName.matcher("").matches()).isFalse();
        assertThat(nodeName.matcher("Node<script>alert(1)</script>").matches()).isFalse();
        assertThat(nodeName.matcher("foo:bar").matches()).isFalse();
        assertThat(nodeName.matcher("a&b").matches()).isFalse();
        assertThat(nodeName.matcher("http://foo.bar.com").matches()).isFalse();
        assertThat(nodeName.matcher("1+1=3").matches()).isFalse();
    }


    @Test
    public void whenValidPasswordThenMatchRegexp() {
        Pattern nodeName = Pattern.compile(RegisterNodeRequest.PASS_REGEXP);
        assertThat(nodeName.matcher("Test").matches()).isTrue();
        assertThat(nodeName.matcher("foo#bar!com.").matches()).isTrue();
        assertThat(nodeName.matcher(UUID.randomUUID().toString()).matches()).isTrue();
        assertThat(nodeName.matcher("HelloWorld!").matches()).isTrue();
        assertThat(nodeName.matcher("DANWXQHUKKVWQYOUDWPRLRHXEYDQLCGUTQRDXTMLQDYPRYBIXJKWBAZQPOOORMMIYAFAJIRIAXRPLDOKR").matches()).isTrue();
        assertThat(nodeName.matcher("Node<script>alert(1)</script>").matches()).isTrue();
        assertThat(nodeName.matcher("foo:bar").matches()).isTrue();
        assertThat(nodeName.matcher("a&b").matches()).isTrue();
        assertThat(nodeName.matcher("http://foo.bar.com").matches()).isTrue();
        assertThat(nodeName.matcher("1+1=3").matches()).isTrue();

        assertThat(nodeName.matcher("Test ").matches()).isFalse();
        assertThat(nodeName.matcher(" Test").matches()).isFalse();
        assertThat(nodeName.matcher(" ").matches()).isFalse();
        assertThat(nodeName.matcher("").matches()).isFalse();
    }

    @Test
    public void whenInvalidPasswordThenDoNotMatchRegexp() {
        Pattern nodeName = Pattern.compile(RegisterNodeRequest.PASS_REGEXP);
        assertThat(nodeName.matcher("Test ").matches()).isFalse();
        assertThat(nodeName.matcher(" Test").matches()).isFalse();
        assertThat(nodeName.matcher("Hi there").matches()).isFalse();
        assertThat(nodeName.matcher("").matches()).isFalse();
    }


}