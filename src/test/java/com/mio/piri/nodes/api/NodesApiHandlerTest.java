/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.api;

import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.exceptions.CannotRegisterNode;
import com.mio.piri.metrics.MeterFactory;
import com.mio.piri.nodes.*;
import com.mio.piri.nodes.api.dtos.*;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.vavr.collection.HashMap;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class NodesApiHandlerTest {

    @Mock
    private NodeRegistry registry;

    @Mock
    private NodesRepository repository;

    @Spy
    private MeterRegistry meterRegistry = new SimpleMeterRegistry();

    @Mock
    private RandomKeyGenerator randomKeyGenerator;

    @Mock
    private HashGenerator hashGenerator;

    @InjectMocks
    private final NodesApiHandler clientApi = new NodesApiHandler();

    // TODO add test for registering synced/unsynced
    // test that health check info is set

    @Before
    public void initMocks() {
        when(repository.getNodeName(anyString())).thenReturn(Option.none());
    }

    @Test
    public void givenConflictingPasswordWhenRegisterThenThrow() {
        when(hashGenerator.sha256("boom")).thenReturn("hash");
        when(repository.containsKey("hash")).thenReturn(true);
        RegisterNodeRequest request = new RegisterNodeRequest("foo", "bar", false, "boom");
        Try<Mono<ResponseEntity<RegisterNodeResponse>>> result = Try.of(() -> clientApi.register(request));
        assertThat(result.isFailure()).isTrue();
        assertThat(result.getCause()).isInstanceOf(CannotRegisterNode.class);
        CannotRegisterNode ex = (CannotRegisterNode) result.getCause();
        assertThat(ex.getMessage()).contains("Password already taken.");
    }

    @Test
    public void givenConflictingPartialPasswordWhenRegisterThenThrow() {
        when(hashGenerator.sha256("boom")).thenReturn("hash");
        when(hashGenerator.publicKey("hash")).thenReturn("ha");
        when(repository.containsKeyStartingWith("ha")).thenReturn(true);
        RegisterNodeRequest request = new RegisterNodeRequest("foo", "bar", false, "boom");
        Try<Mono<ResponseEntity<RegisterNodeResponse>>> result = Try.of(() -> clientApi.register(request));
        assertThat(result.isFailure()).isTrue();
        assertThat(result.getCause()).isInstanceOf(CannotRegisterNode.class);
        CannotRegisterNode ex = (CannotRegisterNode) result.getCause();
        assertThat(ex.getMessage()).contains("Password already taken.");
    }

    @Test
    public void givenNoPasswordWhenRegisterThenGenerate() {
        when(randomKeyGenerator.generateNodePassword()).thenReturn("generated");
        when(hashGenerator.sha256("generated")).thenReturn("hash");
        when(hashGenerator.publicKey("hash")).thenReturn("fooKey");
        IriNode node = mock(IriNode.class);
        when(node.queryNodeHealth()).thenReturn(Mono.just(new NodeInfo()));
        when(registry.registerIriNode("foo", "bar", "fooKey", false)).thenReturn(node);
        // test
        RegisterNodeRequest request = new RegisterNodeRequest("foo", "bar", false);
        // verify
        Try<Mono<ResponseEntity<RegisterNodeResponse>>> result = Try.of(() -> clientApi.register(request));
        assertThat(result.isSuccess()).withFailMessage(result.toString()).isTrue();
    }

    @Test
    public void whenUnregisterWithPasswordThenReturnNodeName() {
        when(hashGenerator.sha256(anyString())).thenReturn("hash");
        when(repository.getNodeName("hash")).thenReturn(Option.of("bar"));
        IriNode unregistered = mock(IriNode.class);
        when(unregistered.getName()).thenReturn("bar");
        when(registry.unregisterNode("bar")).thenReturn(unregistered);

        Try<ResponseEntity<UnregisterNodeResponse>> result = Try.of(() -> clientApi.unregisterWithPassword("foo"));
        assertThat(result.isSuccess()).isTrue();
        assertThat(result.get().getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.get().getBody()).isNotNull();
        //noinspection ConstantConditions
        assertThat(result.get().getBody().getName()).isEqualTo("bar");
    }

    @Test
    public void givenUnknownNameWhenUnregisterWithPasswordThenReturnNotFound() {
        when(hashGenerator.sha256(anyString())).thenReturn("hash");
        when(repository.getNodeName("hash")).thenReturn(Option.none());
        when(repository.getNodeName("foo")).thenReturn(Option.of("bar"));
        when(registry.unregisterNode("bar")).thenReturn(null);

        Try<ResponseEntity<UnregisterNodeResponse>> result = Try.of(() -> clientApi.unregisterWithPassword("foo"));
        assertThat(result.isFailure()).isTrue();
        assertThat(result.getCause()).isInstanceOf(ResponseStatusException.class);
        ResponseStatusException ex = (ResponseStatusException) result.getCause();
        assertThat(ex.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(ex.getReason()).contains("Node not found.");
    }

    @Test
    public void givenUnknownPasswordWhenUnregisterThenReturnNotFound() {
        when(hashGenerator.sha256("foo")).thenReturn("hash");
        Try<ResponseEntity<UnregisterNodeResponse>> result = Try.of(() -> clientApi.unregisterWithPassword("foo"));
        assertThat(result.isFailure()).isTrue();
        assertThat(result.getCause()).isInstanceOf(ResponseStatusException.class);
        ResponseStatusException ex = (ResponseStatusException) result.getCause();
        assertThat(ex.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(ex.getReason()).contains("There is no node for this password.");
    }

    @Test
    public void whenUnregisterWithPasswordThenTryPassword() {
        when(hashGenerator.sha256(anyString())).thenReturn("hash");
        when(repository.getNodeName("hash")).thenReturn(Option.none()); // don't find by hash
        when(repository.getNodeName("foo")).thenReturn(Option.of("node-name")); // find by password
        when(registry.unregisterNode("node-name")).thenReturn(mock(IriNode.class)); // unregister by name

        Try<ResponseEntity<UnregisterNodeResponse>> result = Try.of(() -> clientApi.unregisterWithPassword("foo"));
        assertThat(result.isSuccess()).withFailMessage(result.toString()).isTrue();
        assertThat(result.get().getStatusCode()).isEqualTo(HttpStatus.OK);

        verify(repository).removeNodeByName("node-name");
    }

    @Test
    public void whenUnregisterWithPasswordThenTryHashedPassword() {
        when(hashGenerator.sha256(anyString())).thenReturn("hash");
        when(repository.getNodeName("hash")).thenReturn(Option.of("node-name")); // find by hash
        when(registry.unregisterNode("node-name")).thenReturn(mock(IriNode.class)); // unregister by name

        Try<ResponseEntity<UnregisterNodeResponse>> result = Try.of(() -> clientApi.unregisterWithPassword("foo"));
        assertThat(result.isSuccess()).withFailMessage(result.toString()).isTrue();
        assertThat(result.get().getStatusCode()).isEqualTo(HttpStatus.OK);

        verify(repository).removeNodeByName("node-name");
    }

    @Test
    public void whenUnregisterByNameThenReturnNodeInfo() {
        when(repository.removeNodeByName("foo")).thenReturn(Option.of(new NodeProperties("foo", "bar", "fooKey", true)));
        IriNode unregistered = mock(IriNode.class);
        when(unregistered.getName()).thenReturn("foo");
        when(unregistered.getUrl()).thenReturn("bar");
        when(registry.unregisterNode("foo")).thenReturn(unregistered);

        Try<ResponseEntity<UnregisterNodeResponse>> result = Try.of(() -> clientApi.unregisterByName(new UnregisterNodeRequest("foo")));
        assertThat(result.isSuccess()).isTrue();
        assertThat(result.get().getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.get().getBody()).isNotNull();
        //noinspection ConstantConditions
        assertThat(result.get().getBody().getName()).isEqualTo("foo");
        //noinspection ConstantConditions
        assertThat(result.get().getBody().getUrl()).isEqualTo("bar");
    }

    @Test
    public void givenUnknownNameWhenUnregisterByNameThenReturnNotFound() {
        when(repository.removeNodeByName("foo")).thenReturn(Option.none());
        when(registry.unregisterNode("foo")).thenReturn(null);
        Try<ResponseEntity<UnregisterNodeResponse>> result = Try.of(() -> clientApi.unregisterByName(new UnregisterNodeRequest("foo")));
        assertThat(result.isFailure()).isTrue();
        ResponseStatusException ex = (ResponseStatusException) result.getCause();
        assertThat(ex.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(ex.getReason()).contains("Node not found.");
    }

    @Test
    public void whenGetThenReturnEmptyNodeInfo() {
        // node
        when(registry.getAllNodes()).thenReturn(HashMap.empty());
        // test
        StepVerifier.create(clientApi.getNodes()).verifyComplete();
    }

    @Test
    public void whenGetThenReturnNodeInfoWithoutCommands() {
        /// node
        IriNode node = node("bar", "foo", -1234, -1, 2345L, false);
        when(registry.getAllNodes()).thenReturn(HashMap.of("bar", node));
        // test
        StepVerifier.create(clientApi.getNodes())
                .assertNext(
                        ns -> {
                            assertStatusInfo(node, ns);
                            assertThat(ns.getCount()).isZero();
                            assertThat(ns.getMillis()).isZero();
                            assertThat(ns.getCommands()).isEmpty();
                        }
                ).verifyComplete();
    }

    @Test
    public void whenGetThenReturnNodeInfo() {
        IriNode node = setUpNodeInfo();
        // test
        StepVerifier.create(clientApi.getNodes())
                .assertNext(ns -> assertNodeInfo(node, ns))
                .verifyComplete();
    }

    @Test
    public void whenGetNodeByNameThenReturnNodeInfo() {
        // node
        IriNode node = setUpNodeInfo();
        // test
        StepVerifier.create(clientApi.getNodeByName("foo"))
                .assertNext(ns -> assertNodeInfo(node, ns))
                .verifyComplete();
    }

    /**
     * Regression test for bug. Get node info by name returned counters for all nodes with same key.
     * Renamed nodes did have duplicate command counters for same key but different name.
     */
    @Test
    public void givenSameKeyWhenGetNodeByNameThenReturnInfoForCurrentNameOnly() {

        IriNode node = node("current-name", "same-key", 42L, 41L, 1L, true);
        when(registry.getAllNodes()).thenReturn(HashMap.of("current-name", node));

        Timer timer1 = meterRegistry.timer(MeterFactory.NAME_IRI_COMMANDS_TIMER, MeterFactory.NODE_NAME, "current-name", MeterFactory.NODE_KEY, "same-key", MeterFactory.COMMAND, "bar");
        timer1.record(Duration.ofMillis(100));
        // timer 2 should not get returned. Same key but another name.
        Timer timer2 = meterRegistry.timer(MeterFactory.NAME_IRI_COMMANDS_TIMER, MeterFactory.NODE_NAME, "another-name", MeterFactory.NODE_KEY, "same-key", MeterFactory.COMMAND, "bar");
        timer2.record(Duration.ofMillis(42));

        StepVerifier.create(clientApi.getNodeByName("current-name"))
                .assertNext(ns -> {
                    Set<ProcessedCommands> processedCommands = ns.getCommands();
                    assertThat(processedCommands).hasSize(1);
                    ProcessedCommands command = processedCommands.iterator().next();
                    assertThat(command.getCommand()).isEqualTo("bar");
                    assertThat(command.getCount()).isOne();
                    assertThat(command.getMillis()).isEqualTo(100);
                })
                .verifyComplete();
    }

    private IriNode setUpNodeInfo() {
        // node
        IriNode node = node("foo", "fooKey", 42L, 41L, 1L, true);
        when(registry.getAllNodes()).thenReturn(HashMap.of("foo", node));
        // command
        Timer timer = meterRegistry.timer(MeterFactory.NAME_IRI_COMMANDS_TIMER, MeterFactory.NODE_NAME, "foo", MeterFactory.NODE_KEY, "fooKey", MeterFactory.COMMAND, "bar");
        timer.record(Duration.ofMillis(101));
        return node;
    }

    private void assertNodeInfo(IriNode node, NodeStatus ns) {
        assertStatusInfo(node, ns);
        Set<ProcessedCommands> processedCommands = ns.getCommands();
        assertThat(processedCommands).hasSize(1);
        ProcessedCommands command = processedCommands.iterator().next();
        assertThat(command.getCommand()).isEqualTo("bar");
        assertThat(command.getCount()).isOne();
        assertThat(command.getMillis()).isEqualTo(101);
        assertThat(ns.getCount()).isOne();
        assertThat(ns.getMillis()).isEqualTo(101);
    }

    private void assertStatusInfo(IriNode node, NodeStatus statusInfo) {
        assertThat(statusInfo.getName()).isEqualTo(node.getName());
        assertThat(statusInfo.getMilestone()).isEqualTo(node.getLatestMilestoneIndex());
        assertThat(statusInfo.getDelay()).isEqualTo(node.getDelay());
        assertThat(statusInfo.isAvailable()).isEqualTo(node.isCallPossible());

    }

    private IriNode node(String name, String key, long milestone, long solidMilestone, long delay, boolean available) {
        IriNode node = mock(IriNode.class);
        NodeInfo nodeInfo = new NodeInfo("app-name", "app-version", 3, milestone, "", solidMilestone, "");
        when(node.getLatestNodeInfo()).thenReturn(nodeInfo);
        when(node.getName()).thenReturn(name);
        when(node.getKey()).thenReturn(key);
        when(node.getLatestMilestoneIndex()).thenReturn(nodeInfo.getLatestMilestoneIndex());
        when(node.getDelay()).thenReturn(delay);
        when(node.isCallPossible()).thenReturn(available);
        return node;
    }

}