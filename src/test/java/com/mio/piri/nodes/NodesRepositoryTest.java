/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import org.apache.commons.lang3.RandomStringUtils;
import org.h2.mvstore.MVMap;
import org.h2.mvstore.MVStore;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NodesRepositoryTest {

    private static final String PASS = "test";

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    private String absFileName;

    private final HashGenerator hashGenerator = mock(HashGenerator.class);

    private NodesRepository repository;

    @Before
    public void initFile() throws IOException {
        folder.create();
        absFileName = folder.getRoot().getAbsolutePath() + File.separatorChar + RandomStringUtils.randomAlphabetic(5) + ".h2";
        repository = new NodesRepository(absFileName, PASS, true, hashGenerator);
    }

    @Test
    public void whenSaveToFileThenPersistData() {
        repository.addNode("foo", "bar", "some-url", false);
        repository.saveToFile();
        NodesRepository newRepository = new NodesRepository(absFileName, PASS, true, hashGenerator);
        assertThat(newRepository.getAllNodes()).hasSize(1);
        NodeProperties node = newRepository.getAllNodes().iterator().next();
        assertThat(node.pow).isFalse();
    }

    @Test
    public void givenNoFileNameWhenSaveToFileThenDoNotPersist() {
        repository = new NodesRepository(null, null, false, hashGenerator);
        repository.addNode("foo", "bar", "some-url", true);
        repository.saveToFile();
        NodesRepository newRepository = new NodesRepository(null, null, false, hashGenerator);
        assertThat(newRepository.getAllNodes()).hasSize(0);
    }

    @Test
    public void whenAddNodeThenAddToMap() {
        repository.addNode("foo", "bar", "some-url", true);
        assertThat(repository.getAllNodes()).hasSize(1);
    }

    @Test
    public void whenGetAllNodesThenReturnNamesAndUrls() {
        repository.addNode("foo", "bar", "some-url", true);
        Collection<NodeProperties> allNodes = repository.getAllNodes();
        assertThat(allNodes.iterator().next().name).isEqualTo("bar");
        assertThat(allNodes.iterator().next().url).isEqualTo("some-url");
    }

    @Test
    public void whenGetNodeNameThenReturnOptionalName() {
        repository.addNode("foo", "bar", "some-url", true);
        assertThat(repository.getNodeName("foo").get()).isEqualTo("bar");
        assertThat(repository.getNodeName("not-available").isEmpty()).isTrue();
    }

    @Test
    public void whenRemoveNodeThenRemoveFromMap() {
        repository.addNode("foo", "bar", "some-url", true);
        repository.removeNode("foo");
        assertThat(repository.getAllNodes()).isEmpty();
    }

    @Test(expected = IllegalStateException.class)
    public void givenSameKeyWhenAddThenThrow() {
        repository.addNode("x", "foo", "bar", true);
        repository.addNode("x", "bar", "foo", true);
    }

    @Test
    public void whenRemoveNodeByNameThenReturnRemovedNode() {
        repository.addNode("x", "y", "z", false);
        assertThat(repository.removeNodeByName("y").isEmpty()).isFalse();
    }

    @Test
    public void givenValidKeyWhenContainsKeyThenReturnTrue() {
        repository.addNode("x", "y", "z", false);
        assertThat(repository.containsKey("x")).isTrue();
    }

    @Test
    public void givenUnknownKeyWhenContainsKeyThenReturnFalse() {
        repository.addNode("x", "y", "z", false);
        assertThat(repository.containsKey("foo")).isFalse();
    }

    @Test
    public void givenPartialKeyWhenContainsKeyStartingWithThenReturnTrue() {
        repository.addNode("xyz", "y", "z", false);
        assertThat(repository.containsKeyStartingWith("xy")).isTrue();
    }

    @Test
    public void givenUnknownKeyWhenContainsKeyStartingWithThenReturnFalse() {
        assertThat(repository.containsKeyStartingWith("xz")).isFalse();
        repository.addNode("xyz", "y", "z", false);
        assertThat(repository.containsKeyStartingWith("xz")).isFalse();
    }

    @Test
    public void givenV1DbWhenInitThenConvertToV2() {
        // close (empty) test db. not sure if necessary
        repository.saveToFile();
        // init db in old db format
        MVStore.Builder storeBuilder = new MVStore.Builder().fileName(absFileName);
        storeBuilder.compress();
        storeBuilder.encryptionKey(PASS.toCharArray());
        MVStore store = storeBuilder.open();
        MVMap<String, String[]> registrations = store.openMap("registrations");
        registrations.put("x", new String[]{"y", "z"});
        store.close();
        // reopen test db.
        repository = new NodesRepository(absFileName, PASS, true, hashGenerator);
        NodeProperties nodeProperties = repository.removeNodeByName("y").get();
        assertThat(nodeProperties.name).isEqualTo("y");
        assertThat(nodeProperties.url).isEqualTo("z");
        assertThat(nodeProperties.pow).isFalse();
    }

    @Test
    public void givenV1DbWhenInitThenConvertToV3() {
        // close (empty) test db. not sure if necessary
        repository.saveToFile();
        // init db in old db format
        MVStore.Builder storeBuilder = new MVStore.Builder().fileName(absFileName);
        storeBuilder.compress();
        storeBuilder.encryptionKey(PASS.toCharArray());
        MVStore store = storeBuilder.open();
        MVMap<String, String[]> registrations = store.openMap("registrations");
        registrations.put("9", new String[]{"y", "z"});
        when(hashGenerator.sha256("9")).thenReturn("new-hash");
        store.close();
        // reopen test db.
        repository = new NodesRepository(absFileName, PASS, true, hashGenerator);
        NodeProperties nodeProperties = repository.removeNodeByName("y").get();
        assertThat(nodeProperties.name).isEqualTo("y");
        assertThat(nodeProperties.url).isEqualTo("z");
        assertThat(nodeProperties.pow).isFalse();
    }

    @Test
    public void givenV2DbWhenInitThenConvertToV3() {
        repository.addNode("ABC9XYZ", "foo", "bar", false);
        repository.saveToFile();
        when(hashGenerator.sha256("ABC9XYZ")).thenReturn("new-hash");
        // reopen test db.
        repository = new NodesRepository(absFileName, PASS, true, hashGenerator);
        NodeProperties nodeProperties = repository.removeNode("new-hash").get();
        assertThat(nodeProperties.name).isEqualTo("foo");
        assertThat(nodeProperties.url).isEqualTo("bar");
        assertThat(nodeProperties.pow).isFalse();
    }

    @Test
    public void givenV3DbWhenInitThenDoNotConvertToV3() {
        repository.addNode("abc9xyz", "foo", "bar", true);
        repository.saveToFile();
        // reopen test db.
        repository = new NodesRepository(absFileName, PASS, true, hashGenerator);
        NodeProperties nodeProperties = repository.removeNode("abc9xyz").get();
        assertThat(nodeProperties.name).isEqualTo("foo");
        assertThat(nodeProperties.url).isEqualTo("bar");
        assertThat(nodeProperties.pow).isTrue();
    }

}