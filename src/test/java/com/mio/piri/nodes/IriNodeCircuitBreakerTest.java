/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.mio.piri.commands.IriCommand;
import com.mio.piri.metrics.MeterFactory;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreaker.State;
import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import io.github.resilience4j.circuitbreaker.CircuitBreakerOpenException;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.micrometer.core.instrument.Clock;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;
import io.vavr.control.Try;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class IriNodeCircuitBreakerTest {

    private final NodeClient nodeClient = mock(NodeClient.class);
    private final MeterFactory metrics = mock(MeterFactory.class);
    private final IriCommand command = mock(IriCommand.class);

    private final Timer timer = Metrics.timer("timer");

    private final CircuitBreakerConfig circuitBreakerConfig = CircuitBreakerConfig.custom()
            .failureRateThreshold(1)
            .waitDurationInOpenState(Duration.ofSeconds(2))
            .ringBufferSizeInHalfOpenState(1)
            .ringBufferSizeInClosedState(1)
            .build();

    private final CircuitBreakerRegistry circuitBreakerRegistry = CircuitBreakerRegistry.of(circuitBreakerConfig);
    private final CircuitBreaker circuitBreaker = circuitBreakerRegistry.circuitBreaker("foo");
//    private final ClientResponse clientResponse = ClientResponse.create(HttpStatus.OK).body("foo").build();
    private final ResponseEntity<String> responseEntity = ResponseEntity.status(HttpStatus.OK).body("foo");
    private final IriNode node = new IriNode("foo", "url", "fooKey", true, nodeClient, metrics, circuitBreaker);

    @Before
    public void setUpMocks() {
        operationOk(); // default
        when(command.getCommand()).thenReturn("foo");
        when(metrics.createCommandTimer(anyString(), anyString(), anyString())).thenReturn(timer);
        when(metrics.sample()).thenReturn(Timer.start(Clock.SYSTEM));
    }

    @Test
    public void whenCallThenReturnResponse() {
        Mono<ResponseEntity<String>> response = node.call(command);
        assertNotNull(response);
    }

    @Test
    public void givenTimePassedWhenCallThenCircuitIsClosedAgain() throws InterruptedException {

        // circuit breaker open
        circuitBreaker.transitionToOpenState();
        Try<ResponseEntity<String>> tryResponse1 = Try.ofSupplier(() -> node.call(command).block());
        assertThat(tryResponse1.getCause()).isInstanceOf(CircuitBreakerOpenException.class);

        TimeUnit.SECONDS.sleep(2);

        // circuit breaker is closed again
        Try<Mono<ResponseEntity<String>>> tryResponse2 = Try.ofSupplier(() -> node.call(command));
        assertThat(tryResponse2.isSuccess()).isTrue();

    }

    @Test
    public void givenErrorsWhenExchangeThenOpen() {
        // working client
        Supplier<ResponseEntity<String>> callAndBlock = () -> node.call(command).block();
        Try<ResponseEntity<String>> response = Try.ofSupplier(callAndBlock);
        assertThat(response.isSuccess()).isTrue();

        // operation fails
        operationFails();
        response = Try.ofSupplier(callAndBlock);
        assertThat(response.getCause().getMessage()).isEqualTo("test");

        System.out.println(node.isCallPossible());

        assertThat(node.isCallPossible()).isFalse();
        assertThat(circuitBreaker.getState()).isEqualTo(State.OPEN);

        // the following code doesn't work because of mocking:
        // circuit breaker is now open
        // response = Try.of(() -> node.call(command).block());
        // assertThat(response.getCause()).isInstanceOf(CircuitBreakerOpenException.class);

    }

    @Test
    public void givenOpenCircuitWhenIsCallPermittedThenFalse() {
        assertTrue(node.isCallPossible());
        operationFails();
        Try.ofSupplier(() -> node.call(command).block());
        assertFalse(node.isCallPossible());
    }

    private void operationOk() {
        when(nodeClient.post(any(Node.class), any(IriCommand.class))).thenReturn(Mono.just(responseEntity));
                //.uri(any(URI.class)).syncBody(any(IriCommand.class)).exchange()).thenReturn(Mono.just(clientResponse));
    }

    private void operationFails() {
        when(nodeClient.post(any(Node.class), any(IriCommand.class))).thenReturn(Mono.error(new RuntimeException("test")));
    }

}