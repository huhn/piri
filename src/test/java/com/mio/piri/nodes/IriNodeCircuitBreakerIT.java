/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mio.piri.commands.GetNodeInfo;
import com.mio.piri.commands.response.ResponseDtoBuilder;
import com.mio.piri.tolerance.CircuitBreakerFactory;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerOpenException;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

import static com.mio.piri.commands.IriCommands.GET_NODE_INFO;
import static org.assertj.core.api.Assertions.assertThat;

//@DirtiesContext
@RunWith(SpringRunner.class)
@SpringBootTest(properties = {
        "piri.circuit.open.duration=5s",
        "piri.circuit.failure.rate=50",
        "piri.circuit.buffer.closed=2",
        "piri.circuit.buffer.half-open=1"
}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IriNodeCircuitBreakerIT {

    private static final ObjectMapper JSON = new ObjectMapper();

    private MockWebServer server;

    @Autowired
    private NodeRegistry nodeRegistry;

    @Autowired
    private CircuitBreakerFactory circuitBreakerFactory;

    private IriNode node;

    private final String syncedNodeInfo = jsonString(ResponseDtoBuilder.nodeInfoBuilder()
            .latestMilestoneIndex(42)
            .latestSolidSubtangleMilestoneIndex(42)
            .build());

    @Before
    public void setUpTestNode() throws IOException {
        server = new MockWebServer();
        server.start(1234);
        node = nodeRegistry.registerIriNode(RandomStringUtils.randomAlphabetic(25), server.url("/").toString(), RandomStringUtils.randomAlphabetic(5),true);
    }

    @After
    public void shutdown() throws Exception {
        this.server.shutdown();
        nodeRegistry.unregisterNode(node.getName());
    }

    @Test
    public void givenOpenCircuitWhenPermissionCheckThenReturnFalse() {
        assertThat(node.isCallPossible()).isTrue();
        CircuitBreaker circuitBreaker = circuitBreakerFactory.circuitBreaker(node.getName());
        circuitBreaker.transitionToOpenState();
        assertThat(node.isCallPossible()).isFalse();
    }

    @Test
    public void givenManyTimeoutsWhenCallThenOpenCircuit() {

        // prepare request that takes longer than timeout
        int delay = 10;
        prepareResponse(response -> response
                .setHeader("Content-Type", "application/json")
                .setBody(syncedNodeInfo)
                .setBodyDelay(delay, TimeUnit.MILLISECONDS));

        Duration original = GET_NODE_INFO.getTimeout();
        GET_NODE_INFO.setTimeout(Duration.ofMillis(1)); // dirties context!

        assertThat(node.isCallPossible()).isTrue();

        // buffer needs to be full (test setup: 2 requests with 50% failure threshold).
        // first call fails and fills buffer
        assertThat(node.isCallPossible()).isTrue();
        StepVerifier.create(node.call(new GetNodeInfo()))
                .expectError(TimeoutException.class)
                .log()
                .verify(Duration.ofSeconds(1));

        assertThat(node.isCallPossible()).isTrue();
        StepVerifier.create(node.call(new GetNodeInfo()))
                .expectError(TimeoutException.class)
                .log()
                .verify(Duration.ofSeconds(1));

        // assert that circuit is open after so many failures
        assertThat(node.isCallPossible()).isFalse();
        StepVerifier.create(node.call(new GetNodeInfo()))
                .expectError(CircuitBreakerOpenException.class)
                .log()
                .verify(Duration.ofSeconds(1));

        GET_NODE_INFO.setTimeout(original); // set back to restore context
    }

    /**
     * Normal call should return error in case circuit is open.
     */
    @Test
    public void givenOpenCircuitWhenCallThenError() {
        CircuitBreaker circuitBreaker = circuitBreakerFactory.circuitBreaker(node.getName());
        circuitBreaker.transitionToOpenState();
        assertThat(node.isCallPossible()).isFalse();
        StepVerifier.create(node.call(new GetNodeInfo()))
                .expectError(CircuitBreakerOpenException.class)
                .verify(Duration.ofSeconds(1));

    }

    /**
     * Health check should not return errors even if circuit breaker is open.
     */
    @Test
    public void givenOpenCircuitWhenHealthCheckThenNoError() {
        CircuitBreaker circuitBreaker = circuitBreakerFactory.circuitBreaker(node.getName());
        circuitBreaker.transitionToOpenState();
        StepVerifier.create(node.queryNodeHealth())
                .assertNext(info -> assertThat(info.isSynced()).isFalse())
                .verifyComplete();

    }

    private void prepareResponse(Consumer<MockResponse> consumer) {
        MockResponse response = new MockResponse();
        consumer.accept(response);
        this.server.enqueue(response);
    }


    private String jsonString(Object nodeInfo) {
        try {
            return JSON.writeValueAsString(nodeInfo);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not convert to json", e);
        }
    }


}