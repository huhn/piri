/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.mio.piri.commands.AttachToTangle;
import com.mio.piri.commands.GetNodeInfo;
import com.mio.piri.commands.GetTransactionsToApprove;
import com.mio.piri.commands.IriCommand;
import com.mio.piri.commands.response.NodeInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.net.URI;
import java.time.Duration;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NodeClientTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private WebClient webClient;

    @InjectMocks
    private NodeClient nodeClient = new NodeClient();

    private final ClientResponse ok = ClientResponse.create(HttpStatus.OK).body("test").header("foo", "bar").build();
    private final ClientResponse serviceUnavailable = ClientResponse.create(HttpStatus.SERVICE_UNAVAILABLE).build();
    private final ClientResponse internalServerError = ClientResponse.create(HttpStatus.INTERNAL_SERVER_ERROR).build();
    private final ClientResponse badRequest = ClientResponse.create(HttpStatus.BAD_REQUEST).build();

    @Mock
    private IriNode node;
    private final IriCommand command = new GetNodeInfo();

    @SuppressWarnings("unchecked")
    private void setExchangeResponse(ClientResponse clientResponse) {
        WebClient.RequestBodyUriSpec spec = mock(WebClient.RequestBodyUriSpec.class);
        when(webClient.post()).thenReturn(spec);
        when(spec.headers(any())).thenReturn(spec);
        when(spec.uri(any(URI.class))).thenReturn(spec);
        WebClient.RequestHeadersSpec headersSpec = mock(WebClient.RequestHeadersSpec.class);
        when(spec.syncBody(any(IriCommand.class))).thenReturn(headersSpec);
        // when(headersSpec.headers(any())).thenReturn(headersSpec);
        when(headersSpec.exchange()).thenReturn(Mono.just(clientResponse));
        // when(webClient.post().uri(any(URI.class)).syncBody(any(IriCommand.class)).headers(any()).exchange()).thenReturn(Mono.just(clientResponse));
    }

    private void setGetNodeInfoResponse() {
        when(webClient.post().uri(any(URI.class)).syncBody(any(GetNodeInfo.class)).retrieve().bodyToMono(NodeInfo.class)).thenReturn(Mono.just(new NodeInfo()));
    }

    @Before
    public void initMocks() {
        when(node.getTimeout(any(IriCommand.class))).thenReturn(Duration.ofSeconds(1));
        when(node.getUri()).thenReturn(URI.create("http://foo.bar.com"));
        when(node.getName()).thenReturn("foo");
    }

    @Test
    public void whenPostThenReturnResponse() {
        setExchangeResponse(ok);
        Mono<ResponseEntity<String>> response = nodeClient.post(node, command);
        assertThat(response).isNotNull();
    }

    @Test
    public void whenPostThenReturnCompleteResponse() {
        setExchangeResponse(ok);
        StepVerifier.create(nodeClient.post(node, command))
                .assertNext(responseEntity -> {
                    assertThat(responseEntity.getStatusCode()).isSameAs(HttpStatus.OK);
                    assertThat(responseEntity.getBody()).isEqualTo("test");
                    assertThat(responseEntity.getHeaders()).containsKey("foo");
                    assertThat(responseEntity.getHeaders()).containsValue(Collections.singletonList("bar"));
                })
                .verifyComplete();
    }

    // we can't throw here. IRI returns 500 for gTTA with depth that doesn't find the reference transaction for example.
    @Test
    public void givenGTTACommandAndInternalServerErrorWhenPostThenDoNotThrow() {
        setExchangeResponse(internalServerError);
        IriCommand gTTA = mock(GetTransactionsToApprove.class);
        when(gTTA.getCommand()).thenReturn("getTransactionsToApprove");
        StepVerifier.create(nodeClient.post(node, gTTA))
                .assertNext(responseEntity -> assertThat(responseEntity.getStatusCode()).isSameAs(HttpStatus.INTERNAL_SERVER_ERROR))
                .verifyComplete();
    }

    @Test
    public void givenInternalServerErrorWhenPostThenThrow() {
        setExchangeResponse(internalServerError);
        StepVerifier.create(nodeClient.post(node, command))
                .expectError(WebClientResponseException.class)
                .verify(Duration.ZERO);
    }

    @Test
    public void givenServiceUnavailableWhenPostThenThrow() {
        setExchangeResponse(serviceUnavailable);
        StepVerifier.create(nodeClient.post(node, command))
                .expectError(WebClientResponseException.class)
                .verify(Duration.ZERO);
    }

    @Test
    public void givenBadRequestWhenPostThenDoNotThrow() {
        setExchangeResponse(badRequest);
        StepVerifier.create(nodeClient.post(node, command))
                .assertNext(responseEntity -> assertThat(responseEntity.getStatusCode()).isSameAs(HttpStatus.BAD_REQUEST))
                .verifyComplete();
    }

    @Test
    public void givenBadRequestOnPowCheckWhenPostThenDoNotThrow() {
        setExchangeResponse(badRequest);
        StepVerifier.create(nodeClient.post(node, new AttachToTangle()))
                .assertNext(responseEntity -> assertThat(responseEntity.getStatusCode()).isSameAs(HttpStatus.BAD_REQUEST))
                .verifyComplete();
    }

    @Test
    public void whenRetrieveThenReturnOk() {
        setGetNodeInfoResponse();
        StepVerifier.create(nodeClient.retrieve(node, new GetNodeInfo(), NodeInfo.class))
                .assertNext(nodeInfo -> assertThat(nodeInfo).isNotNull())
                .verifyComplete();
    }

}