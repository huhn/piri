/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;


public class RandomKeyGeneratorTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final RandomKeyGenerator randomKeyGenerator = new RandomKeyGenerator();


    @Test
    public void shouldCreateRandomKeys() {
        final long noOfKeys = 10000; // takes approx 0.016ms/key
        Set<String> keys = new HashSet<>();
        long start = System.currentTimeMillis();
        for (int i = 0; i < noOfKeys; i++) {
            keys.add(randomKeyGenerator.generateNodePassword());
        }
        long stop = System.currentTimeMillis();
        logger.debug("Created [{}] keys in [{}]ms. Avg: [{}]ms.", noOfKeys, stop - start, (float) (stop-start)/noOfKeys);
        assertThat(keys.size()).isEqualTo(noOfKeys); // no duplicates
    }

}