/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.selection;

import com.mio.piri.commands.IriCommand;
import com.mio.piri.nodes.Node;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SelectionUtilityTest {

    private final SelectionUtility selectionUtility = new SelectionUtility();
    private final IriCommand command = mock(IriCommand.class);
    private final Node node = mock(Node.class);


    @Test
    public void whenIsCallFeasibleThenTrue() {
        when(command.needsUpToDateNode()).thenReturn(true);
        when(node.isCallPossible()).thenReturn(true);
        when(node.isSynced()).thenReturn(true);
        when(node.isSupported(command)).thenReturn(true);

        assertThat(selectionUtility.isCallFeasible(node, command, 1)).isTrue();
    }

    @Test
    public void givenNullWhenIsCallFeasibleThenFalse() {
        assertThat(selectionUtility.isCallFeasible(null, command, 1)).isFalse();
    }

    @Test
    public void givenDelayedNodeWhenIsCallFeasibleThenFalse() {
        when(node.isCallPossible()).thenReturn(true);
        when(node.isSupported(command)).thenReturn(true);

        assertThat(selectionUtility.isCallFeasible(node, command, 1)).isTrue();

        when(node.getDelay()).thenReturn(2L);
        assertThat(selectionUtility.isCallFeasible(node, command, 1)).isFalse();

    }

    @Test
    public void whenIsCallFeasibleThenCheckIfNodeIsSynced() {
        when(command.needsUpToDateNode()).thenReturn(true);
        when(node.isCallPossible()).thenReturn(true);
        when(node.isSupported(command)).thenReturn(true);

        assertThat(selectionUtility.isCallFeasible(node, command, 1)).isFalse();

        when(node.isSynced()).thenReturn(true);
        assertThat(selectionUtility.isCallFeasible(node, command, 1)).isTrue();
    }

    @Test
    public void whenIsCallFeasibleThenCheckIfCommandIsSupported() {
        when(node.isCallPossible()).thenReturn(true);

        assertThat(selectionUtility.isCallFeasible(node, command, 1)).isFalse();

        when(node.isSupported(command)).thenReturn(true);
        assertThat(selectionUtility.isCallFeasible(node, command, 1)).isTrue();
    }

    @Test
    public void whenIsCallFeasibleThenCheckIfCallIsPossible() {
        when(node.isSupported(command)).thenReturn(true);

        assertThat(selectionUtility.isCallFeasible(node, command, 1)).isFalse();

        when(node.isCallPossible()).thenReturn(true);
        assertThat(selectionUtility.isCallFeasible(node, command, 1)).isTrue();
    }

    @Test
    public void whenIsCallFeasibleThenCheckMaxRetries() {
        selectionUtility.setMaxRetriesPerNode(1);
        selectionUtility.setSuccessiveRetryOnSameNode(true);

        when(node.getName()).thenReturn("foo");
        when(node.isCallPossible()).thenReturn(true);
        when(node.isSupported(command)).thenReturn(true);
        when(command.getExecutionHistory())
                .thenReturn(Collections.singletonList("foo")) // first retry
                .thenReturn(Arrays.asList("foo", "foo")); // one retry too much

        assertThat(selectionUtility.isCallFeasible(node, command, 1)).isTrue();
        assertThat(selectionUtility.isCallFeasible(node, command, 1)).isFalse();
    }

    @Test
    public void whenIsCallFeasibleThenCheckSuccessiveRetriesOnSameNode() {
        selectionUtility.setMaxRetriesPerNode(1);
        selectionUtility.setSuccessiveRetryOnSameNode(false);

        when(node.getName()).thenReturn("foo");
        when(node.isCallPossible()).thenReturn(true);
        when(node.isSupported(command)).thenReturn(true);
        when(command.getExecutionHistory())
                .thenReturn(Collections.singletonList("foo")) // successive retry
                .thenReturn(Arrays.asList("foo", "bar")); // no successive retry

        assertThat(selectionUtility.isCallFeasible(node, command, 1)).isFalse();
        assertThat(selectionUtility.isCallFeasible(node, command, 1)).isTrue();
    }

}