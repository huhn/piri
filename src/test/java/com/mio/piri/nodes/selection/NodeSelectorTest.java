/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.selection;

import com.mio.piri.commands.IriCommand;
import com.mio.piri.nodes.Node;
import com.mio.piri.nodes.NodeRegistry;
import io.vavr.collection.HashMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NodeSelectorTest {

    @Mock
    private SessionSelectionStrategy sessionStrategy;

    @Mock
    private RandomSelectionStrategy randomStrategy;

    @Mock
    private NodeRegistry nodeRegistry;

    @InjectMocks
    private NodeSelector nodeSelector = new NodeSelector();

    @Mock
    private IriCommand command;

    @Mock
    private Node node;

    private final HashMap<String, Node> nodes = HashMap.empty();

    @Before
    public void initMocks() {
        when(nodeRegistry.getAllNodes()).thenReturn(nodes);
    }

    @Test
    public void givenActiveSessionWhenSelectThenReturnBoundNode() {
        when(command.wantsSameNodePerClient()).thenReturn(true);
        when(sessionStrategy.selectNode(command, nodes)).thenReturn(node);
        assertThat(nodeSelector.selectNodes(command)).containsExactly(node);
        verifyZeroInteractions(randomStrategy);
    }

    @Test
    public void givenNoNodeWhenSelectThenReturnEmptyList() {
        assertThat(nodeSelector.selectNodes(command)).isEmpty();
    }

    @Test
    public void givenCommandThatDoesNotCareAboutDelaySelectThenBindNodeToSession() {
        // we don't want to bind a node when the command accepts a node that is not current
        // no harm switching to another node then
        when(command.wantsSameNodePerClient()).thenReturn(true);
        when(command.wantsUpToDateNode()).thenReturn(false);
        when(randomStrategy.selectNode(command, nodes)).thenReturn(node);

        assertThat(nodeSelector.selectNodes(command)).containsExactly(node);

        verify(sessionStrategy).selectNode(command, nodes);
        verify(sessionStrategy, never()).bindNodeToSession(command, node);
    }

    @Test
    public void givenCommandThatWantsSameClientWhenSelectThenBindNodeToSession() {
        when(command.wantsSameNodePerClient()).thenReturn(true);
        when(command.wantsUpToDateNode()).thenReturn(true);
        when(randomStrategy.selectNode(command, nodes)).thenReturn(node);

        assertThat(nodeSelector.selectNodes(command)).containsExactly(node);

        verify(sessionStrategy).selectNode(command, nodes);
        verify(sessionStrategy).bindNodeToSession(command, node);

    }

    @Test
    public void givenNoActiveSessionWhenSelectNodesThenCallAllStrategies() {
        when(command.wantsSameNodePerClient()).thenReturn(true);
        when(sessionStrategy.selectNode(command, nodes)).thenReturn(null);
        nodeSelector.selectNodes(command);
        verify(sessionStrategy).selectNode(command, nodes);
        verify(randomStrategy).selectNode(command, nodes);
    }

    @Test
    public void givenCommandWantsNoSessionWhenSelectNodesThenCallRandomStrategy() {
        when(randomStrategy.selectNode(command, nodes)).thenReturn(node);
        assertThat(nodeSelector.selectNodes(command)).containsExactly(node);
        verifyZeroInteractions(sessionStrategy);
    }

    @Test
    public void givenSessionBindingDisabledWhenSelectNodesThenCallRandomStrategy() {
        nodeSelector.setSessionBindingEnabled(false);
        nodeSelector.selectNodes(command);
        verifyZeroInteractions(sessionStrategy);
        verify(randomStrategy).selectNode(command, nodes);
    }

}