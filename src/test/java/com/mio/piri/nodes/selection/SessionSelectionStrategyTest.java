/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.selection;

import com.mio.piri.commands.IriCommand;
import com.mio.piri.nodes.Node;
import io.vavr.collection.HashMap;
import io.vavr.control.Option;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SessionSelectionStrategyTest {

    @Mock
    private SessionBinding sessionBinding;

    @Mock
    private SelectionUtility selectionUtility;

    @InjectMocks
    private final SessionSelectionStrategy strategy = new SessionSelectionStrategy();

    @Mock
    private IriCommand command;

    @Mock
    private Node node;

    private HashMap<String, Node> nodes;

    @Before
    public void initMocks() {
        nodes = HashMap.of("node-name", node);
        when(sessionBinding.getNodeForClient("session-id")).thenReturn(Option.of("node-name"));
        when(selectionUtility.isCallFeasible(node, command, Integer.MAX_VALUE)).thenReturn(true);
        when(command.wantsSameNodePerClient()).thenReturn(true);
    }

    @Test
    public void whenSelectThenReturnNodeBoundToClient() {
        when(command.getSessionId()).thenReturn("session-id");
        assertThat(strategy.selectNode(command, nodes)).isSameAs(node);
    }

    @Test
    public void givenCommandIsNotFeasibleWhenSelectThenUnbindNode() {
        when(command.getSessionId()).thenReturn("session-id");
        when(selectionUtility.isCallFeasible(node, command, Integer.MAX_VALUE)).thenReturn(false);
        assertThat(strategy.selectNode(command, nodes)).isNull();
        verify(sessionBinding).removeNodeFromMapping(node);
    }

    @Test
    public void givenCallNotFeasibleWhenSelectThenReturnNull() {
        when(selectionUtility.isCallFeasible(node, command, Integer.MAX_VALUE)).thenReturn(false);
        when(command.getSessionId()).thenReturn("session-id");
        assertThat(strategy.selectNode(command, nodes)).isNull();
    }

    @Test
    public void givenCommandThatCaresAboutDelayWhenSelectThenCheckForDelay() {
        when(command.getSessionId()).thenReturn("session-id");
        when(command.wantsUpToDateNode()).thenReturn(true);
        strategy.selectNode(command, nodes);
        verify(selectionUtility).isCallFeasible(node, command, 2);
    }

    @Test
    public void givenBoundNodeThatIsNotInCurrentNodesWhenSelectThenReturnNull() {
        when(command.getSessionId()).thenReturn("old-id");
        when(sessionBinding.getNodeForClient("old-id")).thenReturn(Option.of("unknown-name"));
        assertThat(strategy.selectNode(command, nodes)).isNull();
    }

    @Test
    public void givenCommandThatWantsSessionWhenBindNodeToSessionThenAddNodeToSessionBinding() {
        when(command.getSessionId()).thenReturn("foo-session");
        when(node.getName()).thenReturn("foo-name");
        strategy.bindNodeToSession(command, node);
        verify(sessionBinding).putNodeForClient("foo-session", "foo-name");
    }

}