/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.exceptions.CannotRegisterNode;
import com.mio.piri.metrics.MeterFactory;
import com.mio.piri.nodes.selection.SessionBinding;
import com.mio.piri.tolerance.CircuitBreakerFactory;
import com.mio.piri.util.UrlValidator;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.micrometer.core.instrument.MeterRegistry;
import io.vavr.control.Try;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NodeRegistryTest {

    @Mock
    private UrlValidator urlValidator;
    @SuppressWarnings("unused") // use in registry initializer
    @Mock
    private WebClient.Builder clientBuilder;
    @SuppressWarnings("unused") // needs to be injected for init method
    @Mock
    private MeterRegistry meterRegistry;
    @Mock
    private Environment env;
    @Mock
    private CircuitBreakerFactory circuitBreakerFactory;
    @Mock
    private MeterFactory meterFactory;
    @Mock
    private NodesRepository nodesRepository;
    @Mock
    private SessionBinding sessionBinding;

    @InjectMocks
    private NodeRegistry registry = new NodeRegistry();

    @Mock
    private NodeInfo nodeInfo;

    @Before
    public void setUpMocks() {
        when(circuitBreakerFactory.circuitBreaker(anyString())).thenReturn(CircuitBreaker.ofDefaults("foo"));
        when(urlValidator.isValidUrl(anyString())).thenReturn(true);
        when(env.getProperty(anyString(), anyString())).thenReturn("42s");
        when(env.getProperty(eq("piri.health.check.interval"), anyString())).thenReturn("10s");
    }

    // FIXME remove and mock static initialization code
//    @Test
//    public void whenInitThenRegisterNode() {
//        when(env.getProperty("iri.nodes")).thenReturn("default");
//        when(env.getRequiredProperty("iri.default.url")).thenReturn("defaultUrl");
//        registry.initialize();
//        assertThat(registry.getNextNode(command)).isNotNull();
//    }

    @Test
    public void whenInitThenCreateGauges() {
        registry.initialize();
        verify(meterFactory).createTotalNodesGauge(any());
        verify(meterFactory).createActiveNodesGauge(any());
        verify(meterFactory).createInactiveNodesGauge(any());
        verify(meterFactory).createSyncedNodesGauge(any());
        verifyNoMoreInteractions(meterFactory);
    }

    // FIXME remove and mock static initialization code
//    @Test
//    public void whenInitThenRegisterPersistedNodes() {
//        Collection<NodeProperties> props = Collections.singleton(new NodeProperties("abc", "def"));
//        when(nodesRepository.getAllNodes()).thenReturn(props);
//        registry.initialize();
//        assertThat(registry.unregisterNode("abc")).isNotNull(); // check that node was loaded
//    }

    @Test
    public void whenUnregisterThenRemoveNode() {
        IriNode node = registry.registerIriNode("foo", "bar", "fooKey", true);
        assertThat(registry.unregisterNode("foo")).isSameAs(node);
    }

    @Test
    public void givenUnknownNodeWhenUnregisterThenDoNothing() {
        assertThat(registry.unregisterNode("foo")).isNull();
        verifyZeroInteractions(sessionBinding);
        verifyZeroInteractions(nodesRepository);
    }

    @Test
    public void givenNoNodesWhenHealthCheckThenDoNotThrow() {
        registry.runHealthCheck(new HashMap<>(), 0);
    }

    @Test
    public void givenNoNodesWhenPowCheckThenDoNotThrow() {
        registry.runPowCheck(new HashMap<>(), 0);
    }

    @Test
    public void whenHealthCheckThenQueryForEachNode() {
        HashMap<String, IriNode> nodeMap = nodeMapWithTwoNodes();
        nodeMap.values().forEach(node -> when(node.queryNodeHealth()).thenReturn(Mono.just(nodeInfo)));
        registry.runHealthCheck(nodeMap, 0);
        nodeMap.values().forEach(node -> verify(node).queryNodeHealth());
    }

    @Test
    public void whenPowCheckThenQueryEachNodeThatHasPowEnabled() {
        HashMap<String, IriNode> nodeMap = nodeMapWithTwoNodes();
        IriNode node = nodeWithPow();
        nodeMap.put("foo", node);
        registry.runPowCheck(nodeMap, 0);
        nodeMap.forEach((key, value) -> {
            if (!key.equals("foo")) {
                verify(value, never()).doPowCheck();
            } else {
                verify(value).doPowCheck();
            }
        });
    }

    @Test
    public void whenHealthCheckThenUpdateLatestKnownMilestone() {
        HashMap<String, IriNode> nodeMap = nodeMapWithTwoNodes();
        when(nodeInfo.getLatestMilestoneIndex()).thenReturn(42L);
        nodeMap.values().forEach(node -> when(node.queryNodeHealth()).thenReturn(Mono.just(nodeInfo)));
        registry.runHealthCheck(nodeMap, 0);
        assertThat(registry.getLatestKnownMilestone()).isEqualTo(42);
    }

    @Test
    public void givenErrorWhenHealthCheckThenLogAndDoNotThrow() {
        HashMap<String, IriNode> nodes = new HashMap<>();
        IriNode node = mock(IriNode.class);
        when(node.queryNodeHealth()).thenReturn(Mono.error(new RuntimeException("test")));
        nodes.put("foo", node);
        registry.runHealthCheck(nodes, 0);
    }

    @Test
    public void givenInvalidNameWhenRegisterThenThrow() {
        Try result = Try.ofSupplier(() -> registry.registerIriNode(" ", "http://localhost:1234", "some-key", false));
        assertThat(result.isFailure()).isTrue();
        assertThat(result.getCause()).isInstanceOf(CannotRegisterNode.class);
        assertThat(result.getCause().getMessage()).containsIgnoringCase("invalid node name");
    }

    @Test
    public void givenInvalidUrlWhenRegisterThenThrow() {
        when(urlValidator.isValidUrl(anyString())).thenReturn(false);
        Try result = Try.ofSupplier(() -> registry.registerIriNode("name", "invalid", "some-key", false));
        assertThat(result.isFailure()).isTrue();
        assertThat(result.getCause()).isInstanceOf(CannotRegisterNode.class);
        assertThat(result.getCause().getMessage()).containsIgnoringCase("invalid node url");
    }

    @Test
    public void givenInvalidKeyWhenRegisterThenThrow() {
        Try result = Try.ofSupplier(() -> registry.registerIriNode("name", "http://localhost:1234", "invalid key", false));
        assertThat(result.isFailure()).isTrue();
        assertThat(result.getCause()).isInstanceOf(CannotRegisterNode.class);
        assertThat(result.getCause().getMessage()).containsIgnoringCase("invalid key");
    }

    private HashMap<String, IriNode> nodeMapWithTwoNodes() {
        HashMap<String, IriNode> nodes = new HashMap<>();
        nodes.put("a-node", mock(IriNode.class));
        nodes.put("b-node", mock(IriNode.class));
        return nodes;
    }

    private IriNode nodeWithPow() {
        IriNode node = mock(IriNode.class);
        when(node.isPowEnabled()).thenReturn(true);
        when(node.isCallPossible()).thenReturn(true);
        return node;
    }

}