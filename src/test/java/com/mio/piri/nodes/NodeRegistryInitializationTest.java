/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.metrics.MeterFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class NodeRegistryInitializationTest {

    @Mock
    private Environment env;

    @Mock
    private NodeRegistry nodeRegistry;

    @Mock
    private NodesRepository nodesRepository;

    @Mock
    private IriNode node;

    @Before
    public void setupMocks() {
        when(nodeRegistry.registerIriNode(anyString(), anyString(), anyString(), anyBoolean())).thenReturn(node);
        when(nodeRegistry.getLatestKnownMilestone()).thenReturn(42L);
        when(node.queryNodeHealth()).thenReturn(Mono.just(new NodeInfo()));
    }

    @Test
    public void shouldRegisterConfiguredNodes() {
        when(env.getProperty("iri.nodes")).thenReturn(" foo , bar");
        when(env.getProperty("iri.foo.pow")).thenReturn("true");
        when(env.getRequiredProperty("iri.foo.url")).thenReturn("fooUrl");
        when(env.getRequiredProperty("iri.bar.url")).thenReturn("barUrl");
        NodeRegistryInitializer.initConfiguredNodes(nodeRegistry, env);
        verify(nodeRegistry).registerIriNode("foo", "fooUrl", "foo", true);
        verify(nodeRegistry).registerIriNode("bar", "barUrl", "bar", false);
    }

    @Test
    public void shouldRegisterPersistedNodes() {
        when(nodesRepository.getAllNodes()).thenReturn(Arrays.asList(new NodeProperties("foo", "fooUrl", "fooKey", true),
                new NodeProperties("bar", "barUrl", "barKey", false)));
        NodeRegistryInitializer.initPersistedNodes(nodeRegistry, nodesRepository);
        verify(nodeRegistry).registerIriNode("foo", "fooUrl", "fooKey", true);
        verify(nodeRegistry).registerIriNode("bar", "barUrl", "barKey", false);
    }

    @Test
    public void shouldInitGauges() {
        Map<String, IriNode> nodes = new HashMap<>();
        MeterFactory factory = mock(MeterFactory.class);
        NodeRegistryInitializer.initNodeGauges(factory, nodes);
        verify(factory).createInactiveNodesGauge(nodes);
        verify(factory).createActiveNodesGauge(nodes);
        verify(factory).createTotalNodesGauge(nodes);
    }

}