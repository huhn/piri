/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.util;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.SslInfo;

import java.net.InetAddress;
import java.net.InetSocketAddress;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ClientSessionExtractorTest {

    @Mock
    private Environment env;

    @Mock
    private ServerHttpRequest serverRequest;

    @Mock
    private HttpHeaders headers;

    @Mock
    private InetAddress inetAddress;

    @Mock
    private SslInfo sslInfo;

    @Before
    public void setupMocks() {
        InetSocketAddress inetSocketAddress = new InetSocketAddress(inetAddress, 1234);
        when(inetAddress.getHostAddress()).thenReturn("6.6.6.6");
        when(serverRequest.getHeaders()).thenReturn(headers);
        when(serverRequest.getRemoteAddress()).thenReturn(inetSocketAddress);
        when(serverRequest.getSslInfo()).thenReturn(sslInfo);
        disableProxyMode();
    }

    @Test
    public void whenGetClientIpThenReturnRemoteIp() {
        ClientSessionExtractor session = new ClientSessionExtractor(env);
        when(inetAddress.getHostAddress()).thenReturn("foo");
        assertThat(session.getClientIp(serverRequest)).isEqualTo("foo");
    }

    @Test
    public void whenGetClientSslIdThenReturnSslId() {
        ClientSessionExtractor session = new ClientSessionExtractor(env);
        when(sslInfo.getSessionId()).thenReturn("foo");
        assertThat(session.getClientSslId(serverRequest)).isEqualTo("foo");
    }

    @Test
    public void givenProxyModeWhenGetClientIpThenReturnHeader() {
        enableProxyMode("foo", null, null, false);
        ClientSessionExtractor session = new ClientSessionExtractor(env);
        when(headers.containsKey("foo")).thenReturn(true);
        when(headers.getFirst("foo")).thenReturn("bar");
        assertThat(session.getClientIp(serverRequest)).isEqualTo("bar");
    }

    @Test
    public void givenProxyModeButNoHeaderWhenGetClientIpThenReturnNull() {
        enableProxyMode("foo", null, null, false);
        ClientSessionExtractor session = new ClientSessionExtractor(env);
        assertThat(session.getClientIp(serverRequest)).isNull();
    }

    @Test
    public void givenProxyModeWhenGetSslIdThenReturnHeader() {
        enableProxyMode(null, "foo", null, false);
        ClientSessionExtractor session = new ClientSessionExtractor(env);
        when(headers.containsKey("foo")).thenReturn(true);
        when(headers.getFirst("foo")).thenReturn("bar");
        assertThat(session.getClientSslId(serverRequest)).isEqualTo("bar");
    }

    @Test
    public void givenProxyModeButNoHeaderWhenGetSslIdThenReturnNull() {
        enableProxyMode(null, "foo", null, false);
        ClientSessionExtractor session = new ClientSessionExtractor(env);
        assertThat(session.getClientSslId(serverRequest)).isNull();
    }

    @Test
    public void givenTrustedSiteLocalAddressWhenGetClientIpThenReturnHeader() {
        enableProxyMode("foo", null, null, true);
        when(headers.containsKey("foo")).thenReturn(true);
        when(headers.getFirst("foo")).thenReturn("bar");
        when(inetAddress.isSiteLocalAddress()).thenReturn(true);
        ClientSessionExtractor session = new ClientSessionExtractor(env);
        assertThat(session.getClientIp(serverRequest)).isEqualTo("bar");
    }

    @Test
    public void givenTrustedLoopbackAddressWhenGetClientIpThenReturnHeader() {
        enableProxyMode("foo", null, null, true);
        when(headers.containsKey("foo")).thenReturn(true);
        when(headers.getFirst("foo")).thenReturn("bar");
        when(inetAddress.isLoopbackAddress()).thenReturn(true);
        ClientSessionExtractor session = new ClientSessionExtractor(env);
        assertThat(session.getClientIp(serverRequest)).isEqualTo("bar");
    }

    @Test
    public void givenNoTrustedSiteLocalAddressWhenGetClientIpThenReturnNull() {
        enableProxyMode("foo", null, null, true);
        when(headers.containsKey("foo")).thenReturn(true);
        when(headers.getFirst("foo")).thenReturn("bar");
        ClientSessionExtractor session = new ClientSessionExtractor(env);
        assertThat(session.getClientIp(serverRequest)).isNull();
    }

    @Test
    public void givenTrustedAddressWhenGetClientIpThenReturnHeader() {
        enableProxyMode("foo", null, "1.2.*.4", true);
        when(inetAddress.getHostAddress()).thenReturn("1.2.3.4");
        when(headers.containsKey("foo")).thenReturn(true);
        when(headers.getFirst("foo")).thenReturn("bar");
        ClientSessionExtractor session = new ClientSessionExtractor(env);
        assertThat(session.getClientIp(serverRequest)).isEqualTo("bar");
    }

    @Test
    public void givenUntrustedAddressWhenGetClientIpThenReturnNull() {
        enableProxyMode("foo", null, "1.2.3.4", true);
        when(inetAddress.getHostAddress()).thenReturn("2.3.4.5");
        when(headers.containsKey("foo")).thenReturn(true);
        when(headers.getFirst("foo")).thenReturn("bar");
        ClientSessionExtractor session = new ClientSessionExtractor(env);
        assertThat(session.getClientIp(serverRequest)).isNull();
    }

    @Test
    public void givenNoTrustCheckWhenGetClientThenIgnoreIp() {
        enableProxyMode("foo", null, "1.2.3.4", false);
        when(inetAddress.getHostAddress()).thenReturn("2.3.4.5");
        when(headers.containsKey("foo")).thenReturn(true);
        when(headers.getFirst("foo")).thenReturn("bar");
        ClientSessionExtractor session = new ClientSessionExtractor(env);
        assertThat(session.getClientIp(serverRequest)).isEqualTo("bar");
    }


    private void disableProxyMode() {
        when(env.getProperty("piri.proxy.mode.enabled", Boolean.class, false)).thenReturn(false);
        when(env.getProperty("piri.proxy.trust.check.enabled", Boolean.class, true)).thenReturn(true);
    }

    private void enableProxyMode(String remoteIpHeader, String remoteSslIdHeader, String trustedProxies, boolean trustChecked) {
        when(env.getProperty("piri.proxy.mode.enabled", Boolean.class, false)).thenReturn(true);
        when(env.getProperty("piri.proxy.trust.check.enabled", Boolean.class, true)).thenReturn(trustChecked);
        when(env.getProperty("piri.proxy.trusted.ip.regexp")).thenReturn(trustedProxies);
        when(env.getProperty("piri.proxy.remote-ip-header", "X-Forwarded-For")).thenReturn(remoteIpHeader);
        when(env.getProperty("piri.proxy.remote-ssl-id-header", "X-Real-SslId")).thenReturn(remoteSslIdHeader);
    }

}