/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonTestUtil {

    private static final Logger LOG = LoggerFactory.getLogger(JsonTestUtil.class);

    private static final ObjectMapper JSON = new ObjectMapper();

    public static String toJson(final Object toBeConverted) {
        try {
            return JSON.writeValueAsString(toBeConverted);
        } catch (JsonProcessingException e) {
            LOG.error("Could not convert: {}", toBeConverted);
            throw new RuntimeException("Could not convert to json", e);
        }
    }

}
