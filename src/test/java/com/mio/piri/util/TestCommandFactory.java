/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.util;

import com.mio.piri.commands.*;

import java.util.Arrays;
import java.util.Collections;

public class TestCommandFactory {

    public static String command(String command) {
        return String.format("{\"command\": \"%s\"}", command);
    }

    public static GetNeighbors getNeighborsCommand() {
        GetNeighbors getNeighbors = new GetNeighbors();
        getNeighbors.setCommand("getNeighbors");
        return getNeighbors;
    }

    public static AddNeighbors addNeighborsCommand(String uri) {
        AddNeighbors addNeighbors = new AddNeighbors();
        addNeighbors.setCommand("addNeighbors");
        addNeighbors.setUris(Collections.singletonList(uri));
        return addNeighbors;
    }

    public static RemoveNeighbors removeNeighborsCommand(String uri) {
        RemoveNeighbors removeNeighbors = new RemoveNeighbors();
        removeNeighbors.setCommand("removeNeighbors");
        removeNeighbors.setUris(Collections.singletonList(uri));
        return removeNeighbors;
    }

    public static AttachToTangle attachToTangleCommand(String trunk, String branch, String trytes) {
        AttachToTangle att = new AttachToTangle();
        att.setCommand("attachToTangle");
        att.setTrunkTransaction(trunk);
        att.setBranchTransaction(branch);
        att.setMinWeightMagnitude(14); // min allowed value for mainnet is 14. min value for testnet is 9.
        att.setTrytes(Collections.singletonList(trytes));
        return att;
    }

    public static StoreTransactions storeTransactions(String trytes) {
        StoreTransactions bt = new StoreTransactions();
        bt.setCommand("storeTransactions");
        bt.setTrytes(Collections.singletonList(trytes));
        return bt;
    }

    public static FindTransactions findTransactions(String address) {
        FindTransactions ft = new FindTransactions();
        ft.setCommand("findTransactions");
        ft.setApprovees(Collections.singletonList(address));
        return ft;
    }

    public static GetTrytes getTrytes(String... hash) {
        GetTrytes gt = new GetTrytes();
        gt.setCommand("getTrytes");
        gt.setHashes(Arrays.asList(hash));
        return gt;
    }

    public static GetInclusionStates getInclusionStates(String tx, String tip) {
        GetInclusionStates gis = new GetInclusionStates();
        gis.setCommand("getInclusionStates");
        gis.setTransactions(Collections.singletonList(tx));
        gis.setTips(Collections.singletonList(tip));
        return gis;
    }

    public static GetBalances getBalances(String address) {
        GetBalances gb = new GetBalances();
        gb.setCommand("getBalances");
        gb.setAddresses(Collections.singletonList(address));
        gb.setThreshold(100);
        return gb;
    }

    public static GetTransactionsToApprove getTransactionsToApprove() {
        GetTransactionsToApprove gta = new GetTransactionsToApprove();
        gta.setCommand("getTransactionsToApprove");
        gta.setDepth(1);
        return gta;
    }

    public static BroadcastTransactions broadcastTransactions(String trytes) {
        BroadcastTransactions bt = new BroadcastTransactions();
        bt.setCommand("broadcastTransactions");
        bt.setTrytes(Collections.singletonList(trytes));
        return bt;
    }

    public static WereAddressesSpentFrom wereAddressesSpentFrom(String address) {
        WereAddressesSpentFrom wsf = new WereAddressesSpentFrom();
        wsf.setCommand("wereAddressesSpentFrom");
        wsf.setAddresses(Collections.singletonList(address));
        return wsf;
    }

    public static CheckConsistency checkConsistency(String tail) {
        CheckConsistency cc = new CheckConsistency();
        cc.setCommand("checkConsistency");
        cc.setTails(Collections.singletonList(tail));
        return cc;
    }

}
