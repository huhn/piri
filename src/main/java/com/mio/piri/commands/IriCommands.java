/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands;

import org.apache.commons.lang3.StringUtils;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public enum IriCommands {

    ADD_NEIGHBORS("addNeighbors"),
    ATTACH_TO_TANGLE(AttachToTangle.COMMAND),
    BROADCAST_TRANSACTIONS("broadcastTransactions"),
    CHECK_CONSISTENCY("checkConsistency"),
    FIND_TRANSACTIONS("findTransactions"),
    GET_BALANCES("getBalances"),
    GET_INCLUSION_STATES("getInclusionStates"),
    GET_MISSING_TRANSACTIONS("getMissingTransactions"),
    GET_NEIGHBORS("getNeighbors"),
    GET_NODE_INFO(GetNodeInfo.COMMAND),
    GET_TIPS("getTips"),
    GET_TRANSACTIONS_TO_APPROVE(GetTransactionsToApprove.COMMAND),
    GET_TRYTES(GetTrytes.COMMAND),
    INTERRUPT_ATTACHING_TO_TANGLE("interruptAttachingToTangle"),
    REMOVE_NEIGHBORS("removeNeighbors"),
    STORE_TRANSACTIONS("storeTransactions"),
    WERE_ADDRESSES_SPENT_FROM("wereAddressesSpentFrom");

    public static Duration DEFAULT_TIMEOUT = Duration.ofSeconds(30);
    private final static Map<String, Duration> timeouts = new HashMap<>();

    private final String key;

    IriCommands(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public static IriCommands byKey(String wanted) {
        for (IriCommands iriCommand : IriCommands.values()) {
            if (StringUtils.equalsIgnoreCase(wanted, iriCommand.key)) {
                return iriCommand;
            }
        }
        return null;
    }

    public void setTimeout(Duration timeout) {
        timeouts.put(key, timeout);
    }

    public Duration getTimeout() {
        return getTimeout(key);
    }

    public static Duration getTimeout(String command) {
        return timeouts.computeIfAbsent(command, val -> DEFAULT_TIMEOUT);
    }


}
