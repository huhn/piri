/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class NodeInfo {

    public static final int INVALID_MILESTONE = -1;

    @JsonInclude(value=NON_EMPTY)
    private String appName;

    @JsonInclude(value=NON_EMPTY)
    private String appVersion;

    private int neighbors = 0;

    // @NotNull
    private long latestMilestoneIndex = INVALID_MILESTONE;

    private String latestMilestone = "";

    // @NotNull
    private long latestSolidSubtangleMilestoneIndex = INVALID_MILESTONE;

    private String latestSolidSubtangleMilestone = "";

    @JsonIgnore
    public boolean isSynced() {
        // milestones can be off by one for a short period of time
        return hasValidLatestMilestoneIndex() && latestSolidSubtangleMilestoneIndex + 1 >= latestMilestoneIndex;
    }

    @JsonIgnore
    private boolean hasValidLatestMilestoneIndex() {
        return latestMilestoneIndex > 0;
    }

}

