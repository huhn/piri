/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands.response;

import lombok.Builder;

public class ResponseDtoBuilder {

    @Builder(builderMethodName = "nodeInfoBuilder")
    public static NodeInfo buildNodeInfo(String appName, String appVersion, Integer neighbors, Integer latestMilestoneIndex, Integer latestSolidSubtangleMilestoneIndex) {
        NodeInfo nodeInfo = new NodeInfo();
        nodeInfo.setAppName(appName);
        nodeInfo.setAppVersion(appVersion);
        if (neighbors != null) {
            nodeInfo.setNeighbors(neighbors);
        }
        if (latestMilestoneIndex != null) {
            nodeInfo.setLatestMilestoneIndex(latestMilestoneIndex);
        }
        if (latestSolidSubtangleMilestoneIndex != null) {
            nodeInfo.setLatestSolidSubtangleMilestoneIndex(latestSolidSubtangleMilestoneIndex);
        }
        return nodeInfo;
    }

}
