/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.commands;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.EXISTING_PROPERTY, visible = true, property="command")
@JsonSubTypes({
        @Type(value = GetNodeInfo.class, name = GetNodeInfo.COMMAND),
        @Type(value = GetTips.class, name = "getTips"),
        @Type(value = FindTransactions.class, name = "findTransactions"),
        @Type(value = GetTrytes.class, name = GetTrytes.COMMAND),
        @Type(value = GetInclusionStates.class, name = "getInclusionStates"),
        @Type(value = GetBalances.class, name = "getBalances"),
        @Type(value = GetTransactionsToApprove.class, name = GetTransactionsToApprove.COMMAND),
        @Type(value = AttachToTangle.class, name = AttachToTangle.COMMAND),
        @Type(value = InterruptAttachingToTangle.class, name = "interruptAttachingToTangle"),
        @Type(value = BroadcastTransactions.class, name = "broadcastTransactions"),
        @Type(value = StoreTransactions.class, name = "storeTransactions"),
        @Type(value = WereAddressesSpentFrom.class, name = "wereAddressesSpentFrom"),
        @Type(value = CheckConsistency.class, name = "checkConsistency"),
        @Type(value = GetMissingTransactions.class, name = "getMissingTransactions"),
        @Type(value = GetNeighbors.class, name = "getNeighbors"),
        @Type(value = AddNeighbors.class, name = "addNeighbors"),
        @Type(value = RemoveNeighbors.class, name = "removeNeighbors")
})
public abstract class IriCommand {

    @Null
    @JsonIgnore
    private String sessionId;

    @Null
    @JsonIgnore
    private String ip;

    @NotBlank
    @Size(max = 32)
    private String command;

    @JsonIgnore
    public boolean wantsSameNodePerClient() {
        return true;
    }

    // true: needs a node that is not delayed
    // false: doesn't care
    @JsonIgnore
    public boolean needsUpToDateNode() {
        return true;
    }

    // true: can live with a slight delay if not possible otherwise
    // false: doesn't care
    @JsonIgnore
    public boolean wantsUpToDateNode() {
        return true;
    }

    @JsonIgnore
    private final List<String> executionHistory = new ArrayList<>();

}
