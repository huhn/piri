/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.SslInfo;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.regex.Pattern;

public class ClientSessionExtractor {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final boolean proxyEnabled;
    private final String remoteIpHeader;
    private final String remoteSslIdHeader;
    private final Pattern trustedProxies;
    private final boolean trustCheckEnabled;

    public ClientSessionExtractor(Environment env) {
        proxyEnabled = env.getProperty("piri.proxy.mode.enabled", Boolean.class, false);
        String trustedIps = env.getProperty("piri.proxy.trusted.ip.regexp");
        trustedProxies = StringUtils.isNotBlank(trustedIps) ? Pattern.compile(trustedIps) : null;
        remoteIpHeader = env.getProperty("piri.proxy.remote-ip-header", "X-Forwarded-For");
        remoteSslIdHeader = env.getProperty("piri.proxy.remote-ssl-id-header", "X-Real-SslId");
        trustCheckEnabled = env.getProperty("piri.proxy.trust.check.enabled", Boolean.class, true);
        Logger logger = LoggerFactory.getLogger(getClass());
        if (proxyEnabled) {
            if (trustCheckEnabled) {
                logger.info("Trusted proxy IP pattern: [{}]", trustedIps);
            } else {
                logger.warn("Proxy trust check disabled. Trusting all IPs.");
            }
            logger.info("Proxy remote IP Header: [{}]", remoteIpHeader);
            logger.info("Proxy remote SslId Header: [{}]", remoteSslIdHeader);
        } else {
            logger.info("Proxy mode not enabled.");
        }
    }

    public String getClientIp(ServerHttpRequest request) {
        return proxyEnabled ? getProxyClientIp(request) : ip(inetAddress(request.getRemoteAddress()));
    }

    private String getProxyClientIp(ServerHttpRequest request) {
        return containsHeaderAndIsTrusted(request, remoteIpHeader)
                ? request.getHeaders().getFirst(remoteIpHeader)
                : null;
    }

    public String getClientSslId(ServerHttpRequest request) {
        return proxyEnabled ? getProxyClientSslSessionId(request) : sslId(request.getSslInfo());
    }

    private String getProxyClientSslSessionId(ServerHttpRequest request) {
        return containsHeaderAndIsTrusted(request, remoteSslIdHeader)
                ? request.getHeaders().getFirst(remoteSslIdHeader)
                : null;
    }

    private boolean containsHeaderAndIsTrusted(ServerHttpRequest request, String header) {
        return containsHeader(request, header) && (!trustCheckEnabled || isTrustedProxy(inetAddress(request.getRemoteAddress())));
    }

    private boolean containsHeader(ServerHttpRequest request, String header) {
        boolean containsHeader = request.getHeaders().containsKey(header);
        if (!containsHeader) {
            logger.debug("Header [{}] missing.", header);
        }
        return containsHeader;
    }

    private boolean isTrustedProxy(InetAddress proxyAddress) {
        String ip = ip(proxyAddress);
        boolean trusted = ip != null && isTrustedProxy(proxyAddress, ip);
        if (!trusted) {
            logger.warn("Untrusted proxy IP: [{}].", ip);
        }
        return trusted;
    }

    private boolean isTrustedProxy(InetAddress proxyAddress, String ip) {
        return trustedProxies == null
                ? (proxyAddress.isLoopbackAddress() || proxyAddress.isSiteLocalAddress())
                : trustedProxies.matcher(ip).matches();
    }

    private String sslId(SslInfo sslInfo) {
        return sslInfo == null ? null : sslInfo.getSessionId();
    }

    private String ip(InetAddress address) {
        return address == null ? null : address.getHostAddress();
    }

    private InetAddress inetAddress(InetSocketAddress address) {
        return address == null ? null : address.getAddress();
    }


}
