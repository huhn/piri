/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.tolerance;

import com.mio.piri.metrics.MeterFactory;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.ratelimiter.RateLimiterConfig;
import io.github.resilience4j.ratelimiter.internal.AtomicRateLimiter;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.convert.DurationStyle;
import org.springframework.core.env.Environment;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

public class RateLimiterRegistry {

    private static final String NAME_IP_COMMAND = "%s-%s";
    private static final String NAME_IP = "%s";
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final int DEFAULT_LIMIT = 10;
    private static final int DEFAULT_MAX_RATE_LIMITERS_COUNT = 1000;

    private static final String NAME_MUST_NOT_BE_NULL = "Name must not be null";
    private static final String CONFIG_MUST_NOT_BE_NULL = "Config must not be null";

    private final RateLimiterConfig ipRateLimiterConfig;
    private final Map<String, RateLimiterConfig> rateLimitedCommandsConfig = new HashMap<>();

    private final Map<String, RateLimiter> rateLimiters = new ConcurrentHashMap<>();

    private final int maxRateLimitersCount;
    private final Duration idleTime;
    private final Map<String, Instant> accessTime = new ConcurrentHashMap<>();

    private final Pattern noRateLimitForPattern;

    private final RateLimiter global;

    public RateLimiterRegistry(Environment env, MeterFactory meterFactory) {
        ipRateLimiterConfig = RateLimiterConfig.custom()
                .limitRefreshPeriod(getRefreshPeriod(env, "ip"))
                .limitForPeriod(getLimitPerPeriod(env, "ip"))
                .timeoutDuration(getTimeoutDuration(env, "ip"))
                .build();
        maxRateLimitersCount = getMaxRateLimitersCount(env);
        idleTime = getIdleTime(env);

        RateLimiterConfig globalIriRateLimiterConfig = RateLimiterConfig.custom()
                .limitRefreshPeriod(getRefreshPeriod(env, "global"))
                .limitForPeriod(getLimitPerPeriod(env, "global"))
                .timeoutDuration(getTimeoutDuration(env, "global"))
                .build();
        global = rateLimiter("global", globalIriRateLimiterConfig);

        Arrays.stream(getRateLimitedCommands(env)).forEach(str -> rateLimitedCommandsConfig.put(str,
                RateLimiterConfig.custom()
                        .limitRefreshPeriod(getRefreshPeriod(env, "ip." + str))
                        .limitForPeriod(getLimitPerPeriod(env, "ip." + str))
                        .timeoutDuration(getTimeoutDuration(env, "ip." + str))
                        .build()
                ));
        meterFactory.createRateLimiterCountGauge(rateLimiters);

        String noLimitPattern = StringUtils.stripToNull(env.getProperty("piri.rate.ip.unlimited.pattern"));
        if (noLimitPattern == null) {
            logger.debug("No rate limit exception configured.");
            noRateLimitForPattern = null;
        } else {
            logger.info("Set rate limit exception for IPs matching: [{}]", noLimitPattern);
            noRateLimitForPattern = Pattern.compile(noLimitPattern);
        }

    }

    public RateLimiter globalRateLimiter() {
        return global;
    }

    public RateLimiter rateLimiter(final String ip, final String command) {
        Tuple2<String, RateLimiterConfig> limiterSpecification = getLimiterInfo(ip, command);
        accessTime.put(limiterSpecification._1, Instant.now());
        cleanUpIfCacheIsFull();
        return rateLimiter(limiterSpecification._1, limiterSpecification._2);
    }

    public boolean isIpRateLimited(String ip) {
        // rate limit if no exception defined or ip does not match pattern
        return noRateLimitForPattern == null || StringUtils.isBlank(ip) || !noRateLimitForPattern.matcher(ip).matches();
    }

    private Tuple2<String, RateLimiterConfig> getLimiterInfo(final String ip, final String command) {
        return isCommandRateLimited(command)
                ? Tuple.of(String.format(NAME_IP_COMMAND, ip, command), rateLimitedCommandsConfig.get(command))
                : Tuple.of(String.format(NAME_IP, ip), ipRateLimiterConfig);
    }

    private boolean isCommandRateLimited(String command) {
        return rateLimitedCommandsConfig.containsKey(command);
    }

    private RateLimiter rateLimiter(final String name, final RateLimiterConfig rateLimiterConfig) {
        requireNonNull(name, NAME_MUST_NOT_BE_NULL);
        requireNonNull(rateLimiterConfig, CONFIG_MUST_NOT_BE_NULL);
        return rateLimiters.computeIfAbsent(
                name,
                limitName -> new AtomicRateLimiter(name, rateLimiterConfig)
        );
    }

    private void cleanUpIfCacheIsFull() {
        if (accessTime.size() > maxRateLimitersCount) {
            Set<String> oldRateLimiters = accessTime.entrySet()
                    .stream().filter(entry -> Instant.now().isAfter(entry.getValue().plus(idleTime)))
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toSet());
            rateLimiters.keySet().removeAll(oldRateLimiters);
            accessTime.keySet().removeAll(oldRateLimiters);
            if (oldRateLimiters.size() > 0) {
                logger.info("Removed [{}] rate limiters from cache.", oldRateLimiters.size());
            }
        }
    }

    private Duration getRefreshPeriod(Environment env, String type) {
        String durationString = env.getProperty("piri.rate." + type + ".period");
        Duration duration = durationString == null
                ? Duration.ofSeconds(1)
                : DurationStyle.detectAndParse(durationString);
        logger.info("Configured [{}] rate limit period: [{}].", type, duration);
        return duration;
    }

    private Duration getTimeoutDuration(Environment env, String type) {
        String durationString = env.getProperty("piri.rate." + type + ".timeout");
        Duration duration = durationString == null
                ? Duration.ofSeconds(1)
                : DurationStyle.detectAndParse(durationString);
        logger.info("Configured [{}] rate limit timeout: [{}].", type, duration);
        return duration;
    }

    private int getLimitPerPeriod(Environment env, String type) {
        int value = getInt(env, "piri.rate." + type + ".limit", DEFAULT_LIMIT);
        logger.info("Configured [{}] rate limit per period: [{}].", type, value);
        return value;
    }

    private int getMaxRateLimitersCount(Environment env) {
        int value = getInt(env, "piri.rate.cleanup.max.count", DEFAULT_MAX_RATE_LIMITERS_COUNT);
        logger.info("Configured [ip] rate limiters cache size: [{}].", value);
        return value;
    }

    private Duration getIdleTime(Environment env) {
        String durationString = env.getProperty("piri.rate.cleanup.idle");
        Duration duration = durationString == null
                ? Duration.ofMinutes(10)
                : DurationStyle.detectAndParse(durationString);
        logger.info("Configured [ip] rate limiter idle time: [{}].", duration);
        return duration;
    }

    private int getInt(Environment env, String key, int defaultValue) {
        Integer value = env.getProperty(key, Integer.class);
        return value == null ? defaultValue : value;
    }

    private String[] getRateLimitedCommands(Environment env) {
        return splitList(env.getProperty("piri.rate.ip.limited.commands"));
    }

    private static String[] splitList(String strings) {
        String[] split = StringUtils.split(StringUtils.remove(strings, ' '), ',');
        return split == null ? new String[]{} : split;
    }

}
