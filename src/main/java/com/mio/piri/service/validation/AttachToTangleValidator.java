/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service.validation;

import com.mio.piri.commands.AttachToTangle;
import org.springframework.core.env.Environment;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


public class AttachToTangleValidator implements Validator {

    private final int max;
    private final int min;

    private final MinMaxValidator minMaxValidator;

    public AttachToTangleValidator(Environment env, MinMaxValidator minMaxValidator) {
        this.minMaxValidator = minMaxValidator;
        min = env.getProperty("iri.attachToTangle.minWeightMagnitude.min", Integer.class, AttachToTangle.DEFAULT_WEIGHT_MIN);
        max = env.getProperty("iri.attachToTangle.minWeightMagnitude.max", Integer.class, AttachToTangle.DEFAULT_WEIGHT_MAX);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return AttachToTangle.class.equals(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        AttachToTangle command = (AttachToTangle) target;
        // command.getMinWeightMagnitude can be null here
        //noinspection ConstantConditions
        minMaxValidator.validate(command.getMinWeightMagnitude() == null ? 0 : command.getMinWeightMagnitude(), min, max, "minWeightMagnitude", errors);
    }

}
