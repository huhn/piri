/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.service.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Arrays;
import java.util.List;

public class DelegatingValidator implements Validator {

    private final Class<?> supportedClass;
    private final List<Validator> validators;

    public DelegatingValidator(Class<?> supportedClass, Validator... validators) {
        this.supportedClass = supportedClass;
        this.validators = Arrays.asList(validators);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return supportedClass.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        for (Validator validator : validators) {
            if (validator.supports(target.getClass())) {
                validator.validate(target, errors);
            }
        }
    }
}
