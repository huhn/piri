/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */
package com.mio.piri.service;

import com.mio.piri.commands.AttachToTangle;
import com.mio.piri.commands.IriCommand;
import com.mio.piri.exceptions.ExceptionHandlerUtil;
import com.mio.piri.exceptions.NoNodeAvailable;
import com.mio.piri.metrics.MeterFactory;
import com.mio.piri.nodes.Node;
import com.mio.piri.nodes.selection.NodeSelector;
import com.mio.piri.tolerance.RateLimiterRegistry;
import com.mio.piri.util.ClientSessionExtractor;
import com.mio.piri.service.validation.DelegatingValidator;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.ratelimiter.RequestNotPermitted;
import io.github.resilience4j.reactor.ratelimiter.operator.RateLimiterOperator;
import io.micrometer.core.instrument.Counter;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.codec.DecodingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import javax.validation.Valid;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

@RestController
public class IriApiHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Logger commandLogger = LoggerFactory.getLogger("commands");

    @Autowired
    private CommandChecker commandChecker;

    @Autowired
    private MeterFactory metrics;

    @Autowired
    private DelegatingValidator commandValidator;

    @Autowired
    private ClientSessionExtractor session;

    @Autowired
    private RateLimiterRegistry rateLimiterRegistry;

    @Autowired
    private NodeSelector nodeSelector;

    private final Map<String, Counter> rejectedCommandsCounter = new ConcurrentHashMap<>();
    private final Map<Tuple2<String, String>, Counter> failedCommandsCounter = new ConcurrentHashMap<>();

    @CrossOrigin
    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> exchangeWithIri(@Valid @RequestBody IriCommand command, ServerHttpRequest request) {

        String ip = session.getClientIp(request);
        String sslId = session.getClientSslId(request);
        command.setSessionId(StringUtils.isBlank(sslId) ? ip : sslId);
        command.setIp(ip);

        // TODO add better exception handling if retries fail (hide internal errors with node name etc.)
        Mono<ResponseEntity<String>> responseEntityMono = executeCommand(command)
                .doOnNext(responseEntity -> {
                    if (responseEntity.getStatusCode().isError()) {
                        commandLogger.warn("Command [{}] returned error [{}] with code [{}]. IP [{}] Details [{}] Id [{}]",
                                command.getCommand(), responseEntity.getBody(), responseEntity.getStatusCode(), ip, command, command.getSessionId());
                    }
                })
                .onErrorResume(retryPredicate(), throwable -> retry(throwable, command))
                .onErrorResume(retryPredicate(), throwable -> retry(throwable, command))
                .onErrorResume(retryPredicate(), throwable -> retry(throwable, command))
                .onErrorResume(retryPredicate(), throwable -> retry(throwable, command));

        if (rateLimiterRegistry.isIpRateLimited(ip)) {
            RateLimiter rateLimiter = rateLimiterRegistry.rateLimiter(ip, command.getCommand());
            RateLimiter globalLimiter = rateLimiterRegistry.globalRateLimiter();
            responseEntityMono = responseEntityMono
                    .transform(RateLimiterOperator.of(rateLimiter))
                    .transform(RateLimiterOperator.of(globalLimiter));
        }

        return responseEntityMono;

    }

    private Mono<ResponseEntity<String>> retry(Throwable t, IriCommand command) {
        failedCommandCounter(command.getCommand(), t).increment();
        logger.info("Retry: {}", StringUtils.abbreviate(command.toString(), 100));
        if (logger.isTraceEnabled()) {
            logger.trace(t.toString(), t);
        }
        return executeCommand(command);
    }

    private Mono<ResponseEntity<String>> executeCommand(IriCommand command) {
        return commandChecker.isEnabled(command) ? forward(command) : unauthorized(command);
    }

    private Mono<ResponseEntity<String>> forward(IriCommand command) {
        List<Node> nodes = nodeSelector.selectNodes(command);
        if (nodes.isEmpty()) {
            String msg = String.format("Rejecting command [%s] because no node is available.", command.getCommand());
            logger.warn(msg);
            return Mono.error(new NoNodeAvailable(msg));
        } else {
            Node node = nodes.get();
            logCommand(node.getName(), command);
            command.getExecutionHistory().add(node.getName());
            return node.call(command);
        }
    }

    private void logCommand(String node, IriCommand command) {
        Mono.fromRunnable(() -> commandLogger.info("IP [{}] N [{}] C [{}] D [{}] Id [{}]",
                command.getIp(), node, command.getCommand(), command, command.getSessionId()))
                .subscribeOn(Schedulers.elastic())
                .subscribe();
    }

    private Predicate<Throwable> retryPredicate() {
        return throwable -> !(throwable instanceof NoNodeAvailable);
    }

    private Mono<ResponseEntity<String>> unauthorized(IriCommand command) {
        rejectedCommandCounter(command.getCommand()).increment();
        return Mono.just(createUnauthorizedResponse(command));
    }

    @ExceptionHandler(DecodingException.class)
    public ResponseEntity handleDecodingErrors(DecodingException dex) {
        rejectedCommandCounter("unreadable").increment();
        logger.warn("Decoding error: {}", dex.getMessage());
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Failed to read HTTP message");
    }

    @ExceptionHandler(WebExchangeBindException.class)
    public ResponseEntity handleInvalidCommand(WebExchangeBindException wex) {
        rejectedCommandCounter("invalid").increment();
        logger.info("Validation failure: [{}]", StringUtils.abbreviate(String.valueOf(wex.getTarget()), 116));
        String errorMessage = ExceptionHandlerUtil.extractValidationErrorMessage(wex);

        if (wex.getTarget() instanceof IriCommand && !commandChecker.isEnabled((IriCommand) wex.getTarget())) {
            // return unauthorized as IRI does
            logger.info(errorMessage);
            return createUnauthorizedResponse((IriCommand) wex.getTarget());
        } else if (wex.getTarget() instanceof AttachToTangle) {
            // exactly this response is needed for pow check
            logger.debug("Return POW enabled response.");
            return invalidCommandResponse();
        } else {
            // this is not 100% compatible with IRI as the error message is much more detailed. If this
            // is a problem change to invalidCommandResponse
            throw ExceptionHandlerUtil.convertToResponseException(wex, errorMessage);
        }
    }

    @ExceptionHandler({RequestNotPermitted.class, NoNodeAvailable.class})
    public ResponseEntity handleRateLimitReached(RuntimeException re) {
        rejectedCommandCounter("limit").increment();
        logger.info("Too many requests. {}", re.getMessage());
        throw new ResponseStatusException(HttpStatus.TOO_MANY_REQUESTS, "Rate limit reached or no node available. Try again later.");
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(commandValidator);
    }

    private Counter rejectedCommandCounter(String command) {
        return rejectedCommandsCounter.computeIfAbsent(command, key -> metrics.createRejectedCommandCounter(command));
    }

    private Counter failedCommandCounter(String command, Throwable throwable) {
        String error = throwable.getClass().getSimpleName();
        return failedCommandsCounter.computeIfAbsent(Tuple.of(command, error), key -> metrics.createFailedCommandCounter(command, error));
    }

    private ResponseEntity<String> invalidCommandResponse() {
        return ResponseEntity.badRequest()
                .contentType(MediaType.APPLICATION_JSON)
                .body("{\"error\":\"Invalid parameters\",\"duration\":0}");
    }

    private ResponseEntity<String> createUnauthorizedResponse(IriCommand command) {
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .contentType(MediaType.APPLICATION_JSON)
                .body("{\"error\":\"COMMAND " + command.getCommand() + " is not available on this node\",\"duration\":0}");
    }

}
