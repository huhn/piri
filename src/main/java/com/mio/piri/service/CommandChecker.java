/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */
package com.mio.piri.service;

import com.mio.piri.commands.IriCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CommandChecker {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final String[] supportedCommands = {
            "attachToTangle",
            "getNeighbors",
            "addNeighbors",
            "removeNeighbors",
            "getNodeInfo",
            "getTips",
            "findTransactions",
            "getTrytes",
            "getInclusionStates",
            "getBalances",
            "getTransactionsToApprove",
            "interruptAttachingToTangle",
            "broadcastTransactions",
            "storeTransactions",
            "wereAddressesSpentFrom",
            "checkConsistency",
            "getMissingTransactions"
    };

    private final Set<String> enabledCommands = new HashSet<>();

    public CommandChecker(Environment env) {
        Arrays.stream(supportedCommands).forEach(commandString -> {
            if (env.getProperty("iri.allow." + commandString, Boolean.class, false)) {
                enabledCommands.add(commandString);
            } else {
                logger.info("Command [{}] is disabled.", commandString);
            }
        });
        enabledCommands.forEach(commandString -> logger.debug("Command [{}] is enabled.", commandString));
    }

    public boolean isEnabled(IriCommand iriCommand) {
        return iriCommand != null && enabledCommands.contains(iriCommand.getCommand());
    }

}
