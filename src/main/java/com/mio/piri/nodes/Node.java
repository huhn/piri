/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.mio.piri.commands.IriCommand;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.time.Duration;
import java.util.function.Consumer;

public interface Node {

    String getName();
    String getKey();
    String getUrl();
    URI getUri();
    Duration getTimeout(IriCommand command);

    Mono<ResponseEntity<String>> call(IriCommand command);
    boolean isSupported(IriCommand command);

    boolean isPowEnabled();
    boolean isCallPossible();
    boolean isSynced();
    long getDelay();

    Consumer<HttpHeaders> getHeadersConsumer();

}
