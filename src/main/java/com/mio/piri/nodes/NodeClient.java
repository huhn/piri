/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.mio.piri.commands.AttachToTangle;
import com.mio.piri.commands.IriCommand;
import com.mio.piri.commands.IriCommands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import static java.util.logging.Level.FINE;
import static reactor.core.publisher.SignalType.ON_NEXT;

public class NodeClient {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private WebClient webClient;

    public Mono<ResponseEntity<String>> post(Node node, IriCommand command) {
        return webClient
                .post()
                .uri(node.getUri())
                .headers(node.getHeadersConsumer())
                .syncBody(command)
                .exchange()
                .flatMap(response -> {
                    Mono<ResponseEntity<String>> responseMono = response.toEntity(String.class);
                    handleErrors(node.getName(), command, response);
                    return responseMono;
                })
                .timeout(node.getTimeout(command))
                .log(logger.getName(), FINE, ON_NEXT)
                .doOnError(e -> logger.warn("Command [{}] on Node [{}] failed: {}", command.getCommand(), node.getName(), e.toString()))
                // .transform(CircuitBreakerOperator.of(circuitBreaker)) // TODO do not forget me! Nodes should circuit break outside. Maybe node users should do it.
                ;
    }

    public <T> Mono<T> retrieve(Node node, IriCommand command, Class<T> responseType) {
        return webClient.post()
                .uri(node.getUri())
                // TODO .headers(node.getHeadersConsumer())
                .syncBody(command)
                .retrieve()
                .bodyToMono(responseType)
                .timeout(node.getTimeout(command))
                .log(logger.getName(), FINE, ON_NEXT)
                ;
    }

    /**
     * Logs certain errors and throws for return codes that cannot be the result of a successful call. Checking http status
     * only won't work as IRI returns 500 for gTTA with depth that doesn't find the reference transaction for example.
     * @param command The executed command.
     * @param response The response that might contain errors.
     */
    private void handleErrors(String nodeName, IriCommand command, ClientResponse response) {
        HttpStatus httpStatus = response.statusCode();
        if (httpStatus.isError() && !successfulPowCheckResult(command, httpStatus)) { // don't log for successful pow check
            String msg = String.format("Node [%s] executing command [%s] returned error [%s].",
                    nodeName, command.getCommand(), httpStatus.value());
            logger.info(msg);
            if (isCriticalError(command, httpStatus)) {
                response.bodyToMono(String.class).subscribe(
                        result -> logger.info(msg + " " + result),
                        t -> logger.info(msg + " " + t.toString())
                );
                throw new WebClientResponseException(msg,
                        httpStatus.value(), httpStatus.getReasonPhrase(), response.headers().asHttpHeaders(),
                        null, null);
            }
        }
    }

//    private void throwOnError(String nodeName, IriCommand command, ClientResponse response) {
//        HttpStatus httpStatus = response.statusCode();
//        if (httpStatus.isError()) {
//            logger.info("Node [{}] executing command [{}] returned error [{}]", nodeName, command.getCommand(), httpStatus.value());
//            throw new WebClientResponseException("Node " + nodeName + " returned erroneous response.",
//                    httpStatus.value(), httpStatus.getReasonPhrase(), response.headers().asHttpHeaders(),
//                    null, null);
//        }
//    }

    private boolean isCriticalError(IriCommand command, HttpStatus httpStatus) {
        // TODO fix this after IRI API is refactored and documented with proper response codes.
        return !(isGetTransactionsToApproveAttempt(command, httpStatus) || isBadRequest(httpStatus));
    }

    private boolean isBadRequest(HttpStatus httpStatus) {
        return httpStatus == HttpStatus.BAD_REQUEST;
    }

    private boolean isGetTransactionsToApproveAttempt(IriCommand command, HttpStatus httpStatus) {
        // gTTA with insufficient depth IRI 1.5.4 and before
        return httpStatus == HttpStatus.INTERNAL_SERVER_ERROR && IriCommands.GET_TRANSACTIONS_TO_APPROVE == IriCommands.byKey(command.getCommand());
    }

    private boolean successfulPowCheckResult(IriCommand command, HttpStatus httpStatus) {
        return httpStatus == HttpStatus.BAD_REQUEST
                && command instanceof AttachToTangle
                && ((AttachToTangle) command).isEmptyRequest();
    }


    public void setWebClient(WebClient webClient) {
        this.webClient = webClient;
    }
}
