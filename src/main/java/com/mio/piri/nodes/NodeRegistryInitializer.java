/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.mio.piri.metrics.MeterFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import java.util.Map;

public class NodeRegistryInitializer {

    private static final Logger LOG = LoggerFactory.getLogger(NodeRegistryInitializer.class);

    // TODO try to move this into the piri initialization bean

    public static void initConfiguredNodes(NodeRegistry nodeRegistry, Environment env) {
        for (String nodeName : splitNodeList(env.getProperty("iri.nodes"))) {
            String powEnabled = env.getProperty("iri." + nodeName + ".pow");
            registerNodeAndSkipIfError(nodeRegistry, nodeName, env.getRequiredProperty("iri." + nodeName + ".url"), nodeName, Boolean.parseBoolean(powEnabled));
        }
        for (String nodeName : splitNodeList(env.getProperty("pow.nodes"))) {
            PowNode powNode = nodeRegistry.registerPowNode(nodeName, env.getRequiredProperty("pow." + nodeName + ".url"), nodeName);
            String authHeaderValue = env.getProperty("pow." + nodeName + ".header.Authorization");
            if (StringUtils.isNotBlank(authHeaderValue)) {
                powNode.setHeadersConsumer(httpHeaders -> httpHeaders.add("Authorization", authHeaderValue));
            }
        }
    }

    public static void initPersistedNodes(NodeRegistry nodeRegistry, NodesRepository nodesRepository) {
        for (NodeProperties nodeProperties : nodesRepository.getAllNodes()) {
            registerNodeAndSkipIfError(nodeRegistry, nodeProperties.name, nodeProperties.url, nodeProperties.key, nodeProperties.pow);
        }
    }

    private static void registerNodeAndSkipIfError(NodeRegistry nodeRegistry, String name, String url, String key, boolean pow) {
        try {
            IriNode node = nodeRegistry.registerIriNode(name, url, key, pow);
            healthCheckNode(nodeRegistry, node);
        } catch (Exception e) {
            LOG.debug(e.toString(), e);
            LOG.error("Skipping registration of node [{}/{}/{}/{}] because of error: [{}].", name, url, key, pow, e.toString());
        }
    }


    private static void healthCheckNode(NodeRegistry nodeRegistry, IriNode node) {
        node.updateLatestKnownMilestone(nodeRegistry.getLatestKnownMilestone());
        node.queryNodeHealth().subscribe(x -> {}, t -> LOG.error(t.toString(), t));
    }

    public static void initNodeGauges(MeterFactory meterFactory, Map<String, IriNode> nodes) {
        meterFactory.createTotalNodesGauge(nodes);
        meterFactory.createActiveNodesGauge(nodes);
        meterFactory.createInactiveNodesGauge(nodes);
        meterFactory.createSyncedNodesGauge(nodes);
    }

    private static String[] splitNodeList(String nodesList) {
        String[] split = StringUtils.split(StringUtils.remove(nodesList, ' '), ',');
        return split == null ? new String[]{} : split;
    }
}
