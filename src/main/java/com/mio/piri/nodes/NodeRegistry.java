/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.exceptions.CannotRegisterNode;
import com.mio.piri.metrics.MeterFactory;
import com.mio.piri.tolerance.CircuitBreakerFactory;
import com.mio.piri.util.UrlValidator;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.convert.DurationStyle;
import org.springframework.core.env.Environment;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static com.mio.piri.exceptions.CannotRegisterNode.CannotRegisterReason.CONFLICTING;
import static com.mio.piri.exceptions.CannotRegisterNode.CannotRegisterReason.INVALID;

public class NodeRegistry {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final Map<String, IriNode> nodes = new ConcurrentHashMap<>();
    private final List<String> nodeNames = Collections.synchronizedList(new ArrayList<>());

    @SuppressWarnings("CanBeFinal") // injected in test
    private UrlValidator urlValidator = new UrlValidator();

    @Autowired
    private NodeClient nodeClient;

    @Autowired
    private MeterFactory meterFactory;

    @Autowired
    private CircuitBreakerFactory circuitBreakerFactory;

    @Autowired
    private NodesRepository nodesRepository;

    @Autowired
    private Environment env;

    private final AtomicLong latestKnownMilestone = new AtomicLong(0);

    @PostConstruct
    public void initialize() {

        // timeouts need to be initialized here already!

        NodeRegistryInitializer.initConfiguredNodes(this, env);
        NodeRegistryInitializer.initPersistedNodes(this, nodesRepository);
        NodeRegistryInitializer.initNodeGauges(meterFactory, nodes);

        Duration healthInterval = DurationStyle.detectAndParse(env.getProperty("piri.health.check.interval", "3m"));
        Flux.interval(Duration.ofSeconds(10), healthInterval).subscribe(
                checkNumber -> runHealthCheck(nodes, checkNumber),
                t -> logger.error(t.toString(), t)
        );

        Duration powCheckInterval = DurationStyle.detectAndParse(env.getProperty("piri.pow.check.interval", "30m"));
        Flux.interval(Duration.ofMinutes(1), powCheckInterval).subscribe(checkNumber -> runPowCheck(nodes, checkNumber),
                t -> logger.error(t.toString(), t));

    }

    public IriNode registerIriNode(String name, String url, String key, boolean pow) {
        assertNodePropertiesAndThrowIfFailure(name, url, key);
        CircuitBreaker circuitBreaker = circuitBreakerFactory.circuitBreaker(name);
        IriNode node = new IriNode(name, url, key, pow, nodeClient, meterFactory, circuitBreaker);
        nodes.put(node.getName(), node);
        nodeNames.add(name);
        logger.info("Added node {}. Total number of nodes: [{}].", node, nodes.size());
        return node;
    }

    public PowNode registerPowNode(String name, String url, String key) {
        assertNodePropertiesAndThrowIfFailure(name, url, key);
        CircuitBreaker circuitBreaker = circuitBreakerFactory.circuitBreaker(name);
        PowNode node = new PowNode(name, url, key, nodeClient, meterFactory, circuitBreaker);
        nodes.put(node.getName(), node);
        logger.info("Added POW node {}. Total number of nodes: [{}].", node, nodes.size());
        return node;
    }

    private void assertNodePropertiesAndThrowIfFailure(String name, String url, String key) {
        validateNodeParameters(name, url, key);
        if (nodes.containsKey(name) || nodeNames.contains(name)) {
            String msg = String.format("Node with name [%s] is already registered.", name);
            logger.debug("Cannot register node. {}", msg);
            throw new CannotRegisterNode(CONFLICTING, msg);
        }
        Collection<? extends Node> nodes = this.nodes.values();
        if (nodes.stream().anyMatch(n -> urlValidator.areEqual(url, n.getUrl()))
                || nodes.stream().anyMatch(n -> urlValidator.resolveToSameAddress(url, n.getUrl()))) {
            String msg = String.format("Node with url [%s] is already registered.", url);
            logger.debug("Cannot register node. {}", msg);
            throw new CannotRegisterNode(CONFLICTING, msg);
        }
    }

    public IriNode unregisterNode(String name) {
        if (nodeNames.contains(name) || nodes.containsKey(name)) {
            logger.info("Unregistering node with name [{}]...", name);
            IriNode node = nodes.get(name);
            boolean removed = nodeNames.remove(name);
            if (!removed) {
                logger.warn("Could not remove node [{}] from node names list {}", name, nodeNames);
            }
            nodes.remove(name);
            return node;
        } else {
            logger.info("Cannot unregister node with name [{}]. Not found.", name);
            return null;
        }
    }

    public io.vavr.collection.Map<String, Node> getAllNodes() {
        return io.vavr.collection.HashMap.ofAll(nodes);
    }


    private void validateNodeParameters(String name, String url, String key) {
        if (!isValidNodeName(name)) {
            String msg = String.format("Invalid node name: [%s].", name);
            logger.debug(msg);
            throw new CannotRegisterNode(INVALID, msg);
        }
        if (!isValidNodeUrl(url)) {
            String msg = String.format("Invalid node url: [%s].", url);
            logger.debug(msg);
            throw new CannotRegisterNode(INVALID, msg);
        }
        if (!isValidNodeKey(key)) {
            String msg = String.format("Invalid key [%s] for node: [%s/%s].", key, name, url);
            logger.debug(msg);
            throw new CannotRegisterNode(INVALID, msg);
        }
    }

    private boolean isValidNodeName(String name) {
        return StringUtils.isNotBlank(name);
    }

    private boolean isValidNodeKey(String key) {
        return StringUtils.isNotBlank(key) && !StringUtils.containsWhitespace(key);
    }

    private boolean isValidNodeUrl(String url) {
        return StringUtils.isNotBlank(url) && urlValidator.isValidUrl(url);
    }

    void runPowCheck(Map<String, IriNode> nodes, long checkNumber) {
        logger.info("Running pow check [{}].", checkNumber);
        nodes.values().stream().filter(node -> node.isPowEnabled() && node.isCallPossible()).forEach(IriNode::doPowCheck);
    }

    void runHealthCheck(Map<String, IriNode> nodes, long checkNumber) {
        logger.info("Running health check [{}].", checkNumber);
        Iterable<Mono<NodeInfo>> nodeInfos = nodes.values().stream().map(IriNode::queryNodeHealth).collect(Collectors.toList());

        // TODO add quorum for current milestone
        // wait until all node infos are there and zip/combine them
        Mono.zip(nodeInfos, nis -> Arrays.stream(nis).mapToLong(ni -> ((NodeInfo) ni).getLatestMilestoneIndex()).max())
                .doFinally(x -> logNodeHealthSummary(nodes))
                .subscribe(milestone -> milestone.ifPresent(val -> {
                            this.updateLatestKnownMilestone(val);
                            nodes.values().forEach(node -> node.updateLatestKnownMilestone(latestKnownMilestone.get()));
                        }),
                        t -> logger.error("Health check failed: [{}]", t.toString(), t) // error should not happen as it terminates
                );
    }

    long getLatestKnownMilestone() {
        return latestKnownMilestone.get();
    }

    private void updateLatestKnownMilestone(long val) {
        if (val > 0) {
            logger.debug("Current milestone: {}.", val);
            latestKnownMilestone.set(val);
        }
    }

    private void logNodeHealthSummary(Map<String, IriNode> nodes) {
        Object[] inactive = nodes.values().stream().filter(node -> !node.isAvailable()).toArray();
        logger.info("Active nodes: [{}]. Inactive nodes: [{}] => {}", nodes.values().size() - inactive.length, inactive.length, inactive);
    }

}
