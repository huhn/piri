/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.api;

import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.exceptions.CannotRegisterNode;
import com.mio.piri.metrics.MeterFactory;
import com.mio.piri.nodes.*;
import com.mio.piri.exceptions.ExceptionHandlerUtil;
import com.mio.piri.nodes.api.dtos.*;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import io.swagger.annotations.*;
import io.vavr.control.Option;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.codec.DecodingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.mio.piri.exceptions.CannotRegisterNode.CannotRegisterReason.CONFLICTING;
import static com.mio.piri.exceptions.CannotRegisterNode.CannotRegisterReason.UNPROCESSABLE;

@Api("Nodes")
@CrossOrigin
@RestController
@RequestMapping(value = "/nodes")
public class NodesApiHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private NodeRegistry nodeRegistry;

    @Autowired
    private RandomKeyGenerator randomKeyGenerator;

    @Autowired
    private HashGenerator hashGenerator;

    @ApiOperation(value = "${api.nodes.register.value}", notes = "${api.nodes.register.notes}", code = 201)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully added node."),
            @ApiResponse(code = 409, message = "There is already a node with that name or url."),
            @ApiResponse(code = 422, message = "The node is not synced or not available."),
    })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<ResponseEntity<RegisterNodeResponse>> register(@Valid @RequestBody RegisterNodeRequest registration) {

        logger.info(registration.toString());

        String password = StringUtils.defaultIfBlank(registration.getPassword(), randomKeyGenerator.generateNodePassword());
        String hashedPassword = hashGenerator.sha256(password);
        String nodeKey = hashGenerator.publicKey(hashedPassword);
        if (nodesRepository.containsKey(hashedPassword) || nodesRepository.containsKeyStartingWith(nodeKey)) {
            throw new CannotRegisterNode(CONFLICTING, "Password already taken. Try again without password.");
        }
        IriNode registeredNode = nodeRegistry.registerIriNode(registration.getName(), registration.getUrl(), nodeKey, registration.isPow());
        return registeredNode.queryNodeHealth()
                .map(nodeInfo -> {
                            if (nodeInfo.getLatestMilestoneIndex() < 0) {
                                throw new CannotRegisterNode(UNPROCESSABLE, "Node is not available.");
                            } else if (!nodeInfo.isSynced()) {
                                throw new CannotRegisterNode(UNPROCESSABLE, "Node is not synced.");
                            }
                            logger.debug("Registered node {} is synced. Proceeding.", registeredNode);
                            registeredNode.updateWithHealthCheckResult(nodeInfo);
                            nodesRepository.addNode(hashedPassword, registeredNode.getName(), registeredNode.getUrl(), registeredNode.isPowEnabled());
                            RegisterNodeResponse registered = new RegisterNodeResponse(registeredNode.getName(), registeredNode.getUrl(), password);
                            return ResponseEntity.status(HttpStatus.CREATED).body(registered);
                        }
                ).doOnError(t -> nodeRegistry.unregisterNode(registeredNode.getName())
                ).onErrorMap(t -> !(t instanceof CannotRegisterNode), t -> {
                    logger.error(t.toString(), t);
                    throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error registering node. Please contact the endpoint operator.");
                });
    }

    @Autowired
    private NodesRepository nodesRepository;

    @Autowired
    private MeterRegistry meterRegistry;

    @ApiOperation(value = "${api.nodes.get.nodes.value}", notes = "${api.nodes.get.nodes.notes}")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<NodeStatus> getNodes() {
        return Flux.fromStream(nodeRegistry.getAllNodes().values().map(this::createNodeStatus).toJavaStream());
    }

    @ApiOperation(value = "${api.nodes.get-by-name.value}", notes = "${api.nodes.get-by-name.notes}")
    @GetMapping(value = "/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<NodeStatus> getNodeByName(
            @ApiParam(name = "name", value = "${api.param.nodes.get-by-name.name.value}", required = true)
            @PathVariable String name) {
        Node node = nodeRegistry.getAllNodes().get(name)
                .getOrElseThrow(() -> createNotFoundException("There is no node with name [" + name + "]."));
        return Mono.just(createNodeStatus(node));
    }

    private NodeStatus createNodeStatus(Node node) {
        IriNode iriNode = (IriNode) node;
        Collection<Timer> timers = meterRegistry.find(MeterFactory.NAME_IRI_COMMANDS_TIMER).tag(MeterFactory.NODE_NAME, node.getName()).tag(MeterFactory.NODE_KEY, node.getKey()).timers();
        Set<ProcessedCommands> processedCommands = createProcessedCommands(timers);
        NodeInfo nodeInfo = iriNode.getLatestNodeInfo();
        return new NodeStatus(node.getName(),
                node.getKey(),
                nodeInfo.getAppName(),
                nodeInfo.getAppVersion(),
                nodeInfo.getNeighbors(),
                nodeInfo.getLatestMilestoneIndex(),
                nodeInfo.getLatestSolidSubtangleMilestoneIndex(),
                node.isPowEnabled() && iriNode.isPowAvailable(),
                iriNode.getDelay(),
                iriNode.isCallPossible(),
                processedCommands);
    }

    private Set<ProcessedCommands> createProcessedCommands(Collection<Timer> timers) {
        return timers.stream().map(timer ->
                new ProcessedCommands(timer.getId().getTag(MeterFactory.COMMAND), timer.count(), (long) timer.totalTime(TimeUnit.MILLISECONDS)))
                .collect(Collectors.toSet());
    }


    /**
     * This endpoint is for the admin only for unregistering nodes without password.
     *
     * @param request The request containing the information for unregistering a node.
     * @return A response with node registration information of <code>404</code> if not found.
     */
    @ApiOperation(value = "${api.nodes.unregister.name.value}", notes = "${api.nodes.unregister.name.notes}")
    @PostMapping(value = "/unregister")
    public ResponseEntity<UnregisterNodeResponse> unregisterByName(@Valid @RequestBody UnregisterNodeRequest request) {
        logger.info("Trying to unregister node. Request: [{}].", request);
        nodesRepository.removeNodeByName(request.getName())
                .peek(np -> logger.info("Removed node from repository: {}", np))
                .onEmpty(() -> logger.info("No node found in repository for request: {}", request));

        IriNode unregistered = Option.of(nodeRegistry.unregisterNode(request.getName()))
                .peek(node -> logger.info("Unregistered node from registry: {}", node))
                .onEmpty(() -> logger.info("No node found in registry for request: {}", request))
                .getOrElseThrow(() -> createNotFoundException("Node not found."));

        return ResponseEntity.ok(new UnregisterNodeResponse(unregistered.getName(), unregistered.getUrl()));
    }

    @ApiOperation(value = "${api.nodes.unregister.password.value}", notes = "${api.nodes.unregister.password.notes}")
    @DeleteMapping(value = "/{password}")
    public ResponseEntity<UnregisterNodeResponse> unregisterWithPassword(
            @ApiParam(name = "password", value = "${api.param.unregister-by-password.password.value}", required = true)
            @PathVariable String password) {
        String nodeName = nodesRepository.getNodeName(hashGenerator.sha256(password)) // try with hashed password
                .orElse(() -> nodesRepository.getNodeName(password)) // try with password TODO this is deprecated. only for backwards compatibility. remove soon.
                .peek(n -> nodesRepository.removeNodeByName(n)) // remove if found
                .getOrElseThrow(() -> createNotFoundException("There is no node for this password."));

        IriNode unregistered = Option.of(nodeRegistry.unregisterNode(nodeName))
                .getOrElseThrow(() -> createNotFoundException("Node not found. Already unregistered."));

        return ResponseEntity.ok(new UnregisterNodeResponse(unregistered.getName(), unregistered.getUrl()));
    }

    private ResponseStatusException createNotFoundException(String reason) {
        return new ResponseStatusException(HttpStatus.NOT_FOUND, reason);
    }

    @ExceptionHandler(DecodingException.class)
    public ResponseEntity handleDecodingErrors(DecodingException dex) {
        logger.warn("Decoding error: {}", dex.getMessage());
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Failed to read HTTP message");
    }

    @ExceptionHandler(WebExchangeBindException.class)
    public ResponseEntity handleInvalidCommand(WebExchangeBindException wex) {
        throw ExceptionHandlerUtil.convertToResponseException(wex, ExceptionHandlerUtil.extractValidationErrorMessage(wex));
    }

    @ExceptionHandler(CannotRegisterNode.class)
    public ResponseEntity handleInvalidCommand(CannotRegisterNode rne) {
        logger.warn(rne.getMessage());
        HttpStatus status;
        if (CONFLICTING == rne.getReason()) {
            status = HttpStatus.CONFLICT;
        } else if (UNPROCESSABLE == rne.getReason()) {
            status = HttpStatus.UNPROCESSABLE_ENTITY;
        } else {
            status = HttpStatus.BAD_REQUEST;
        }
        throw new ResponseStatusException(status, rne.getMessage());
    }

}
