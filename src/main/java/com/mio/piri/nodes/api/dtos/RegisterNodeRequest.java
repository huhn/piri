/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.api.dtos;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Request for registering a new node.")
public class RegisterNodeRequest {

    // public static final String NAME_REGEXP = "^[\\p{Alnum}_.-]+$";
    public static final String NAME_REGEXP = "^\\p{Alnum}[\\p{Alnum}_. -]+\\p{Alnum}$"; // allow no spaces at beginning and end
    public static final String PASS_REGEXP = "^[\\p{Graph}]+$";

    @ApiModelProperty(notes = "${api.model.register-node-request.name.notes}", example = "foo", required = true)
    @NotBlank
    @Size(max = 36)
    @Pattern(regexp = NAME_REGEXP)
    private String name;

    @ApiModelProperty(notes = "${api.model.register-node-request.url.notes}", example = "http://foo.bar.com:14265", required = true, position = 1)
    @NotBlank
    private String url;

    @ApiModelProperty(notes = "${api.model.register-node-request.pow.notes}", position = 2, example = "false")
    private boolean pow = false;

    @ApiModelProperty(notes = "${api.model.register-node-request.password.notes}", hidden = true, position = 3) // hide property
    @Size(min = 10, max = 100)
    @Pattern(regexp = PASS_REGEXP)
    private String password = null;

    public RegisterNodeRequest(String name, String url, boolean pow) {
        this.name = name;
        this.url = url;
        this.pow = pow;
    }

}
