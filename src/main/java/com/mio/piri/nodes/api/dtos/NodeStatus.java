/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.api.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Set;

@Getter
@AllArgsConstructor
public class NodeStatus {

    private final String name;
    private final String key;
    private final String appName;
    private final String appVersion;
    private final int neighbors;
    private final long milestone;
    private final long solidMilestone;
    private final boolean pow;
    private final long delay;
    private final boolean available;

    public long getCount() {
        return commands.stream().mapToLong(ProcessedCommands::getCount).sum();
    }

    public long getMillis() {
        return commands.stream().mapToLong(ProcessedCommands::getMillis).sum();
    }

    private Set<ProcessedCommands> commands;

}
