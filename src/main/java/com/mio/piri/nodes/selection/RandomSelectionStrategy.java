/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.selection;

import com.mio.piri.commands.AttachToTangle;
import com.mio.piri.commands.IriCommand;
import com.mio.piri.nodes.Node;
import com.mio.piri.nodes.PowNode;
import io.vavr.Tuple2;
import io.vavr.collection.Iterator;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class RandomSelectionStrategy {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private SelectionUtility selectionUtility;

    public Node selectNode(IriCommand command, Map<String, Node> nodes) {
        return selectNodes(command, nodes, 1).getOrNull();
    }

    public Seq<Node> selectNodes(IriCommand command, Map<String, Node> nodes, int howMany) {

        Seq<Node> shuffledNodes = shuffleNodes(command, nodes);
        if (logger.isDebugEnabled()) {
            logger.debug("Shuffled nodes: [{}].", shuffledNodes.map(Node::getName).mkString(", "));
        }

        return command.wantsUpToDateNode() || command.needsUpToDateNode()
                ? getLeastDelayedRandomNodes(command, shuffledNodes, howMany)
                : getFirstFittingNodes(command, shuffledNodes, maxAllowedDelay(command), howMany);

    }

    private int maxAllowedDelay(IriCommand command) {
        assert !(command.wantsUpToDateNode() || command.needsUpToDateNode());
        if (command instanceof AttachToTangle) {
            return Integer.MAX_VALUE;
        } else {
            // we don't want commands executed on nodes that are delayed too much and are not well maintained.
            // for example broadcast transactions
            return 10;
        }
    }

    private Seq<Node> shuffleNodes(IriCommand command, Map<String, Node> nodes) {
        if (command instanceof AttachToTangle) {
            Tuple2<? extends Seq<Node>, ? extends Seq<Node>> powAndOtherNodes = nodes.values().shuffle().partition(n -> n instanceof PowNode);
            return powAndOtherNodes._1().appendAll(powAndOtherNodes._2());
        } else {
            return nodes.values().shuffle();
        }
    }

    private Seq<Node> getLeastDelayedRandomNodes(IriCommand command, Seq<Node> nodeSeq, int howMany) {
        Seq<Node> nodes = getFirstFittingNodes(command, nodeSeq, 0, howMany);
        nodes = nodes.size() < howMany
                ? nodes.appendAll(getFirstFittingNodes(command, nodeSeq, 1, howMany))
                : nodes;
        nodes = nodes.size() < howMany && !command.needsUpToDateNode()
                ? nodes.appendAll(getFirstFittingNodes(command, nodeSeq, 2, howMany))
                : nodes;
        return nodes;
    }

    private Seq<Node> getFirstFittingNodes(IriCommand command, Seq<Node> nodeList, int maxDelay, int howMany) {
        Seq<Node> result = List.empty();

        Iterator<Node> iterator = nodeList.iterator();
        while (iterator.hasNext() && result.size() < howMany) {
            Node node = iterator.next();
            if (selectionUtility.isCallFeasible(node, command, maxDelay)) {
                result = result.append(node);
            } else if (logger.isDebugEnabled()) {
                logger.debug("Call to node [{}] not feasible for command [{}].", node.getName(), command.getCommand());
            }
        }

        return result;
    }

}
