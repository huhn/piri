/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.selection;

import com.mio.piri.nodes.Node;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.control.Option;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SessionBinding {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final Duration maxIdleTime;

    private final Map<String, Tuple2<String, Instant>> map = new ConcurrentHashMap<>();

    public SessionBinding(Duration maxMappingIdleTime, Duration cleanInterval) {
        this.maxIdleTime = maxMappingIdleTime;
        startClientMappingsCleanupJob(cleanInterval);
    }

    public Option<String> getNodeForClient(String sessionId) {
        return sessionId == null
                ? Option.none()
                : Option.of(map.get(sessionId)).map(tuple -> getNodeNameIfStillValidOrNull(sessionId, tuple));
    }

    public void putNodeForClient(String sessionId, String nodeName) {
        if (sessionId != null) {
            map.put(sessionId, createNodeMapping(nodeName));
        } else {
            logger.warn("Cannot add session. Session id is null.");
        }
    }

    public int cleanUpMappings() {
        int before = getSize();
        map.values().removeIf(val -> !isNodeMappingStillValid(val));
        return before - map.size();
    }

    public int getSize() {
        return map.size();
    }

    public void removeNodeFromMapping(Node node) {
        if (node != null) {
            logger.debug("Removing node [{}] from session mapping...", node.getName());
            map.values().removeIf(val -> StringUtils.equals(node.getName(), val._1));
        }
    }

    private String getNodeNameIfStillValidOrNull(String sessionId, Tuple2<String, Instant> nodeMapping) {
        if (isNodeMappingStillValid(nodeMapping)) {
            map.put(sessionId, nodeMapping.update2(Instant.now()));
            return nodeMapping._1();
        } else {
            // clean up
            map.remove(sessionId);
            return null;
        }
    }

    private Tuple2<String, Instant> createNodeMapping(String nodeName) {
        return Tuple.of(nodeName, Instant.now());
    }

    private boolean isNodeMappingStillValid(Tuple2<String, Instant> nodeMapping) {
        return nodeMapping._2().isAfter(Instant.now().minus(maxIdleTime));
    }

    private void startClientMappingsCleanupJob(Duration interval) {
        Flux.interval(interval, interval)
                .subscribe(checkNumber -> {
                            int deleted = this.cleanUpMappings();
                            logger.info("Deleted [{}] client to node mapping(s).", deleted);
                        },
                        t -> logger.error(t.toString(), t));

    }

}
