/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes.selection;

import com.mio.piri.commands.IriCommand;
import com.mio.piri.nodes.Node;
import com.mio.piri.nodes.NodeRegistry;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class NodeSelector {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Setter
    private boolean sessionBindingEnabled = true;

    @Autowired
    private SessionSelectionStrategy sessionStrategy;

    @Autowired
    private RandomSelectionStrategy randomStrategy;

    @Autowired
    private NodeRegistry nodeRegistry;

    public List<Node> selectNodes(IriCommand command) {
        Map<String, Node> allNodes = nodeRegistry.getAllNodes();
        Node node = getSessionBoundNode(command, allNodes);

        if (node == null) {
            node = randomStrategy.selectNode(command, allNodes);
            logger.debug("Got random node for command [{}]: [{}]", command.getCommand(), node);
            bindNodeToSessionIfSensible(command, node);
        }
        return node == null ? List.empty() : List.of(node);
    }

    private Node getSessionBoundNode(IriCommand command, Map<String, Node> allNodes) {
        return sessionBindingEnabled && command.wantsSameNodePerClient()
                ? sessionStrategy.selectNode(command, allNodes)
                : null;
    }

    private void bindNodeToSessionIfSensible(IriCommand command, Node node) {
        if (sessionBindingEnabled && command.wantsSameNodePerClient() && command.wantsUpToDateNode()) {
            sessionStrategy.bindNodeToSession(command, node);
        }
    }


}
