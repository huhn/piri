/*
 * Copyright (c) 2018.
 *
 * This file is part of Piri.
 *
 * Piri is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * Piri is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with Piri. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mio.piri.nodes;

import com.mio.piri.commands.AttachToTangle;
import com.mio.piri.commands.IriCommand;
import com.mio.piri.commands.response.NodeInfo;
import com.mio.piri.metrics.MeterFactory;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;

import java.time.Duration;

// TODO add test and fix hierarchy
public class PowNode extends IriNode implements Node {

    public static Duration POW_TIMEOUT = Duration.ofSeconds(5);
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public PowNode(String name, String url, String key, NodeClient nodeClient, MeterFactory metrics, CircuitBreaker circuitBreaker) {
        super(name, url, key,true, nodeClient, metrics, circuitBreaker);
        info.setAppName("POW Node");
        info.setAppVersion(null);
        info.setNeighbors(0);
    }

    @Override
    public Duration getTimeout(IriCommand command) {
        logger.debug("Return pow timeout [{}] for command [{}].", POW_TIMEOUT, command.getCommand());
        return POW_TIMEOUT;
    }

    @Override
    public Mono<NodeInfo> queryNodeHealth() {
        logger.debug("Query node health for node [{}]", name);
        // TODO refactor health and pow checks for pow and other nodes
        // we need to use the attach to tangle result and set the node to available if it isn't
        // there should be a pow check method that does all that and returns a mono
        return attachToTangle(new AttachToTangle())
                .then(Mono.just(info));
    }

    @Override
    protected NodeInfo updateWithHealthCheckError(Throwable t) {
        logger.info("Node [{}] not healthy: {}", name, t.getMessage());
        if (isAvailable()) {
            setUnavailable("not available");
        }
        return info;
    }

    @Override
    public void updateWithHealthCheckResult(final NodeInfo currentNodeInfo) {
        logger.debug("[{}] Health check successful.", name);
        if (!isAvailable()) {
            setAvailable();
        }
    }

    @Override
    public boolean isSupported(IriCommand command) {
        // we support pow only
        return powAvailable && command instanceof AttachToTangle;
    }

}
